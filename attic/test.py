from typing import List, Union, Dict
from pydantic import BaseModel, field_validator, Field
from uuid import uuid4


class IDModel(BaseModel):
    id: str = Field(
        min_length=32,
        max_length=36,
        pattern="^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$",
    )


class ConnectionModel(IDModel):
    view: int = 1


class ConnectionList(BaseModel):
    ids: List[Union[ConnectionModel, str]]

    @field_validator("ids")
    @classmethod
    def convert_ids(cls, v):
        for i, value in enumerate(v):
            if not isinstance(value, ConnectionModel):
                v[i] = ConnectionModel(id=value)
        return v


# Example usage
example_1 = ConnectionList(ids=[{"id": uuid4().hex}, {"id": uuid4().hex}])
example_2 = ConnectionList(ids=[uuid4().hex, uuid4().hex])

print(example_1)
print(example_2)
