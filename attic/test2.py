from typing_extensions import Annotated

from pydantic import AfterValidator, Field, TypeAdapter, WithJsonSchema, BaseModel
from typing import Union
from uuid import uuid4


def convert_id(v):
    if not isinstance(v, ConnectionModel):
        return ConnectionModel(id=v)
    else:
        return v


class IDModel(BaseModel):
    id: str = Field(
        min_length=32,
        max_length=36,
        pattern="^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$",
    )


class ConnectionModel(IDModel):
    view: int = 1


TruncatedFloat = Annotated[
    Union[ConnectionModel, str],
    AfterValidator(convert_id),
    Field(description="List of children connections"),
]


class Test(BaseModel):
    value: TruncatedFloat


print(Test(value=uuid4().hex))
