import os
from itk_demo_configdb.states.context import Context
from itk_demo_configdb.states.dbm import set_dbm_value, get_dbm_value
from itk_demo_configdb.tools import set_connection
import logging


def watch_connection():
    mariadb_key = config.mariadb_key

    if mariadb_key is None or mariadb_key == "":
        log.info(" No mariadb-key configured. You must either provide the environmental variable mariadb_key or configdb_database_uri.")
        exit()
    else:
        try:
            registry = ServiceRegistry(namespace="")
            mariadb = registry.get(mariadb_key)
            uri = f"{config.mariadb_user}:{config.mariadb_pw}@{mariadb.replace('http://', '')}/configdb"
            set_dbm_value("uri", uri)
            log.info(f" SQLALCHEMY_DATABASE_URI set to {uri}")
            return True
        except SRKeyError:
            log.info(f" Mariadb-url '{mariadb_key}' could not be found. Key is being watched.")
        except SRException:
            log.info(f" Error in service registry, ETCD not available.")

        registry = ServiceRegistry(namespace="")
        mariadb_cancel = registry.watch(mariadb_key, mariadb_callback)
        return mariadb_cancel


def mariadb_callback(event_type, key, value):
    log.info(f" Callback of type {event_type}.")
    if event_type == "PUT":
        uri = f"{config.mariadb_user}:{config.mariadb_pw}@{value.replace('http://', '')}/configdb"
        set_dbm_value("uri", uri)
        log.info(f"Set uri to {uri}.")
        Context.set_state(States.DATABASENOTCONNECTED)
        try:
            get("http://localhost:5000/api/database/connect")
        except ConnectionError:
            log.info(" Automatic reconnection failed.")
    elif event_type == "DELETE":
        log.info(f" Mariadb service was stopped.")
        Context.set_state(States.DATABASENOTCONNECTED)