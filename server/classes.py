# classes.py


class ScanInfo:
    active = False
    name = ""

    def __init__(self, active=False, name=""):
        self.active = active
        self.name = name


class Scans:
    def __init__(self):
        self.digitalscan = ScanInfo(active=True, name="digitalscan")
        self.analogscan = ScanInfo(active=False, name="analogscan")
        self.noisescan = ScanInfo(active=False, name="noisescan")


class Test:
    scans = Scans()
    scan_info_array = []

    def __init__(self):
        self.scans = Scans()
        self.scans.analogscan.active = True


# Example usage
test = Test()
test2 = vars(test.scans)
for key, value in vars(test.scans).values():
    print(value.name)
# scan_info_array = [
#     getattr(test.scans, attr)
#     for attr in dir(test.scans)
#     if isinstance(getattr(test.scans, attr), ScanInfo)
#     and getattr(test.scans, attr).active
# ]

# for scan in scan_info_array:
#     print(scan.name)
#     scan.name = "new name"

# print(test)
