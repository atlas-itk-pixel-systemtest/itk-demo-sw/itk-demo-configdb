from configdb_server.testing_tools import extract_dict, sort_list, std_data, Fixture
from configdb_server.exceptions import IDInUseError, NameInUseError
from configdb_server.adapter.base_adapter import Connection
from configdb_server.database_tools import Backends
from configdb_server.database_tools import CopyTreeModel
from configdb_server.dataclasses import ObjectFullModel, TagFullModel, PayloadFullModel, FullTreeModel

import pytest
import json


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_clone(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="new tree", author="pytest"))

    stage_db.clone(db, CopyTreeModel(identifier="new tree", name="root_tag", author="pytest"))
    tree = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_clone_stale(connection: Fixture):
    db = connection.db
    stage_db = connection.stage_db

    write_session = stage_db.backend.create_write_session()
    object_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="root"))
    stage_db.backend.create_tag(write_session, TagFullModel(name="root_tag", type="test", objects=[object_id], author="pytest"))
    child_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="test_child"))
    # unsicher
    payload_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="test_payload", data="test_data"))
    metadata_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="metadata", data={"test": "test"}, meta=True))

    stage_db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])

    stage_db.backend.remove_from_object(
        write_session,
        object_id,
        [child_id],
        [metadata_id, payload_id],
    )
    write_session.close()
    # unsicher
    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="new tree", author="pytest"))

    empty_object = db.read_tree("new tree")

    assert not empty_object["children"]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_clone_stale2(connection: Fixture):
    db = connection.db
    stage_db = connection.stage_db

    write_session = stage_db.backend.create_write_session()
    object_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="root"))
    stage_db.backend.create_tag(write_session, TagFullModel(name="root_tag", type="test", objects=[object_id], author="pytest"))
    child_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="test_child"))
    payload_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="test_payload", data="test_data"))
    metadata_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="metadata", data={"test": "test"}, meta=True))

    stage_db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])

    stage_db.backend.remove_from_object(
        write_session,
        object_id,
        [child_id],
        [metadata_id, payload_id],
    )
    write_session.close()

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="new tree", author="pytest", view=3))

    object = db.read_tree("new tree", view=3)

    assert object["children"][0]["type"] == "test_child"
    assert object["children"][0]["view"] == 2


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_clone_stale3(connection: Fixture):
    db = connection.db
    stage_db = connection.stage_db

    write_session = stage_db.backend.create_write_session()
    object_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="root"))
    stage_db.backend.create_tag(write_session, TagFullModel(name="root_tag", type="test", objects=[object_id], author="pytest"))
    child_id = stage_db.backend.create_object(write_session, ObjectFullModel(type="test_child"))
    payload_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="test_payload", data="test_data"))
    metadata_id = stage_db.backend.create_payload(write_session, PayloadFullModel(type="metadata", data={"test": "test"}, meta=True))

    stage_db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])

    stage_db.backend.remove_from_object(
        write_session,
        object_id,
        [child_id],
        [metadata_id, payload_id],
    )
    write_session.close()

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="new tree", type="pytest", view=3, author="pytest"))

    object = db.read_tree("new tree")

    assert not object["children"]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_commit(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="new tree", author="pytest"))

    tag = db.read_tree("new tree", payload_data=True)

    output = extract_dict(tag)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_chain_meta(connection_meta_data: Fixture):
    stage_db = connection_meta_data.stage_db
    db = connection_meta_data.db
    data = connection_meta_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"))
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag", name="root_tag", author="pytest"))
    tree = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag2", author="pytest"))
    tree2 = db.read_tree("backend_tag2", payload_data=True)
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag2", name="root_tag", author="pytest"))
    tree3 = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    output = sort_list(extract_dict(tree))
    output2 = sort_list(extract_dict(tree2))
    output3 = sort_list(extract_dict(tree3))
    input = sort_list(extract_dict(data))

    assert input == output
    assert input == output2
    assert input == output3


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_chain(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"))
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag", name="root_tag", author="pytest"))
    tree = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag2", author="pytest"))
    tree2 = db.read_tree("backend_tag2", payload_data=True)
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag2", name="root_tag", author="pytest"))
    tree3 = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    output = extract_dict(tree)
    output2 = extract_dict(tree2)
    output3 = extract_dict(tree3)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)
    assert sort_list(input) == sort_list(output2)
    assert sort_list(input) == sort_list(output3)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name],
    indirect=True,
)
def test_keep_ids(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"), keep_ids=True)
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag", name="root_tag", author="pytest"), keep_ids=True)
    tree = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    with pytest.raises(IDInUseError):
        stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag2", author="pytest"), keep_ids=True)

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_failed_commit(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="backend_tag", type="test", author="pytest"))
    write_session.close()

    with pytest.raises(NameInUseError, match="Name already in use: The given name backend_tag is already in use, please retry with a different name."):
        stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"))

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag2", author="pytest"))
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag2", name="root_tag", author="pytest"))
    tree = stage_db.read_tree("root_tag", payload_data=True, resolve_payloads=db)

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_commit_id(connection: Fixture):
    stage_db = connection.stage_db
    db = connection.db

    data = std_data
    root_uuid = stage_db.create_full_tree(FullTreeModel(data=data, author="pytest"))

    stage_db.commit(db, CopyTreeModel(identifier=root_uuid, name="new tree", author="pytest"))

    tag = db.read_tree("new tree", payload_data=True)

    output = extract_dict(tag)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_runkey_with_payload(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    write_session = stage.backend.create_write_session()
    payl_id = stage.backend.create_payload(write_session, PayloadFullModel(type="test_payload", data=json.dumps({"test": "test"}), meta=True))
    write_session.close()

    stage.create_full_tree(FullTreeModel(data=std_data, name="root_tag", payloads=[payl_id], author="pytest"))
    stage.commit(db, CopyTreeModel(identifier="root_tag", name="root_tag", author="pytest"))
    stage.clone(db, CopyTreeModel(identifier="root_tag", name="root_tag2", author="pytest"))

    read_session = stage.backend.create_read_session()
    tree = stage.backend.read_tag_tree(read_session, name="root_tag2", payload_data=True, resolve_payloads=db)
    read_session.close()

    assert tree["metadata"]["test"] == "test"


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_runkey_with_multiple_objects(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    write_session = stage.backend.create_write_session()
    objs = stage.object_insert(list=[{"type": "test1"}, {"type": "test2"}, {"type": "test3"}])
    stage.backend.create_tag(write_session, TagFullModel(name="root_tag", type="test", objects=objs, author="pytest"))
    write_session.close()

    stage.commit(db, CopyTreeModel(identifier="root_tag", name="root_tag", author="pytest"))
    stage.clone(db, CopyTreeModel(identifier="root_tag", name="root_tag2", author="pytest"))

    read_session = stage.backend.create_read_session()
    tree = stage.backend.read_tag_tree(read_session, name="root_tag2", payload_data=True)
    read_session.close()

    assert len(tree["objects"]) == 3
