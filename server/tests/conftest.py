import pytest
from configdb_server.database_tools import Database, Backends
from configdb_server.testing_tools import std_data, std_data_scan, std_data_small, std_data_meta, Fixture
from configdb_server.dataclasses import FullTreeModel, ReuseObjectModel, TagFullModel, PayloadFullModel, ObjectFullModel
import json
import os

current_dir = os.path.dirname(os.path.dirname(__file__))


# @pytest.fixture(scope="session", autouse=True)
# def setup_once():
#     command = [
#         "docker",
#         "compose",
#         "-f",
#         os.path.join(current_dir, "compose.yaml"),
#         "up",
#         "-d",
#     ]
#     process = subprocess.Popen(command, stdout=subprocess.PIPE)
#     output, error = process.communicate()
#     process.wait()


def create_connection(request) -> Fixture:
    backend = Backends[request.param]
    if backend == Backends.SQLALCHEMY_MARIADB or backend == Backends.MARIADB:
        stage_db = Database(backend, "configdb:test@localhost:3307/configdb")
        db = Database(backend, "configdb:test@localhost:3308/configdb")
    elif backend == Backends.SQLALCHEMY_POSTGRES:
        stage_db = Database(backend, "configdb:test@localhost:5430/configdb")
        db = Database(backend, "configdb:test@localhost:5431/configdb")
    else:
        # stage_db = Database(backend, "/file:stage.db?mode=memory&cache=shared&uri=true")
        # db = Database(backend, "/file:backend.db?mode=memory&cache=shared&uri=true")
        # ?mode=memory&cache=shared&uri=true

        stage_db = Database(backend, f"/{str(os.path.join(current_dir, 'pytest_stage.db'))}")
        db = Database(backend, f"/{str(os.path.join(current_dir, 'pytest_backend.db'))}")

    stage_db.init_database()
    db.init_database()

    db.backend.clear_database()
    stage_db.backend.clear_database()

    connection = Fixture(db, stage_db)
    return connection


@pytest.fixture(
    params=[
        # Backends.SQLALCHEMY_REC.name,
        Backends.SQLALCHEMY_SQLITE.name,
        Backends.SQLALCHEMY_MARIADB.name,
        Backends.SQLALCHEMY_POSTGRES.name,
        Backends.MARIADB.name,
    ]
)  # Repeat for multiple backend types
def connection(request) -> Fixture:
    connection = create_connection(request)
    return connection

    # if backend == Backends.SQLALCHEMY_MARIADB:
    #     db.backend.clear_database()
    #     stage_db.backend.clear_database()


@pytest.fixture
def connection_data(connection: Fixture) -> Fixture:
    connection.data = std_data
    data = ReuseObjectModel.model_validate(connection.data)
    fullTree = FullTreeModel(data=data, name="root_tag", author="pytest")
    res = connection.stage_db.create_full_tree(fullTree)  # noqa: F841

    return connection


@pytest.fixture
def connection_member_data(connection: Fixture) -> Fixture:
    connection.data = std_data_small
    res = connection.stage_db.create_full_tree(FullTreeModel(data=connection.data, name="root_tag", author="pytest"))  # noqa: F841
    res2 = connection.stage_db.create_full_tree(FullTreeModel(data=connection.data, name="root_tag2", author="pytest"))  # noqa: F841

    session = connection.stage_db.backend.create_write_session()
    res3 = connection.stage_db.backend.create_tag(session, TagFullModel(name="bundle", type="runkey", members=["root_tag", "root_tag2"], author="pytest"))  # noqa: F841
    session.close()

    return connection


@pytest.fixture
def connection_meta_data(connection: Fixture) -> Fixture:
    connection.data = std_data_meta
    connection.stage_db.create_full_tree(FullTreeModel(data=connection.data, name="root_tag", author="pytest"))

    return connection


@pytest.fixture
def connection_scan_data(connection: Fixture) -> Fixture:
    connection.data = std_data_scan
    connection.stage_db.create_full_tree(FullTreeModel(data=connection.data, name="root_tag", author="pytest"))

    return connection


@pytest.fixture
def connection_small_data(connection: Fixture) -> Fixture:
    connection.data = std_data_small
    connection.stage_db.create_full_tree(FullTreeModel(data=connection.data, name="root_tag", author="pytest"))

    return connection


@pytest.fixture(
    params=[
        # Backends.SQLALCHEMY_REC.name,
        Backends.SQLALCHEMY_SQLITE.name,
        Backends.SQLALCHEMY_MARIADB.name,
        Backends.SQLALCHEMY_POSTGRES.name,
        Backends.MARIADB.name,
    ]
)
def connection_search(request) -> Fixture:
    connection = create_connection(request)

    db = connection.stage_db

    write_session = db.backend.create_write_session()

    payl1 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as01"))
    payl2 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds01"))
    payl3 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts01"))

    payl4 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as02"))
    payl5 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds02"))
    payl6 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts02"))

    payl7 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as03"))
    payl8 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds03"))
    payl9 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts03"))

    meta1 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": 1}), meta=True))
    meta2 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": 2}), meta=True))

    obj1 = db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl1, payl2, payl3, meta1]))  # noqa: F841
    obj2 = db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl4, payl5, payl6, meta2]))  # noqa: F841

    tag = db.backend.create_tag(  # noqa: F841
        write_session, TagFullModel(name="tag1", type="scans", payloads=[payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9], author="pytest")
    )

    write_session.close()

    return connection
