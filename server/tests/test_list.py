from configdb_server.testing_tools import extract_dict, sort_list, Fixture
from configdb_server.database_tools import Backends
from configdb_server.dataclasses import CopyTreeModel

import pytest


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_list(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db

    read_session = stage_db.backend.create_read_session()
    rk = stage_db.backend.read_lists(read_session, "root_tag", payload_data=True, decode=False)
    tree1 = stage_db.read_tree("root_tag", payload_data=True, decode=False)
    read_session.close()

    write_session = db.backend.create_write_session()
    ids, payload_ids = db.backend.write_lists(write_session, rk, encode=False, keep_ids=False)
    tree2 = db.read_tree(ids[0], payload_data=True, decode=False)
    write_session.close()

    output = extract_dict(tree1)
    input = extract_dict(tree2)

    assert sort_list(input) == sort_list(output)


@pytest.mark.parametrize(
    "connection", [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name], indirect=True
)
def test_read_lists(connection_data: Fixture):
    stage_db = connection_data.stage_db

    lists = stage_db.read_lists(name="root_tag", payload_data=True, decode=False)

    assert len(lists.payloads) == 10
    assert len(lists.objects) == 8
    assert len(lists.tags) == 1


@pytest.mark.parametrize(
    "connection", [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name], indirect=True
)
def test_read_lists_depth(connection_data: Fixture):
    stage_db = connection_data.stage_db

    lists = stage_db.read_lists(name="root_tag", payload_data=True, decode=False, depth=1)

    assert len(lists.payloads) == 5
    assert len(lists.objects) == 3
    assert len(lists.tags) == 1


@pytest.mark.parametrize(
    "connection", [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name], indirect=True
)
def test_read_write_lists(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db
    data = connection_data.data

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="root_tag", author="pytest"))

    lists = db.read_lists(name="root_tag", payload_data=True, decode=False)
    stage_db.write_lists(lists, keep_ids=False, encode=False, add_tag=True)

    tree = stage_db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)
