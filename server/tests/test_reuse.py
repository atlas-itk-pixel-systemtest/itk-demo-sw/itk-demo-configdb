from configdb_server.exceptions import DatasetNotFoundError, IDInUseError
from configdb_server.testing_tools import reuse_data, Fixture
from configdb_server.database_tools import Backends
from configdb_server.dataclasses import FullTreeModel, PayloadFullModel, CopyTreeModel
from uuid import uuid4
import pytest


def test_reuse_payload(connection: Fixture):
    db = connection.stage_db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(FullTreeModel(name="root_tag", data=data, author="pytest"))
    tree = db.read_tree("root_tag", payload_data=True)

    payl = db.get_all("payload")

    assert len(payl) == 1

    assert tree["children"][0]["payloads"][0] == tree["children"][1]["payloads"][0]
    assert tree["children"][0]["payloads"][0] == tree["children"][2]["payloads"][0]
    assert tree["children"][1]["payloads"][0] == tree["children"][2]["payloads"][0]


def test_reuse_payload_twice(connection: Fixture):
    db = connection.stage_db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(FullTreeModel(data=data, name="root_tag", author="pytest"))

    with pytest.raises(IDInUseError):
        db.create_full_tree(FullTreeModel(data=data, name="root_tag2", author="pytest"))


def test_reuse_old_payload(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, PayloadFullModel(type="test_payload", name="test_data"))
    write_session.close()

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(FullTreeModel(data=data, name="root_tag", author="pytest"))
    tree = db.read_tree("root_tag", payload_data=True)

    payl = db.get_all("payload")

    assert len(payl) == 1

    assert tree["children"][0]["payloads"][0] == tree["children"][1]["payloads"][0]
    assert tree["children"][0]["payloads"][0] == tree["children"][2]["payloads"][0]
    assert tree["children"][1]["payloads"][0] == tree["children"][2]["payloads"][0]


def test_reuse_wrong_payload(connection: Fixture):
    db = connection.stage_db

    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    with pytest.raises(DatasetNotFoundError):
        db.create_full_tree(FullTreeModel(data=data, name="root_tag", author="pytest"))


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_object(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, PayloadFullModel(type="test_payload", name="test_data"))
    write_session.close()

    object_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "felix",
                "children": [
                    {
                        "id": object_id,
                        "type": "optoboard",
                        "payloads": [{"reuse_id": payload_id}],
                    },
                ],
            },
            {"reuse_id": object_id},
        ],
    }

    db.create_full_tree(FullTreeModel(data=data, name="root_tag", author="pytest"))
    session = db.backend.create_read_session()
    tree = db.backend.read_tag_tree(session, name="root_tag", payload_data=True)["objects"][0]
    session.close()

    if len(tree["children"][0]["children"]) == 1:
        assert tree["children"][0]["children"][0] == tree["children"][1]
    else:
        assert tree["children"][0] == tree["children"][1]["children"][0]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_payload_commit(connection: Fixture):
    stage_db = connection.stage_db
    db = connection.db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    stage_db.create_full_tree(FullTreeModel(data=data, name="root_tag", author="pytest"))
    tree1 = stage_db.read_tree("root_tag")
    stage_db.commit(db, CopyTreeModel(name="root_tag", identifier="root_tag", author="pytest"))
    tree2 = db.read_tree("root_tag")
    stage_db.clone(db, CopyTreeModel(name="root_tag2", identifier="root_tag", author="pytest"))
    tree3 = stage_db.read_tree("root_tag2")
    stage_db.commit(db, CopyTreeModel(name="root_tag2", identifier="root_tag2", author="pytest"))
    tree4 = db.read_tree("root_tag2")

    trees = [tree1, tree2, tree3, tree4]
    for tree in trees:
        assert tree["children"][0]["payloads"][0]["id"] == tree["children"][1]["payloads"][0]["id"]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_tree_example(connection: Fixture):
    db = connection.db

    db.create_full_tree(FullTreeModel(data=reuse_data, name="root_tag", author="pytest"))
    tree = db.read_tree("root_tag")

    found = False
    for child1 in tree["children"][0]["children"]:
        if child1["type"] == "A2":
            for child2 in tree["children"][1]["children"]:
                if child2["type"] == "A2":
                    assert child1 == child2
                    found = True
                    break
            if found:
                break

    assert found
