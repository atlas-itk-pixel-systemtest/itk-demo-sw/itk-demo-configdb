from configdb_server.testing_tools import extract_dict, sort_list, format_tag_data, Fixture
from configdb_server.exceptions import NameNotValidError
from configdb_server.dataclasses import TagFullModel

import pytest


def test_create_tag(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="my_tag", type="runkey", objects=[tree["id"]], author="pytest"))
    write_session.close()
    tag = db.read_tree("my_tag", payload_data=True)

    output = extract_dict(tag)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_create_tag_multiple(connection_data: Fixture):
    db = connection_data.stage_db
    connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="my_tag", type="runkey", objects=[tree["id"], tree["children"][0]["id"]], author="pytest"))
    write_session.close()
    read_session = db.backend.create_read_session()
    tag = db.backend.read_tag_tree(read_session, name="my_tag")
    read_session.close()

    assert len(tag["objects"]) == 2


def test_create_tag_comment(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="my_tag", type="runkey", objects=[tree["id"]], comment="test", author="pytest"))
    tag = db.backend.read_tag(write_session, "my_tag")
    write_session.close()

    extract_dict(tag)
    extract_dict(data)

    assert tag["comment"] == "test"


def test_create_invalid_tag(connection_data: Fixture):
    db = connection_data.stage_db
    connection_data.data

    write_session = db.backend.create_write_session()
    with pytest.raises(NameNotValidError):
        db.backend.create_tag(write_session, TagFullModel(name="", type="runkey", author="pytest"))
    with pytest.raises(NameNotValidError):
        db.backend.create_tag(write_session, TagFullModel(name="550e8400e29b011d4a71646655440000", type="runkey", author="pytest"))
    write_session.close()

    assert 1 == 1


def test_format_tag(connection_small_data: Fixture):
    db = connection_small_data.stage_db

    runkey = db.read_tree("root_tag", payload_data=True)
    payload = runkey["payloads"][0]
    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="test_tag", type="test", objects=[runkey["id"]], payloads=[payload["id"]], author="pytest"))
    write_session.close()

    text = db.format_tag("test_tag", include_id=False)

    assert text == format_tag_data


def test_format_empty_tag(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="test_tag", type="test", author="pytest"))
    write_session.close()

    text = db.format_tag("test_tag", include_id=False)

    assert text


def test_latest_tag(connection_small_data: Fixture):
    db = connection_small_data.stage_db

    runkey = db.read_tree("root_tag", payload_data=True)
    payload = runkey["payloads"][0]

    write_session = db.backend.create_write_session()
    db.backend.create_tag(
        write_session, TagFullModel(name="test_tag1", type="test", objects=[runkey["id"]], payloads=[payload["id"]], author="pytest"), tag_latest=True
    )
    tag1 = db.backend.read_tag(write_session, "test_tag1")
    latest1 = db.backend.read_tag(write_session, "latest")
    db.backend.create_tag(write_session, TagFullModel(name="test_tag2", type="test", author="pytest"), tag_latest=True)
    tag2 = db.backend.read_tag(write_session, "test_tag2")
    latest2 = db.backend.read_tag(write_session, "latest")
    write_session.close()

    assert tag1["time"] == latest1["time"]
    assert tag1["payloads"] == latest1["payloads"]
    assert tag1["objects"] == latest1["objects"]
    assert tag2["time"] == latest2["time"]
    assert tag2["payloads"] == latest2["payloads"]
    assert tag2["objects"] == latest2["objects"]


def test_create_tag_member(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, TagFullModel(name="member", type="runkey", author="pytest"))
    db.backend.create_tag(write_session, TagFullModel(name="group", type="runkey", author="pytest"))
    db.backend.create_tag(write_session, TagFullModel(name="root_tag", type="runkey", members=["member"], groups=["group"], author="pytest"))
    tag = db.backend.read_tag(write_session, "root_tag")
    write_session.close()

    assert tag["members"] == ["member"]
    assert tag["groups"] == ["group"]
