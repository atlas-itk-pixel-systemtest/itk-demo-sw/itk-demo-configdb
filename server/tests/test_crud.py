from configdb_server.testing_tools import Fixture
from configdb_server.exceptions import NotValidJSonError, DatasetNotFoundError
from configdb_server.dataclasses import PayloadFullModel, TagFullModel, ObjectFullModel, CopyTreeModel
import pytest
import json


def test_meta_invalid_json(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    session.close()

    with pytest.raises(NotValidJSonError):
        db.backend.create_payload(session, PayloadFullModel(type="meta", data="test", meta=True))


def test_payload(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()

    id1 = db.backend.create_payload(session, PayloadFullModel(type="test", data='{"test": 1}', name="test"))
    id2 = db.backend.create_payload(session, PayloadFullModel(type="meta", data='{"test": 1}', name="test", meta=True))

    payl1 = db.backend.read_payload(session, id1)
    payl2 = db.backend.read_payload(session, id2)

    session.close()

    assert json.loads(payl1["data"]) == json.loads(payl2["data"])


def test_payload_stage(connection_data: Fixture):
    stage_db = connection_data.stage_db
    db = connection_data.db

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"))
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag", name="root_tag", author="pytest"))

    tag = stage_db.read_tree("root_tag")

    object_payload = tag["payloads"][0]

    session = stage_db.backend.create_read_session()
    object_payload_data = stage_db.backend.read_payload(session, object_payload["id"], resolve_payloads=db)["data"]
    session.close()

    assert object_payload_data != "null"


def test_metadata_stage(connection_meta_data: Fixture):
    stage_db = connection_meta_data.stage_db
    db = connection_meta_data.db

    stage_db.commit(db, CopyTreeModel(identifier="root_tag", name="backend_tag", author="pytest"))
    stage_db.clone(db, CopyTreeModel(identifier="backend_tag", name="root_tag", author="pytest"))

    tag = stage_db.read_tree("root_tag")

    object_payloads = [object for object in tag["children"] if object["children"]][0]["children"][0]["payloads"]
    object_payload = [payload for payload in object_payloads if "meta" in payload and payload["meta"]][0]

    session = stage_db.backend.create_read_session()
    object_payload_data = stage_db.backend.read_payload(session, object_payload["id"], resolve_payloads=db)["data"]
    session.close()

    assert object_payload_data != "null"


def test_metadata_json(connection: Fixture):
    db = connection.stage_db
    data = '{\n    "felix_app": 1,\n    "felix_card_number": 0,\n    "felix_device_number": 0,\n    "felix_initialize": 3,\n    "dryrun": 0,\n    "noflx": 0,\n    "felix_config_file": "felix_config.json",\n    "felix_data_interface": "lo",\n    "felix_toflx_ip": "localhost",\n    "felix_tohost_ip": "localhost",\n    "felix_tohost_dcs_pages": 256,\n    "serial": 123\n}'

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, PayloadFullModel(type="meta", data=data, name="test", meta=True))
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert json.loads(payl["data"]) == json.loads(data)


def test_payload_not_found(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, PayloadFullModel(type="payl", data="test_data"))
    session.close()

    with pytest.raises(DatasetNotFoundError):
        db.backend.read_payload(session, id, meta=True)


def test_payload_not_found2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    id = db.backend.create_payload(session, PayloadFullModel(type="payl", data='{"a":1}', meta=True))
    payl = db.backend.read_payload(session, id)
    session.close()

    assert payl["data"] == '{"a": 1}'


def test_payload_no_format(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    id = db.backend.create_payload(session, PayloadFullModel(type="payl", data="test_data", name="test"))
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert payl["data"] == "test_data"


def test_payload_format(connection: Fixture):
    db = connection.stage_db
    data = json.dumps({"a": 1})

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, PayloadFullModel(type="meta", data=data, name="test"))
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert payl["data"] == json.dumps({"a": 1}, indent=4)


def test_all(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    db.backend.create_tag(session, TagFullModel(name="test_tag", type="test", author="pytest"))
    obj_id = db.backend.create_object(session, ObjectFullModel(type="test_obj", tags=["test_tag"]))
    db.backend.create_payload(session, PayloadFullModel(type="test_payl", data="test_data", objects=[obj_id], tags=["test_tag"]))
    db.backend.create_payload(session, PayloadFullModel(type="meta", data='{"a": 1}', name="test", objects=[obj_id], tags=["test_tag"], meta=True))
    tag = db.backend.read_tag_tree(session, name="test_tag")
    object = db.backend.read_object_tree(session, id=obj_id)

    session.close()

    assert len(object["payloads"]) == 2
    assert len(tag["payloads"]) == 2
    assert len(tag["objects"]) == 1
