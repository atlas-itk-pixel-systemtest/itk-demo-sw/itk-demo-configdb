from configdb_server.testing_tools import Fixture
from configdb_server.exceptions import DatasetNotFoundError
from configdb_server.database_tools import Backends
from configdb_server.dataclasses import CopyTreeModel, FullTreeModel
import pytest
import os
import json

current_dir = os.path.dirname(os.path.dirname(__file__))


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_id(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    with open(str(os.path.join(current_dir, "tests/sr1_strips.json")), "r") as file:
        data = file.read()

    rk_name = "test"
    stage.create_full_tree(FullTreeModel(data=json.loads(data), name=rk_name, author="pytest"))
    stage.commit(db, CopyTreeModel(name=rk_name, identifier=rk_name, author="pytest"), delete=False)
    tree = db.read_tree(rk_name)

    assert tree


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_delete(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    with open(str(os.path.join(current_dir, "tests/sr1_strips.json")), "r") as file:
        data = file.read()

    rk_name = "test"
    stage.create_full_tree(FullTreeModel(data=json.loads(data), name=rk_name, author="pytest"))
    session = stage.backend.create_write_session()
    stage.backend.delete_tree(session, identifier=rk_name)
    session.close()

    with pytest.raises(DatasetNotFoundError):
        db.read_tree(rk_name)

    lis = [
        "closure",
        "payload",
        "object",
        "tag",
        "metadata",
    ]  # "tag_tag", "tag_object", "tag_metadata", "tag_payload", "object_payload", "object_metadata", "tag_object"]

    for i in lis:
        assert len(stage.get_all(i)) == 0
