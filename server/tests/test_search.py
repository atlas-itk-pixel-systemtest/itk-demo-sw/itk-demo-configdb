from configdb_server.testing_tools import Fixture
from configdb_server.exceptions import DatasetNotFoundError
from configdb_server.database_tools import Backends
from configdb_server.dataclasses import PayloadFullModel, TagFullModel, ObjectFullModel

import pytest
import json


def test_search(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config("root_tag", object_type="optoboard", search_dict={"serial": "123"})

    assert len(payloads) == 1
    assert payloads[0]["type"] == "optoboard"
    assert payloads[0]["payloads"][0]["data"] == "payload_lpgbt03" or json.loads(payloads[0]["payloads"][0]["data"]) == {
        "serial": "123",
        "test": "test",
    }
    assert payloads[0]["payloads"][1]["data"] == "payload_lpgbt03" or json.loads(payloads[0]["payloads"][1]["data"]) == {
        "serial": "123",
        "test": "test",
    }


def test_search_nested(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config(
        "root_tag",
        object_type="optoboard",
        search_dict={"test": {"test": {"test": "content"}}},
    )

    assert len(payloads) == 1
    assert payloads[0]["payloads"][0]["data"] == "payload_lpgbt01" or json.loads(payloads[0]["payloads"][0]["data"]) == {
        "serial": "100",
        "test": {"test": {"test": "content"}},
    }
    assert payloads[0]["payloads"][1]["data"] == "payload_lpgbt01" or json.loads(payloads[0]["payloads"][1]["data"]) == {
        "serial": "100",
        "test": {"test": {"test": "content"}},
    }


def test_search_for_subtree(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    trees = db.search_for_subtree("root_tag", object_type="optoboard", search_dict={"serial": "100"})

    assert len(trees) == 2
    assert len(trees[0]["children"]) == 2
    assert len(trees[1]["children"]) == 2
    with pytest.raises(KeyError):
        trees[0]["children"][0]["payloads"][0]["data"]


def test_search_for_subtree_depth(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    trees = db.search_for_subtree("root_tag", object_type="optoboard", search_dict={"serial": "100"}, depth=0)

    assert len(trees) == 2
    assert len(trees[0]["children"]) == 0
    assert len(trees[1]["children"]) == 0


def test_search_for_subtree_data(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    trees = db.search_for_subtree("root_tag", object_type="optoboard", search_dict={"serial": "100"}, payload_data=True)

    assert len(trees) == 2
    assert len(trees[0]["children"]) == 2
    assert len(trees[1]["children"]) == 2
    assert len(trees[0]["payloads"][0]["data"]) > 5


def test_search_no_params(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config("root_tag")

    assert len(payloads) == 10


def test_search_wrong_rk(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    with pytest.raises(DatasetNotFoundError):
        db.search_for_config("test")


def test_search_wrong_serial(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config("root_tag", object_type="optoboard", search_dict={"serial": "555"})

    assert len(payloads) == 0


def test_search_root(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config("root_tag", object_type="root", config_type="config")

    assert len(payloads) == 1
    assert payloads[0]["payloads"][0]["data"] == "root_node payload"


def test_search_without_object(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config("root_tag", search_dict={"serial": "123"}, config_type="lpgbt")

    assert len(payloads) == 1
    assert payloads[0]["payloads"][0]["data"] == "payload_lpgbt03"


def test_search_with_config(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config(
        "root_tag",
        object_type="optoboard",
        search_dict={"serial": "123"},
        config_type="lpgbt",
    )

    assert len(payloads) == 1
    assert payloads[0]["payloads"][0]["data"] == "payload_lpgbt03"


def test_search_multiple_results(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config(
        "root_tag",
        object_type="optoboard",
        config_type="lpgbt",
        search_dict={"serial": "100"},
    )

    assert len(payloads) == 2
    assert payloads[0]["payloads"][0]["data"].startswith("payload_lpgbt0")
    assert payloads[1]["payloads"][0]["data"].startswith("payload_lpgbt0")


def test_search_multiple_search(connection_meta_data: Fixture):
    db = connection_meta_data.stage_db

    payloads = db.search_for_config(
        "root_tag",
        object_type="optoboard",
        config_type="lpgbt",
        search_dict={"serial": "100", "test": "test"},
    )

    assert len(payloads) == 1
    assert payloads[0]["payloads"][0]["data"].startswith("payload_lpgbt01")


@pytest.mark.parametrize(
    "connection_search",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag(connection_search: Fixture):
    db = connection_search.stage_db

    payloads = db.search_in_tag(name="tag1", payload_types=["as", "ds"], search_dict={"serial": 1}, payload_data=True)

    assert len(payloads) == 2
    assert "data" in payloads[0]


@pytest.mark.parametrize(
    "connection_search",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag_order(connection_search: Fixture):
    db = connection_search.stage_db

    payloads = db.search_in_tag(name="tag1", payload_types=["as", "ds"], order_by_object=True)

    assert len(payloads) == 2
    assert len(list(iter(payloads.values()))[0]) == 2
    assert isinstance(list(iter(payloads.values()))[0], list)


@pytest.mark.parametrize(
    "connection_search",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag_order_payload(connection_search: Fixture):
    db = connection_search.stage_db

    objects = db.search_in_tag(name="tag1", payload_types=["as", "ds"], order_by_object=True, payload_data=True)
    payloads = list(iter(objects.values()))[0]

    assert len(objects) == 2
    assert len(payloads) == 2
    assert payloads["as"]["data"] == "as_data"
    assert payloads["ds"]["data"] == "ds_data"


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag_str(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()

    payl1 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as01"))
    payl2 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds01"))
    payl3 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts01"))

    payl4 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as02"))
    payl5 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds02"))
    payl6 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts02"))

    payl7 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as03"))
    payl8 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds03"))
    payl9 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts03"))

    meta1 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": "eins"}), meta=True))
    meta2 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": "zwei"}), meta=True))

    db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl1, payl2, payl3, meta1]))
    obj2 = db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl4, payl5, payl6, meta2]))

    db.backend.create_tag(write_session, TagFullModel(name="tag1", type="scans", author="pytest", payloads=[payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9]))

    write_session.close()

    payloads = db.search_in_tag(name="tag1", payload_types=["as", "ds"], search_dict={"serial": "zwei"})

    assert len(payloads) == 2
    assert payloads[0]["object_id"] == obj2


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag_obj_id(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()

    payl1 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as01"))
    payl2 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds01"))
    payl3 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts01"))

    payl4 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as02"))
    payl5 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds02"))
    payl6 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts02"))

    payl7 = db.backend.create_payload(write_session, PayloadFullModel(type="as", data="as_data", name="as03"))
    payl8 = db.backend.create_payload(write_session, PayloadFullModel(type="ds", data="ds_data", name="ds03"))
    payl9 = db.backend.create_payload(write_session, PayloadFullModel(type="ts", data="ts_data", name="ts03"))

    meta1 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": "eins"}), meta=True))
    meta2 = db.backend.create_payload(write_session, PayloadFullModel(type="meta", data=json.dumps({"serial": "zwei"}), meta=True))

    db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl1, payl2, payl3, meta1]))
    obj2 = db.backend.create_object(write_session, ObjectFullModel(type="frontend", payloads=[payl4, payl5, payl6, meta2]))

    db.backend.create_tag(write_session, TagFullModel(name="tag1", type="scans", author="pytest", payloads=[payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9]))

    write_session.close()

    payloads = db.search_in_tag(name="tag1", object_ids=[obj2], payload_data=False)

    assert len(payloads) == 3
    assert payloads[0]["object_id"] == obj2
    assert "payload_data" not in payloads[0]


@pytest.mark.parametrize(
    "connection_search",
    [Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_search_in_tag_full(connection_search: Fixture):
    db = connection_search.stage_db

    payloads = db.search_in_tag(name="tag1", payload_types=["as", "ds"])

    assert len(payloads) == 6
    for payload in payloads:
        assert "object_id" in payload
