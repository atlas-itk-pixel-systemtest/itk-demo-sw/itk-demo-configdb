## The configdb-server package
Documentation can be found [here](https://demi.docs.cern.ch/pypi-packages/configdb-server/).


## Installation and Setup
### Build & Publish

```shell
uv build
uv publish --index gitlab --token $(cat ~/.gitlab-token)
```

### Alembic
To create a new schema revision use (make sure sqlalchemy.url in alembic.ini is set properly):
```shell
cd configdb_server
alembic revision --autogenerate -m "comment"
```
To apply it use the:
```shell
alembic upgrade head
```
or use the function from the configdb-server package.