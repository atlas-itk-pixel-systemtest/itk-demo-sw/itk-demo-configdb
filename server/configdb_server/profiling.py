from sqlalchemy import text


class SQLAlchemyProfiling:
    def __init__(self, engine):
        self.engine = engine

    def enable_profiling(self):
        with self.engine.connect() as connection:
            connection.execute(text("SET profiling_history_size = 100;"))
            connection.execute(text("SET profiling = 1;"))

    def disable_profiling(self):
        with self.engine.connect() as connection:
            connection.execute(text("SET profiling = 0;"))

    def retrieve_profiling(self):
        with self.engine.connect() as connection:
            result = connection.execute(text("SHOW PROFILES;"))
            for row in result:
                print(row)

    def write_to_file(self, file_name="profiling.txt", mode="w"):
        with self.engine.connect() as connection:
            result = connection.execute(text("SHOW PROFILES;"))
            with open(file_name, mode) as f:
                for row in result:
                    f.write(str(row) + "\n")

    def profile_query(self, id, profile, file_name="profiling.txt", mode="w"):
        with self.engine.connect() as connection:
            result = connection.execute(text(f"SHOW PROFILE {profile} FOR QUERY {id};"))
        with open(file_name, mode) as f:
            for row in result:
                f.write(str(row) + "\n")   

    def clear_profiling(self):
        with self.engine.connect() as connection:
            connection.execute(text("SET profiling_history_size = 0;"))
            connection.execute(text("SET profiling = 0;"))
            connection.execute(text("SET profiling_history_size = 100;"))
            connection.execute(text("SET profiling = 1;"))

    def execute_query(self, query):
        with self.engine.connect() as connection:
            connection.execute(text(query))

    def execute_query_with_profiling(self, query):
        with self.engine.connect() as connection:
            connection.execute(text("SET profiling = 1;"))
            connection.execute(text(query))
            result = connection.execute(text("SHOW PROFILES;"))
            for row in result:
                print(row)
            connection.execute(text("SET profiling = 0;"))

class ConnectorProfiling:
    def __init__(self, cnx):
        self.cnx = cnx

    def enable_profiling(self):
        with self.cnx.cursor() as cursor:
            cursor.execute("SET profiling_history_size = 100;")
            cursor.execute("SET profiling = 1;")

    def disable_profiling(self):
        with self.cnx.cursor() as cursor:
            cursor.execute("SET profiling = 0;")

    def retrieve_profiling(self):
        with self.cnx.cursor() as cursor:
            cursor.execute("SHOW PROFILES;")
            for row in cursor:
                print(row)

    def write_to_file(self, file_name="profiling.txt", mode="w"):
        with self.cnx.cursor() as cursor:
            cursor.execute("SHOW PROFILES;")
            with open(file_name, mode) as f:
                for row in cursor:
                    f.write(str(row) + "\n")

    def clear_profiling(self):
        with self.cnx.cursor() as cursor:
            cursor.execute("SET profiling_history_size = 0;")
            cursor.execute("SET profiling = 0;")
            cursor.execute("SET profiling_history_size = 100;")
            cursor.execute("SET profiling = 1;")

    def execute_query(self, query):
        with self.cnx.cursor() as cursor:
            cursor.execute(query)

    def execute_query_with_profiling(self, query):
        with self.cnx.cursor() as cursor:
            cursor.execute("SET profiling = 1;")
            cursor.execute(query)
            cursor.execute("SHOW PROFILES;")
            for row in cursor:
                print(row)
            cursor.execute("SET profiling = 0;")
