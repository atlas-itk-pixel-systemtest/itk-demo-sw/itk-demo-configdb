from configdb_server.adapter.base_adapter import BaseAdapter
from configdb_server.adapter.sqlalchemy import (
    SQLAlchemyMariaDBAdapter,
    SQLAlchemyOracleAdapter,
    SQLAlchemyRecAdapter,
    SQLAlchemySQLiteAdapter,
    MariaDBAdapter,
    SQLAlchemyPostgresAdapter,
)
from configdb_server.exceptions import (
    DictTypeError,
    NameInUseError,
    DatasetNotFoundError,
    DictIDError,
    PayloadFormatError,
    PayloadTypeError,
    NotValidJSonError,
    ChangeFormatError,
    FeatureNotSupported,
)
from configdb_server.dataclasses import (
    TagFullModel,
    ObjectPayloadModel,
    ObjectChildListField,
    PayloadDataIDListField,
    FullTreeModel,
    ListsModel,
    CopyTreeModel,
    CompleteListsModel,
)
from configdb_server.tools import check_if_uuid
from configdb_server.logging import addLoggingLevelBenchmark
from enum import IntEnum, auto
from uuid import uuid4
from typing import ForwardRef
import json

Database = ForwardRef("Database")
FELIX_FILE = "controller.json"
ident_width = 8


class Backends(IntEnum):
    SQLALCHEMY_REC = auto()
    SQLALCHEMY_SQLITE = auto()
    SQLALCHEMY_MARIADB = auto()
    SQLALCHEMY_POSTGRES = auto()
    MARIADB = auto()
    SQLALCHEMY_ORACLE = auto()


class Database:
    """
    Class used to access the database via higher-level methods or directly on the adapter objects returned by get_db()
    Parameters
    ----------
    backend: Type to use for the backend database
    url: url of the backend database
    """

    backend: BaseAdapter

    def __init__(self, backend: Backends, url: str, adapter: str = None):
        """
        Parameters
        ----------
        backend: Type to use for the backend database
        url: url of the backend database
        """

        addLoggingLevelBenchmark()

        if adapter is None:
            adapter = self.backend_dict[backend][1]

        if backend == Backends.SQLALCHEMY_ORACLE:
            f = open("password.txt", "r")
            password = f.read()

            tnsname = "( DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=int8r-s.cern.ch) (PORT=10121) ) (LOAD_BALANCE=on) (ENABLE=BROKEN) (CONNECT_DATA= (SERVER=DEDICATED) (SERVICE_NAME=int8r.cern.ch) ) )"

            self.read_url = f"{adapter}://ATLAS_ITK_CONF_R:{password}@{tnsname}"
            self.write_url = f"{adapter}://ATLAS_ITK_CONF_W:{password}@{tnsname}"
            self.owner_url = f"{adapter}://ATLAS_ITK_CONF:{password}@{tnsname}"

            self.backend = self.backend_dict[backend][0](self.owner_url, self.read_url, self.write_url)
        else:
            self.url = f"{adapter}://{url}"
            self.backend = self.backend_dict[backend][0](self.url)

    # def __del__(self):
    #     del self.backend

    def init_database(self):
        """
        Initializes the backend database by creating it and applying the database schema
        """
        self.backend.create_database()
        self.backend.upgrade_database()

    # Advanced (Plumbing)
    def get_all(
        self,
        table: str,
        filter: str = "",
        name_filter: str = "",
        payload_filter: str = "",
        child_filter: str = "",
        offset: int = 0,
        limit: int = 0,
        order_by: str = "",
        asc: bool = True,
        connections: bool = False,
        payload_data: bool = False,
        depth: int = 0,
    ) -> "list[dict]":
        """
        Method to retrieve all datasets from a specified table

        Parameters
        ----------
        table: table that should be listed
        filter: type to filter by
        name_filter: name to filter by
        payload_filter: type of associated payload to filter by (only works for direct associations)
        child_filter: type of associated object to filter by (only works for direct associations)
        offset: offset for the listed datasets
        limit: limit for the listed datasets
        order_by: dataset attribute by which to order the list
        asc: set to True for ascending order, False for descending order
        payload_data: defines whether data of payload datasets should be included in the output
        depth: defines depth level to which the trees should be read (-1 for full tree)
        connections: defines whether to include the connections of the datasets (payloads and children)

        Returns
        -------
        List of dict containing datasets from the given table
        """
        read_session = self.backend.create_read_session()
        var = self.backend.read_table(
            read_session,
            table,
            filter=filter,
            name_filter=name_filter,
            payload_filter=payload_filter,
            child_filter=child_filter,
            offset=offset,
            limit=limit,
            order_by=order_by,
            asc=asc,
            connections=connections,
            payload_data=payload_data,
            depth=depth,
        )
        if table.lower() == "payload":
            var2 = self.backend.read_table(
                read_session,
                "metadata",
                filter=filter,
                payload_filter=payload_filter,
                child_filter=child_filter,
                offset=offset,
                limit=limit,
                order_by=order_by,
                asc=asc,
                connections=connections,
                payload_data=payload_data,
                depth=depth,
            )
            var.extend(var2)

        read_session.close()
        return var

    # staging area methods
    def clone(self, db: Database, copy: CopyTreeModel, keep_ids: bool = False) -> str:
        """
        Method to clone existing dataset/tree from the backend into the staging area

        Parameters
        ----------
        db: instance of database class to which to clone from
        identifier: name of the root tag or UUID of the root node
        name: name of the tag
        author: author of the tag
        comment: comment of the tag
        type: type of the tag
        view: view of the tree that should be cloned
        keep_ids: defines whether the UUIDs of the datasets should be kept

        Returns
        -------
        list ids of datasets that were cloned
        """

        if keep_ids and self.backend.__class__ == MariaDBAdapter:
            raise FeatureNotSupported

        if copy.name:
            read_session = self.backend.create_read_session()
            try:
                self.backend.read_tag(read_session, copy.name)
            except DatasetNotFoundError:
                pass
            else:
                raise NameInUseError(copy.name)
            read_session.close()

        read_session = db.backend.create_read_session()
        rk = db.backend.read_lists(read_session, copy.identifier, payload_data=True, decode=False, view=copy.view, ref_payloads=True)
        read_session.close()

        write_session = self.backend.create_write_session()
        root_ids, payload_ids = self.backend.write_lists(write_session, rk, encode=False, keep_ids=keep_ids)

        copy.payloads = payload_ids
        copy.objects = root_ids
        if copy.name:
            if not copy.type:
                if copy.identifier in rk.tags:
                    copy.type = rk.tags[copy.identifier].type
                else:
                    copy.type = "runkey"
            self.backend.create_tag(write_session, tag=copy)
        write_session.close()
        return root_ids

    def add_node(self, object: ObjectPayloadModel) -> str:
        """
        Method to add objects to an existing tree

        Parameters
        ----------
        object:

        Returns
        -------
        String containing the hex of the added node UUID
        """
        write_session = self.backend.create_write_session()
        ids = self.__add_payloads(write_session, object.payloads)
        object.payloads = ids
        obj_id = self.backend.create_object(write_session, object=object)
        write_session.close()
        return obj_id

    def add_to_node(
        self,
        id,
        children: list[ObjectChildListField] = [],
        payloads: list[PayloadDataIDListField] = [],
    ) -> str:
        """
        Method to add payloads/metadata to an existing object

        Parameters
        ----------
        id: uuid of the object to which the payload should be added
        payloads: list of payload dicts or UUIDs for this dataset
        children: list of connections to children with UUID strings of objects and their view
        payloads: list of payload dicts or UUIDs for this dataset

        Returns
        -------
        uuids of the added payloads
        """
        write_session = self.backend.create_write_session()
        ids = self.__add_payloads(write_session, payloads)
        self.backend.add_to_object(write_session, id, children=children, payloads=ids)
        write_session.close()
        return ids

    def __add_payloads(self, session, payloads: PayloadDataIDListField = []):
        payload_objs = []
        for payload in payloads:
            try:
                if isinstance(payload, str):
                    payload_objs.append(payload)
                else:
                    payl_id = self.backend.create_payload(write_session=session, payload=payload)
                    payload_objs.append(payl_id)

            except KeyError:
                raise DictTypeError

        return payload_objs

    def create_full_tree(self, tree: FullTreeModel) -> str:
        """
        Method to create a full tree based on the nested data dict
        Parameters
        ----------
        tree: FullTreeModel containing the tree data

        Returns
        -------
        String containing the hex of the root node UUID
        """

        if tree.name:
            read_session = self.backend.create_read_session()
            try:
                self.backend.read_tag(read_session, tree.name)
            except DatasetNotFoundError:
                pass
            else:
                raise NameInUseError(tree.name)
            read_session.close()

        write_session = self.backend.create_write_session()

        if tree.data:
            root_id = self.backend.write_full_tree(write_session, tree.data)
            objects = [root_id]
        else:
            root_id = None
            objects = []

        if tree.name is not None:
            tag = TagFullModel(
                name=tree.name,
                type=tree.type,
                objects=objects,
                author=tree.author,
                comment=tree.comment,
                payloads=tree.payloads,
            )

            self.backend.create_tag(write_session, tag=tag)

        write_session.close()
        return root_id

    def read_tree(
        self,
        identifier: str,
        payload_data: bool = False,
        payload_filter: str = "",
        decode: bool = True,
        format: bool = False,
        depth: int = -1,
        view: int = 1,
        resolve_payloads: Database = None,
    ):
        """
        Method to retrieve a tree from a tag

        Parameters
        ----------
        identifier: name of the tag or uuid of root node
        payload_data: adds data of payload to each dataset
        payload_filter: only include payloads which type contains this string
        decode: defines whether to decode the data of the payloads
        format: defines whether the data should be formatted
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)
        resolve_payloads: database to resolve payloads from

        Returns
        -------
        Dict containing the requested tag tree
        """

        read_session = self.backend.create_read_session()
        var = self.backend.read_tree(
            read_session,
            identifier,
            payload_data=payload_data,
            payload_filter=payload_filter,
            decode=decode,
            format=format,
            depth=depth,
            view=view,
            resolve_payloads=resolve_payloads,
        )
        read_session.close()
        return var

    def commit(
        self,
        db: Database,
        copy: CopyTreeModel,
        delete: bool = True,
        keep_ids: bool = False,
    ) -> str:
        """
        Method to commit and tag tree from staging area to database.
        Clears the committed tree from the staging area afterwards

        Parameters
        ----------
        db: instance of database class to which to commit
        identifier: name of the root tag or UUID of the root node
        name: name of the tag for the committed tree
        author: author of the tag
        comment: comment of the tag
        type: type of the tag
        view: view of the tree that should be committed
        delete: defines whether the committed tree should be deleted from the staging area
        keep_ids: defines whether the UUIDs of the datasets should be kept

        Returns
        -------
        String containing the name of the tag
        """

        if keep_ids and self.backend.__class__ == MariaDBAdapter:
            raise FeatureNotSupported

        if copy.name:
            read_session_ext = db.backend.create_read_session()
            try:
                db.backend.read_tag(read_session_ext, copy.name)
            except DatasetNotFoundError:
                pass
            else:
                raise NameInUseError(copy.name)
            read_session_ext.close()

        read_session = self.backend.create_read_session()
        rk = self.backend.read_lists(
            read_session,
            copy.identifier,
            payload_data=True,
            decode=False,
            view=copy.view,
        )
        read_session.close()

        write_session_ext = db.backend.create_write_session()

        root_ids, payload_ids = db.backend.write_lists(write_session_ext, rk, encode=False, keep_ids=keep_ids, resolve_payloads=True)
        copy.payloads = payload_ids
        copy.objects = root_ids
        # Fill commit history (make new tag)
        if copy.name:
            if not copy.type:
                if copy.identifier in rk.tags:
                    copy.type = rk.tags[copy.identifier].type
                else:
                    copy.type = "runkey"
            db.backend.create_tag(write_session_ext, tag=copy, tag_latest=True)
        write_session_ext.close()

        # delete tree from staging db afterwards
        if delete:
            write_session = self.backend.create_write_session()
            self.backend.delete_tree(write_session, copy.identifier)
            write_session.close()

        return root_ids

    def format_tag(self, name: str, include_id: bool = False, shorten_data: int = 10) -> str:
        """
        Method to format a tree into a string that can be printed

        Parameters
        ----------
        name: name of the tag
        include_id: defines whether to include the dataset ids in the output
        shorten_data: defines whether to shorten the data to a specific amount of characters (-1 to not shorten)

        Returns
        -------
        String containing the tree
        """
        read_session = self.backend.create_read_session()
        tag = self.backend.read_tag_tree(read_session, name=name, payload_data=True)
        read_session.close()

        output = ""

        if tag["author"]:
            output = f"author: {tag['author']}\n"

        if tag["objects"]:
            output = f"{output}objects:"

            for object in tag["objects"]:
                object_out = self.format_tree(object, include_id, shorten_data)
                output = f"{output}{object_out}"

            output = f"{output}\n"

        if tag["payloads"]:
            output = f"{output}payloads:"
            output = _extract_payload(tag, output, 0, include_id, shorten_data)

        return output

    def format_tree(self, tree: dict, include_id: bool = False, shorten_data: int = 10):
        """
        Method to format a tree into a string that can be printed

        Parameters
        ----------
        tree: tree as a dictionary
        include_id: defines whether to include the dataset ids in the output
        shorten_data: defines whether to shorten the data to a specific amount of characters (-1 to not shorten)

        Returns
        -------
        String containing the tree
        """

        output = _extract(tree, "", 0, include_id, shorten_data)

        return output

    def object_insert(self, list: "list[dict]") -> "list[str]":
        """
        Method to add multiple object with new payloads and existing children/parents

        Parameters
        ----------
        list: list containing object datasets to be added to the database

        Returns
        -------
        list containing the objects uuids as strings
        """

        id_list = []
        object_list = []
        payload_list = []
        metadata_list = []
        object_payload_list = []
        object_metadata_list = []
        closure_list = []

        read_session = self.backend.create_read_session()
        for object in list:
            try:
                object = object.model_dump()
            except AttributeError:
                pass
            ancestors = []
            descendants = []

            if "id" in object and object["id"]:
                object_id = object["id"]
            else:
                object_id = uuid4().hex
            id_list.append(object_id)
            try:
                type = object["type"]
            except KeyError:
                raise DictTypeError
            object_list.append({"type": type, "id": object_id})
            closure_list.append(
                {
                    "descendant_id": object_id,
                    "ancestor_id": object_id,
                    "depth": 0,
                    "view": 1,
                }
            )

            if "payloads" in object:
                for payload in object["payloads"]:
                    payload_id = uuid4().hex
                    payload_data = None
                    if "data" in payload:
                        payload_data = payload["data"]

                    try:
                        type = payload["type"]
                    except KeyError:
                        raise DictTypeError

                    meta = payload.get("meta", False)
                    if meta:
                        metadata_list.append(
                            {
                                "type": type,
                                "data": payload_data,
                                "name": payload.get("name"),
                                "id": payload_id,
                            }
                        )
                        object_metadata_list.append({"object_id": object_id, "metadata_id": payload_id})
                    else:
                        payload_list.append(
                            {
                                "type": type,
                                "data": payload_data,
                                "name": payload.get("name"),
                                "id": payload_id,
                            }
                        )
                        object_payload_list.append({"object_id": object_id, "payload_id": payload_id})

            if "parents" in object:
                for connection in object["parents"]:
                    if isinstance(connection, dict):
                        id = connection["id"]
                        view = connection["view"]
                    else:
                        id = connection
                        view = 1

                    if check_if_uuid(id) is False:
                        raise DictIDError
                    new_ancestors = self.backend.get_ancestors(read_session, id, 1, view)
                    ancestors.extend(new_ancestors)

            if "children" in object:
                for connection in object["children"]:
                    if isinstance(connection, dict):
                        id = connection["id"]
                        view = connection["view"]
                    else:
                        id = connection
                        view = 1

                    if check_if_uuid(id) is False:
                        raise DictIDError
                    new_descendants = self.backend.get_descendants(read_session, id, 1, view)
                    descendants.extend(new_descendants)

            for ancestor in ancestors:
                closure_list.append(
                    {
                        "descendant_id": object_id,
                        "ancestor_id": ancestor["id"],
                        "depth": ancestor["depth"],
                        "view": ancestor["view"],
                    }
                )

            for descendant in descendants:
                closure_list.append(
                    {
                        "descendant_id": descendant["id"],
                        "ancestor_id": object_id,
                        "depth": descendant["depth"],
                        "view": descendant["view"],
                    }
                )

                for ancestor in ancestors:
                    dataset = {
                        "descendant_id": descendant["id"],
                        "ancestor_id": ancestor["id"],
                        "depth": ancestor["depth"] + descendant["depth"],
                        "view": ancestor["view"],
                    }
                    if dataset not in closure_list:
                        closure_list.append(dataset)

        read_session.close()

        write_session = self.backend.create_write_session()
        self.backend.insert(write_session, "object", object_list)
        self.backend.insert(write_session, "payload", payload_list, encode=True)
        self.backend.insert(write_session, "object_payload", object_payload_list)
        self.backend.insert(write_session, "metadata", metadata_list)
        self.backend.insert(write_session, "object_metadata", object_metadata_list)
        self.backend.insert(write_session, "closure", closure_list)
        write_session.close()

        return id_list

    def search_for_config(
        self,
        identifier: str,
        object_type: str = "",
        config_type: str = "",
        search_dict: dict = {},
    ):
        """
        Method to get the payloads of an object of a specific type with specific metadata

        Parameters
        ----------
        identifier: name of the root tag or UUID of the root node
        object_type: type of the object to search for
        config_type: type of the config to search for
        search_dict: dict containing the metadata values to search for

        Returns
        -------
        list of objects and their payloads
        """

        read_session = self.backend.create_read_session()
        var = self.backend.search_for_config(read_session, identifier, object_type, config_type, search_dict)
        read_session.close()
        return var

    def search_for_subtree(
        self,
        identifier: str,
        object_type: str = "",
        search_dict: dict = {},
        payload_data: bool = False,
        decode: bool = True,
        depth: int = -1,
        view: int = 1,
    ):
        """
        Method to get the subtrees with a root node of a specific type with specific metadata

        Parameters
        ----------
        read_session: instance of a session for database access
        identifier: name of the root tag or UUID of the root node
        object_type: type of the object to search for
        search_dict: dict containing the metadata values to search for
        payload_data: adds data of payload to each dataset
        decode: defines whether to decode the data of the payloads
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        list of subtrees
        """

        read_session = self.backend.create_read_session()
        var = self.backend.search_for_subtree(
            read_session,
            identifier=identifier,
            object_type=object_type,
            search_dict=search_dict,
            payload_data=payload_data,
            decode=decode,
            depth=depth,
            view=view,
        )
        read_session.close()
        return var

    def search_in_tag(
        self,
        name: str,
        payload_types: list[str] = [],
        object_ids: list[str] = [],
        search_dict: dict = None,
        payload_data: bool = False,
        order_by_object: bool = False,
    ) -> list:
        """
        Gets all payloads with a specific type and object metadata from a tag

        Parameters
        ----------
        read_session: instance of a session for database access
        name: name of the tag
        payload_types: list of types to filter by
        object_metadata: dict containing the metadata values to filter by
        payload_data: adds data of payload to each dataset
        order_by_object: defines whether to order the payloads by object

        Returns
        -------
        List
        """
        read_session = self.backend.create_read_session()
        var = self.backend.search_in_tag(
            read_session,
            name=name,
            payload_types=payload_types,
            object_ids=object_ids,
            search_dict=search_dict,
            payload_data=payload_data,
            order_by_object=order_by_object,
        )
        read_session.close()
        return var

    def read_lists(self, name: str, payload_data: bool = False, decode: bool = True, depth: int = -1, view: int = 1, format: bool = False) -> ListsModel:
        """
        Method to retrieve lists from a tag

        Parameters
        ----------
        name: name of the tag
        payload_data: adds data of payload to each dataset
        decode: defines whether to decode the data of the payloads
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)
        format: defines whether the data should be formatted

        Returns
        -------
        ListsModel
        """
        read_session = self.backend.create_read_session()
        lists = self.backend.read_lists(read_session, identifier=name, payload_data=payload_data, decode=decode, depth=depth, view=view)
        read_session.close()

        closure = lists.closure
        payloads = lists.payloads
        objects = lists.objects
        for id, object in objects.items():
            object.children = [closure.descendant_id for closure in lists.closure if closure.depth == 1 and closure.ancestor_id == id]
            object.payloads = [payload.payload_id for payload in lists.object_payload if payload.object_id == id]
            object.payloads.extend([metadata.metadata_id for metadata in lists.object_metadata if metadata.object_id == id])
        tags = lists.tags
        for name, tag in tags.items():
            id = tag.id
            tag.objects = [object.object_id for object in lists.tag_object if object.tag_id == id]
            tag.payloads = [payload.payload_id for payload in lists.tag_payload if payload.tag_id == id]
            tag.payloads.extend([metadata.metadata_id for metadata in lists.tag_metadata if metadata.tag_id == id])
            tag.members = [member.member_id for member in lists.tag_tag if member.group_id == id]
            tag.groups = [group.group_id for group in lists.tag_tag if group.member_id == id]

        listsModel = ListsModel(payloads=payloads, objects=objects, tags=tags, closure=closure)
        return listsModel

    def write_lists(self, lists: ListsModel, encode: bool = True, keep_ids: bool = True, add_tag: bool = False) -> list[str]:
        """
        Method to write from lists

        Parameters
        ----------
        lists: ListsModel containing the lists to be written
        encode: defines whether payload data has to be encoded
        keep_ids: defines whether ids should be kept or new ones should be created
        add_tag: defines whether a tag should be added

        Returns
        -------
        Dict containing the requested tag tree
        """

        payloads = {}
        metadata = {}
        object_payload = []
        object_metadata = []
        for object_id, object in lists.objects.items():
            for payload_id in object.payloads:
                payload = lists.payloads[payload_id]
                if payload.meta:
                    object_metadata.append({"object_id": object_id, "metadata_id": payload_id})
                    metadata[payload_id] = payload
                else:
                    object_payload.append({"object_id": object_id, "payload_id": payload_id})
                    payloads[payload_id] = payload

        tag_object = []
        tag_payload = []
        tag_metadata = []
        tag_tag = []
        for tag_name, tag in lists.tags.items():
            tag_id = tag.id
            for payload_id in tag.payloads:
                payload = lists.payloads[payload_id]
                if payload.meta:
                    tag_metadata.append({"tag_id": tag_id, "metadata_id": payload_id})
                    metadata[payload_id] = payload
                else:
                    tag_payload.append({"tag_id": tag_id, "payload_id": payload_id})
                    payloads[payload_id] = payload
            for object_id in tag.objects:
                tag_object.append({"tag_id": tag_id, "object_id": object_id})
            for group_id in tag.groups:
                tag_tag.append({"group_id": tag_id, "member_id": group_id})
            for member_id in tag.members:
                tag_tag.append({"group_id": tag_id, "member_id": member_id})

        lists = CompleteListsModel(
            payloads=payloads,
            metadata=metadata,
            objects=lists.objects,
            tags=lists.tags,
            closure=lists.closure,
            object_payload=object_payload,
            object_metadata=object_metadata,
            tag_object=tag_object,
            tag_payload=tag_payload,
            tag_metadata=tag_metadata,
            tag_tag=tag_tag,
        )

        write_session = self.backend.create_write_session()
        root_ids, payload_ids = self.backend.write_lists(write_session, lists, encode=encode, keep_ids=keep_ids, add_tag=add_tag)
        write_session.close()

        return root_ids, payload_ids

    def bundle_payloads(self, payload_ids: list[str]):
        """
        Bundles payloads of the same type together into one ditctionary

        Parameters
        ----------
        payload_ids: list of UUID strings of payload datasets

        Returns
        -------
        dictinary containing the bundled payloads
        """

        read_session = self.backend.create_read_session()

        if len(payload_ids) < 1:
            return {}

        payl1 = self.backend.read_payload(read_session, payload_ids[0])

        try:
            data1 = json.loads(payl1["data"])
        except json.JSONDecodeError:
            raise NotValidJSonError(payl1["id"])

        bundled_data = _create_bundle(data1)

        for i, payload_id in enumerate(payload_ids[1:]):
            payl2 = self.backend.read_payload(read_session, payload_id)
            if payl1["type"] != payl2["type"]:
                raise PayloadTypeError(payl2["type"], payl1["type"], payl2["id"])

            try:
                data = json.loads(payl2["data"])
            except json.JSONDecodeError:
                raise NotValidJSonError(payl2["id"])

            bundled_data = _add_to_bundle(data, payload_ids[i + 1], payload_ids[0], bundled_data)

        read_session.close()
        return bundled_data

    def change_connected_payloads(self, associations: list[dict], changes: dict):
        """
        Bulk execution of changes to json payloads

        Parameters
        ----------
        association_dict: list of dicts containing assocations in form of payload id and object id
        changes: dict containing the changes to be made

        Returns
        -------
        List of ids of new payloads
        """
        read_session = self.backend.create_read_session()

        keys = {}
        new_payloads = []
        new_metadata = []
        res = []

        new_associations = []
        for association in associations:
            try:
                association = association.model_dump()
            except AttributeError:
                pass
            new_associations.append(association)
        associations = new_associations

        for association in associations:
            id = association["payload_id"]
            if id not in keys:
                payload = self.backend.read_payload(read_session, id)

                changed = False

                try:
                    data = json.loads(payload["data"])
                except json.JSONDecodeError:
                    raise NotValidJSonError(payload["id"])
                dic, changed = _recursive_update(data, changes)

                if changed:
                    new_id = uuid4().hex
                    association["new_id"] = new_id
                    payload["id"] = new_id
                    payload["data"] = json.dumps(data)
                    res.append(new_id)
                    if payload["meta"]:
                        new_metadata.append(payload)
                    else:
                        new_payloads.append(payload)
                else:
                    association["new_id"] = id

                keys[id] = association["new_id"]

            else:
                association["new_id"] = keys[id]

        read_session.close()

        write_session = self.backend.create_write_session()

        self.backend.insert(write_session=write_session, table="payload", list=new_payloads, encode=True)
        self.backend.insert(write_session, "metadata", new_metadata)

        for association in associations:
            if association["payload_id"] != association["new_id"]:
                self.backend.update_object_payload(
                    write_session,
                    association["object_id"],
                    association["payload_id"],
                    association["new_id"],
                )

        return res


def _create_bundle(data):
    if isinstance(data, dict):
        bundle = {}
        for key, value in data.items():
            if isinstance(value, dict) or isinstance(value, list):
                new_value = _create_bundle(data=value)
                bundle[key] = new_value
            else:
                bundle[key] = [value]
    elif isinstance(data, list):
        bundle = []
        for value in data:
            if isinstance(value, dict) or isinstance(value, list):
                new_value = _create_bundle(data=value)
                bundle.append(new_value)
            else:
                bundle.append([value])
    return bundle


def _add_to_bundle(data, id, first_id, bundle):
    # control = deepcopy(bundle)
    if isinstance(data, dict):
        for key, value in data.items():
            try:
                if isinstance(value, dict) or isinstance(value, list):
                    _add_to_bundle(data=value, id=id, first_id=first_id, bundle=bundle[key])
                else:
                    bundle[key].append(value)
                # control.pop(key)

            except KeyError:
                raise PayloadFormatError(id, first_id, [key])

    elif isinstance(data, list):
        for i, value in enumerate(data):
            try:
                if isinstance(value, dict):
                    _add_to_bundle(data=value, id=id, first_id=first_id, bundle=bundle[i])
                elif isinstance(value, list):
                    _add_to_bundle(data=value, id=id, first_id=first_id, bundle=bundle[i])
                else:
                    bundle[i].append(value)
                # control[i] = None

            except IndexError:
                raise PayloadFormatError(id, first_id, data)

    # if control is not None and any(item is not None for item in control):
    #     raise PayloadFormatError(first_id, id, list(control.keys()))

    return bundle


def _recursive_update(original_dict, changes_dict, change=False):
    for key, value in changes_dict.items():
        if key not in original_dict:
            original_value = value
            change = True
        else:
            original_value = original_dict[key]

        if isinstance(original_value, dict):  # dict change
            if not isinstance(value, dict):
                raise ChangeFormatError(str(type(value)).split("'")[1], "dict", f"{key} = {value}")
            original_dict[key], change = _recursive_update(original_value, value, change)
        elif isinstance(original_value, list):  # list change
            if isinstance(value, list):  # Add new list
                original_dict[key] = value
                change = True
                break
            elif not isinstance(value, dict):
                raise ChangeFormatError(str(type(value)).split("'")[1], "list", f"{key} = {value}")
            for index, value in value.items():
                index = int(index)
                if len(original_value) == index:
                    original_value.append(value)
                    change = True
                elif len(original_value) < index:
                    raise ChangeFormatError(text=f"Index of change: {index}={value} is out of range.")
                elif isinstance(value, dict):
                    new_value, change = _recursive_update(original_value[index], value, change)
                elif original_value[index] != value:
                    original_value[index] = value
                    change = True
        else:  # value change
            if isinstance(value, list) or isinstance(value, dict):
                raise ChangeFormatError("dict/list", "value", f"{key} = {value}")

            if key not in original_dict or original_dict[key] != value:
                original_dict[key] = value
                change = True

    return original_dict, change


def _extract(tree, output, ident, include_id, shorten_data):
    """
    Extract data from dict
    """

    output = f"{output}\n{' ' * ident * ident_width}{tree['type']} object:"
    ident = ident + 1
    if include_id is True:
        output = f"{output} {tree['id']}"

    output = _extract_payload(tree, output, ident, include_id, shorten_data)

    for dataset in tree["children"]:
        output = _extract(dataset, output, ident, include_id, shorten_data)
    return output


def _extract_payload(tree, output, ident, include_id, shorten_data):
    for dataset in tree["payloads"]:
        output = f"{output}\n{' ' * ident * ident_width}{dataset['type']} payload:"
        ident = ident + 1
        if include_id is True:
            output = f"{output} {dataset['id']}"
        if "data" in dataset:
            if shorten_data != 0:
                data = dataset["data"]
                if shorten_data > 0:
                    data = data[:shorten_data]
                output = f"{output}\n{' ' * ident * ident_width}data: {data}"
        if "name" in dataset:
            output = f"{output}\n{' ' * ident * ident_width}name: {dataset['name']}"
        if include_id is True:
            ident = ident - 1
    return output


Database.backend_dict = {
    Backends.SQLALCHEMY_MARIADB: (SQLAlchemyMariaDBAdapter, "mariadb+pymysql"),
    Backends.MARIADB: (MariaDBAdapter, "mariadb+pymysql"),
    Backends.SQLALCHEMY_SQLITE: (SQLAlchemySQLiteAdapter, "sqlite"),
    Backends.SQLALCHEMY_ORACLE: (SQLAlchemyOracleAdapter, "oracle+oracledb"),
    Backends.SQLALCHEMY_REC: (SQLAlchemyRecAdapter, "sqlite"),
    Backends.SQLALCHEMY_POSTGRES: (SQLAlchemyPostgresAdapter, "postgresql+psycopg2"),
}
