from pydantic import BaseModel, Field, AfterValidator
from typing_extensions import Annotated
from typing import TypeVar, Union, ForwardRef, Any, List, Optional
from uuid import uuid4, UUID
import datetime

TypeX = TypeVar("TypeX")  # for responses

ReuseObjectModel = ForwardRef("ReuseObjectModel")
ReuseConnectionModel = ForwardRef("ReuseConnectionModel")
ReuseModel = ForwardRef("ReuseModel")
ConnectionModel = ForwardRef("ConnectionModel")
ObjectRecursiveModel = ForwardRef("ObjectRecursiveModel")


def uuid_to_hex(v):
    if isinstance(v, UUID):
        return v.hex
    else:
        return v


def convert_id(v):
    if not isinstance(v, ConnectionModel):
        con = ConnectionModel(id=v)
        return con
    else:
        return v


def convert_ids(v):
    for i, value in enumerate(v):
        if not isinstance(value, ConnectionModel):
            v[i] = ConnectionModel(id=value)
    return v


IDFieldDef = Field(
    min_length=32,
    max_length=36,
    pattern="^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$",
    description="uuid of the dataset as a string without dashes",
    examples=[uuid4().hex],
)
NameFieldDef = Field(description="name of the tag", examples=["my_runkey_name"])

IDField = Annotated[Union[str, UUID], AfterValidator(uuid_to_hex), IDFieldDef]
ConnectionField = Annotated[Union[ConnectionModel, IDField], AfterValidator(convert_id), Field(description="connection to another object")]
ViewField = Annotated[int, Field(description="view of the tree (e.g.: 1 for default, 2 for stale, 3 for both)")]
PayloadIDListField = Annotated[List[IDField], Field(description="list of payload UUIDs")]
ObjectIDListField = Annotated[List[IDField], Field(description="list of object UUIDs")]
ObjectParentListField = Annotated[List[ConnectionField], Field(description="list of parent connections")]
ObjectChildListField = Annotated[List[ConnectionField], Field(description="list of children connections")]
TagTypeField = Annotated[str, Field(description="type of the tag", examples=["runkey"])]
TagNameField = Annotated[str, NameFieldDef]
TagNameListField = Annotated[List[TagNameField], Field(description="list of tag names")]
PayloadTypeField = Annotated[str, Field(description="type of the payload", examples=["configuration"])]

SearchField = Annotated[dict[str, Any], Field(description="dict with key-value pairs to search for")]


# Table Classes
class PayloadBaseModel(BaseModel):
    id: Optional[IDField] = None
    name: Annotated[Optional[str], Field(description="name of the payload", examples=["fe_config.json"])] = ""
    type: PayloadTypeField
    data: Annotated[Optional[Union[dict[str, Any], str, bytes]], Field(description="data of the payload", examples=["payload_data as string"])] = ""
    meta: Annotated[bool, Field(description="save payload as searchable metadata")] = False


class PayloadFullModel(PayloadBaseModel):
    objects: ObjectIDListField = []
    tags: TagNameListField = []


class ObjectBaseModel(BaseModel):
    id: Optional[IDField] = None
    type: Annotated[str, Field(description="type of the object", examples=["frontend"])]


class ObjectFullModel(ObjectBaseModel):
    parents: ObjectParentListField = []
    children: ObjectChildListField = []
    tags: TagNameListField = []
    payloads: PayloadIDListField = []


class TagBaseModel(BaseModel):
    id: Optional[IDField] = None
    name: TagNameField
    type: TagTypeField
    author: Annotated[str, Field(description="name of the author of the tag", examples=["Max Mustermann"])]
    comment: Annotated[Optional[str], Field(description="comment for the tag", examples=["comment text"])] = None
    time: Annotated[
        str,
        Field(description="time at which the tag was created", examples=["11:11 22.33.4444"], default=datetime.datetime.now(datetime.timezone.utc)),
    ]


class TagFullModel(TagBaseModel):
    objects: ObjectIDListField = []
    payloads: PayloadIDListField = []
    members: TagNameListField = []
    groups: TagNameListField = []


class ConnectionModel(BaseModel):
    id: IDField
    view: ViewField = 1


class ClosureTableModel(BaseModel):
    ancestor_id: IDField
    descendant_id: IDField
    depth: Annotated[int, Field(description="depth of the connection")]
    view: ViewField = 1


class ObjectPayloadTableModel(BaseModel):
    object_id: IDField
    payload_id: IDField


class ObjectMetadataTableModel(BaseModel):
    object_id: IDField
    metadata_id: IDField


class TagTagTableModel(BaseModel):
    group_id: IDField
    member_id: IDField


class TagObjectTableModel(BaseModel):
    tag_id: IDField
    object_id: IDField


class TagPayloadTableModel(BaseModel):
    tag_id: IDField
    payload_id: IDField


class TagMetadataTableModel(BaseModel):
    tag_id: IDField
    metadata_id: IDField


# Tree Classes

PayloadReuseField = Annotated[List[Union[ReuseModel, PayloadBaseModel]], Field(description="list of payload with option to reuse existing payloads")]
ObjectReuseField = Annotated[List[Union[ReuseConnectionModel, ReuseObjectModel]], Field(description="list of objects with option to reuse existing object")]
PayloadDataIDListField = Annotated[List[Union[PayloadFullModel, IDField]], Field(description="list of payload datasets or uuids")]
PayloadListField = Annotated[List[PayloadBaseModel], Field(description="list of payload UUIDs")]
ObjectListField = Annotated[List[ObjectRecursiveModel], Field(description="list of object UUIDs")]


class ListsModel(BaseModel):
    payloads: dict[IDField, PayloadFullModel] = {}
    objects: dict[IDField, ObjectFullModel] = {}
    tags: dict[str, TagFullModel] = {}
    closure: list[ClosureTableModel] = []


class CompleteListsModel(BaseModel):
    payloads: dict[IDField, PayloadBaseModel] = {}
    objects: dict[IDField, ObjectBaseModel] = {}
    tags: dict[str, TagBaseModel] = {}
    metadata: dict[IDField, PayloadBaseModel] = {}
    closure: list[ClosureTableModel] = []
    tag_tag: list[TagTagTableModel] = []
    tag_object: list[TagObjectTableModel] = []
    tag_payload: list[TagPayloadTableModel] = []
    tag_metadata: list[TagMetadataTableModel] = []
    object_payload: list[ObjectPayloadTableModel] = []
    object_metadata: list[ObjectMetadataTableModel] = []


class ObjectPayloadModel(ObjectFullModel):
    tags: TagNameListField = []
    payloads: PayloadDataIDListField = []
    parents: ObjectParentListField = []


class ReuseModel(BaseModel):
    reuse_id: IDField


class ReuseConnectionModel(ReuseModel):
    view: ViewField = 1


class ReuseObjectModel(ObjectBaseModel):
    children: ObjectReuseField = []
    payloads: PayloadReuseField = []
    view: ViewField = 1


class ObjectRecursiveModel(ReuseObjectModel):
    children: ObjectListField = []
    payloads: PayloadListField = []


class FullTreeModel(TagBaseModel):
    name: Annotated[Optional[str], NameFieldDef] = None
    data: ReuseObjectModel = None
    type: TagTypeField = "runkey"
    payloads: PayloadIDListField = []


# HTTP Bodies
class PayloadUpdateModel(PayloadBaseModel):
    id: IDField
    type: Optional[PayloadTypeField] = None


class CopyTreeModel(TagFullModel):
    identifier: Annotated[str, Field(description="name of tag")]
    view: ViewField = 1
    type: TagTypeField = ""


class InsertObjectsModel(BaseModel):
    list: Annotated[
        List[ObjectPayloadModel],
        Field(description="list of object datasets which can contain payload datasets"),
    ]


class InsertModel(BaseModel):
    list: Annotated[
        List[Union[ObjectFullModel, PayloadFullModel, TagFullModel, ClosureTableModel, TagTagTableModel, TagObjectTableModel, TagPayloadTableModel]],
        Field(description="list of any datasets to put into the database"),
    ]


class AddToNodeModel(BaseModel):
    children: ObjectChildListField = []
    payloads: PayloadDataIDListField = []


class SearchModel(BaseModel):
    search_dict: SearchField


class OptionalSearchModel(BaseModel):
    name: str = ""
    search_dict: SearchField = None
    payload_types: Annotated[List[PayloadTypeField], Field(description="List of payload types to search for")] = []
    object_ids: Annotated[List[IDField], Field(description="List of payload types to search for")] = []


class UpdateObjectConnectionModel(BaseModel):
    parent: IDField
    child: IDField
    view: ViewField


class UpdateConnectionsModel(BaseModel):
    connections: Annotated[
        List[UpdateObjectConnectionModel],
        Field(description="list of parent-child connections and their new view value"),
    ]


class UpdatePayloadModel(BaseModel):
    associations: Annotated[
        List[ObjectPayloadTableModel],
        Field(description="list of object-payload associations for which to update paylaod"),
    ]
    changes: Annotated[
        dict[str, Any],
        Field(description="dict with data to update in payload"),
    ]


# HTTP Responses
class StandardErrorResponseModel(BaseModel):
    status: Annotated[int, Field(description="status code", default=400)]
    title: Annotated[str, Field(description="title of the error code", examples=["Dataset not found"])]
    detail: Annotated[
        str,
        Field(description="describtion of the error code", examples=["Task failed, one or more of the requested/given datasets do not exist."]),
    ]
    instance: Annotated[
        str,
        Field(description="location of the error in the code", examples=["rcf_response.response - wrapper (l. 29)"]),
    ]
    status_id: int
    type: str


class StatusResponseModel(BaseModel):
    status: Annotated[int, Field(description="status code", default=200)]


class StandardResponseModel(StatusResponseModel):
    id: IDField


class StandardMultiResponseModel(StatusResponseModel):
    ids: list[IDField]


class TagResponseModel(StandardResponseModel, TagFullModel):
    pass


class ObjectResponseModel(StandardResponseModel, ObjectFullModel):
    pass


class PayloadResponseModel(StandardResponseModel, PayloadFullModel):
    pass


class ObjectTreeResponse(ObjectResponseModel):
    children: ObjectListField = []
    payloads: PayloadListField = []


class ObjectSearchReponseModel(ObjectBaseModel):
    payloads: PayloadDataIDListField = []


class SearchResponse(StatusResponseModel):
    data: Annotated[List[ObjectSearchReponseModel], Field(description="list of found objects and their payloads")]


class PayloadSearchReponseModel(PayloadBaseModel):
    object_id: IDField


class SearchTagResponse(StatusResponseModel):
    data: Annotated[List[PayloadSearchReponseModel], Field(description="list of found payloads and their object ids")]


class FormatingResponseModel(StatusResponseModel):
    string: str


class TagTreeResponseModel(TagResponseModel):
    objects: ObjectListField = []
    payloads: PayloadListField = []


class SearchSubtreeResponseModel(StatusResponseModel):
    data: ObjectListField = []


class BundleResponseModel(StatusResponseModel):
    bundle: dict[str, List[Any]]


class GetAllResponseModel(StatusResponseModel):
    list: list[Union[PayloadFullModel, TagFullModel, ObjectFullModel]]


class HealthResponseModel(StatusResponseModel):
    state: str
