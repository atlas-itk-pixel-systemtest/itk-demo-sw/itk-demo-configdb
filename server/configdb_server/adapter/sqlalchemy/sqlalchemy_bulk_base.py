from configdb_server.adapter.sqlalchemy.sqlalchemy_base import (
    SQLAlchemyBaseAdapter,
    tables,
)
from configdb_server.models.sqlalchemy.models import (
    object_payload,
    object_metadata,
    tag_object,
    tag_tag,
    tag_payload,
    tag_metadata,
)
from configdb_server.models.sqlalchemy.payload import Payload
from configdb_server.models.sqlalchemy.metadata import Metadata
from configdb_server.models.sqlalchemy.object import Object
from configdb_server.models.sqlalchemy.tag import Tag
from configdb_server.models.sqlalchemy.models import ObjectClosure
from configdb_server.tools import decompressBytes, compressToBytes, check_if_uuid, map_uuids, map_uuids_ref
from configdb_server.dataclasses import (
    CompleteListsModel,
    ObjectFullModel,
    PayloadFullModel,
    TagFullModel,
    TagTagTableModel,
    TagObjectTableModel,
    TagPayloadTableModel,
    TagMetadataTableModel,
    ObjectMetadataTableModel,
    ObjectPayloadTableModel,
    ClosureTableModel,
)
from sqlalchemy.exc import IntegrityError
from configdb_server.exceptions import (
    DatasetNotFoundError,
    WrongTableError,
    IDInUseError,
    DictTypeError,
    NotValidJSonError,
    NotValidUUIDError,
)
from sqlalchemy import insert, select, and_, delete, not_, exists
from sqlalchemy.orm import aliased
from collections import OrderedDict, defaultdict
from typing import TYPE_CHECKING

from uuid import uuid4, UUID
from abc import ABC
from copy import deepcopy
from typing import Union
from datetime import datetime

import time
import logging
import json

if TYPE_CHECKING:
    from configdb_server.database_tools import Database


class SQLAlchemyBulkAdapter(SQLAlchemyBaseAdapter, ABC):
    def __resolve_payloads(self, payloads, resolve_payloads: "Database", meta=False):
        resolve_map = {}

        for i, payload in enumerate(payloads):
            if "#ref#" in payload.name:
                resolve_map[i] = payload.name[5:]
        if meta:
            stmt = select(Metadata.id, Metadata.name, Metadata.type, Metadata.data).where(Metadata.id.in_(list(resolve_map.values())))
        else:
            stmt = select(Payload.id, Payload.name, Payload.type, Payload.data).where(Payload.id.in_(list(resolve_map.values())))
        read_session = resolve_payloads.backend.create_read_session()
        resolved_payloads = read_session.execute(stmt).all()
        read_session.close()
        resolved_payloads_dict = {payload.id.hex: payload for payload in resolved_payloads}

        for index, id in resolve_map.items():
            payload = payloads[index]._asdict()
            payload["data"] = resolved_payloads_dict[id].data
            payload["name"] = resolved_payloads_dict[id].name
            payloads[index] = payload
        return payloads

    def read_object_tree(
        self,
        read_session,
        id,
        payload_data=False,
        payload_filter: str = "",
        decode=True,
        format=False,
        depth=-1,
        view=1,
        resolve_payloads: "Database" = None,
    ) -> dict:
        start = time.perf_counter()

        root_id = id

        if depth != -1:
            join_filter = and_(
                Object.id == ObjectClosure.descendant_id,
                ObjectClosure.ancestor_id == root_id,
                ObjectClosure.depth <= depth,
            )
        else:
            join_filter = and_(
                Object.id == ObjectClosure.descendant_id,
                ObjectClosure.ancestor_id == root_id,
            )

        parent = aliased(ObjectClosure)
        stmt = (
            select(
                ObjectClosure.descendant_id,
                parent.ancestor_id,
                Object.type,
                parent.view,
            )
            .join(Object, join_filter)
            .join(
                parent,
                and_(
                    parent.descendant_id == ObjectClosure.descendant_id,
                    ((parent.depth == 1) & (parent.view.op("&")(view) > 0)) | (ObjectClosure.depth == 0),
                ),
            )
            .order_by(ObjectClosure.descendant_id)
        )

        objects = read_session.execute(stmt).all()

        id_list = [lst.descendant_id for lst in objects]

        payload_columns = [object_payload.c.object_id, Payload.id, Payload.name, Payload.type]

        # Add the 'data' column if payload_data is True
        if payload_data:
            payload_columns.append(Payload.data)

        if payload_filter:
            stmt = (
                select(*payload_columns)
                .join(
                    object_payload,
                    and_(
                        object_payload.c.payload_id == Payload.id,
                        object_payload.c.object_id.in_(id_list),
                        Payload.type.contains(payload_filter),
                    ),
                )
                .order_by(object_payload.c.object_id, Payload.id)
            )
        else:
            stmt = (
                select(*payload_columns)
                .join(
                    object_payload,
                    and_(
                        object_payload.c.payload_id == Payload.id,
                        object_payload.c.object_id.in_(id_list),
                    ),
                )
                .order_by(object_payload.c.object_id, Payload.id)
            )

        payloads = read_session.execute(stmt).all()
        if resolve_payloads and payload_data:
            payloads = self.__resolve_payloads(payloads, resolve_payloads)

        stmt = (
            select(
                object_metadata.c.object_id,
                Metadata.id,
                Metadata.name,
                Metadata.type,
                Metadata.data,
            )
            .join(
                object_metadata,
                and_(
                    object_metadata.c.metadata_id == Metadata.id,
                    object_metadata.c.object_id.in_(id_list),
                ),
            )
            .order_by(object_metadata.c.object_id, Metadata.id)
        )

        meta = read_session.execute(stmt).all()
        if resolve_payloads:
            meta = self.__resolve_payloads(meta, resolve_payloads, meta=True)

        query_time = time.perf_counter()
        logging.benchmark(f"Read query: {round(query_time - start, 3)}")
        logging.info(f"Executed query in {round(query_time - start, 3)} seconds.")

        data = {}

        # Creates flat dict of all datasets
        for record in objects:
            id = record.descendant_id.hex
            if id not in data:
                data[id] = {
                    "children": set(),
                    "payloads": [],
                    "metadata": {},
                    "type": record.type,
                    "id": id,
                }

        for record in payloads:
            if not isinstance(record, dict):
                object_id = record.object_id.hex
                if payload_data:
                    payload_data = record.data
                payload = {"id": record.id.hex, "name": record.name, "type": record.type}
            else:
                object_id = record["object_id"].hex
                if payload_data:
                    payload_data = record["data"]
                payload = {"id": record["id"].hex, "name": record["name"], "type": record["type"]}
            if payload_data:
                if decode:
                    temp_data = decompressBytes(payload_data)
                    if format:
                        try:
                            temp_data = json.loads(temp_data)
                            temp_data = json.dumps(temp_data, indent=4)
                        except json.JSONDecodeError:
                            pass
                    payload["data"] = temp_data
                else:
                    payload["data"] = record.data

            data[object_id]["payloads"].append(payload)

        for record in meta:
            if not isinstance(record, dict):
                object_id = record.object_id.hex
                meta_data = record.data
                meta = {"id": record.id.hex, "name": record.name, "type": record.type}
            else:
                object_id = record["object_id"].hex
                meta_data = record["data"]
                meta = {"id": record["id"].hex, "name": record["name"], "type": record["type"]}
            if not meta_data:
                meta_data = {}
            if format:
                temp_data = json.dumps(meta_data, indent=4)
            else:
                temp_data = json.dumps(meta_data)
            meta["data"] = temp_data
            meta["meta"] = True

            data[object_id]["payloads"].append(meta)
            data[object_id]["metadata"].update(meta_data)

        # Adds children id to parents
        for record in objects:
            id = record.descendant_id.hex
            ancestor = record.ancestor_id.hex
            if ancestor and ancestor != id:
                try:
                    data[ancestor]["children"].add((id, record.view))
                except KeyError:
                    pass

        # Adds children dataset to parents
        # for value in data.values():
        #     new_children = []
        #     for child, view in value["children"]:
        #         child_dict = data[child]
        #         child_dict["view"] = view
        #         new_children.append(child_dict)

        #     value["children"] = new_children

        def __create_tree(data, node):
            new_children = []
            for child in node["children"]:
                child_id = child[0]
                child_view = child[1]
                child_node = deepcopy(data[child_id])
                child_node["view"] = child_view
                child_dict = __create_tree(data, child_node)
                new_children.append(child_dict)

            node["children"] = new_children
            return node

        if isinstance(root_id, UUID):
            root_id = root_id.hex
        if root_id not in data:
            raise DatasetNotFoundError
        root = data[root_id]
        tree = __create_tree(data, root)

        compute_time = time.perf_counter()
        logging.benchmark(f"Runkey construction: {round(compute_time - query_time, 3)}")
        logging.info(f"Constructed runkey in {round(compute_time - query_time, 3)} seconds.")

        return tree

    def read_tag_tree(
        self,
        read_session,
        name,
        payload_data=False,
        payload_filter="",
        decode=True,
        format=False,
        depth=-1,
        view=1,
        resolve_payloads: "Database" = None,
    ) -> dict:
        stmt = select(Tag.id, Tag.name, Tag.author, Tag.type, Tag.time, Tag.comment).where(Tag.name == name)
        tag_data = read_session.execute(stmt).first()

        if not tag_data:
            raise DatasetNotFoundError

        tag_id = tag_data.id

        stmt = select(Tag.name).join(tag_tag, and_(Tag.id == tag_tag.c.group_id, tag_tag.c.member_id == tag_id))
        groups_data = read_session.execute(stmt)

        stmt = select(Tag.name).join(tag_tag, and_(Tag.id == tag_tag.c.member_id, tag_tag.c.group_id == tag_id))
        members_data = read_session.execute(stmt)

        if not tag_data:
            raise DatasetNotFoundError

        payload_columns = [Payload.id, Payload.name, Payload.type]

        # Add the 'data' column if payload_data is True
        if payload_data:
            payload_columns.append(Payload.data)

        if payload_filter:
            stmt = select(*payload_columns).join(
                tag_payload,
                and_(
                    Payload.type.contains(payload_filter),
                    tag_payload.c.tag_id == tag_id,
                    Payload.id == tag_payload.c.payload_id,
                ),
            )
        else:
            stmt = select(*payload_columns).join(
                tag_payload,
                and_(
                    tag_payload.c.tag_id == tag_id,
                    Payload.id == tag_payload.c.payload_id,
                ),
            )

        payloads = read_session.execute(stmt).all()
        if resolve_payloads and payload_data:
            payloads = self.__resolve_payloads(payloads, resolve_payloads)

        stmt = select(Metadata.id, Metadata.name, Metadata.type, Metadata.data).join(
            tag_metadata,
            and_(
                Metadata.id == tag_metadata.c.metadata_id,
                tag_metadata.c.tag_id == tag_id,
            ),
        )
        metas = read_session.execute(stmt).all()
        if resolve_payloads:
            meta = self.__resolve_payloads(metas, resolve_payloads, meta=True)

        stmt = select(tag_object.c.object_id).where(tag_object.c.tag_id == tag_id)
        objects = read_session.execute(stmt).all()

        tag = {
            "payloads": [],
            "objects": [],
            "members": [],
            "groups": [group.name for group in groups_data],
            "metadata": {},
            "id": tag_data.id.hex,
            "name": tag_data.name,
            "author": tag_data.author,
            "type": tag_data.type,
            "time": tag_data.time,
            "comment": tag_data.comment,
        }
        for record in payloads:
            if not isinstance(record, dict):
                if payload_data:
                    payload_data = record.data
                payload = {"id": record.id.hex, "name": record.name, "type": record.type}
            else:
                if payload_data:
                    payload_data = record["data"]
                payload = {"id": record["id"].hex, "name": record["name"], "type": record["type"]}
            if payload_data:
                if decode:
                    payload["data"] = decompressBytes(payload_data)
                else:
                    payload["data"] = payload_data
            tag["payloads"].append(payload)

        for member in members_data:
            tag["members"].append(self.read_tag_tree(read_session, member.name, payload_data, payload_filter, decode, format, depth, view, resolve_payloads))

        for record in metas:
            if not isinstance(record, dict):
                meta_data = record.data
                meta = {"id": record.id.hex, "name": record.name, "type": record.type}
            else:
                meta_data = record["data"]
                meta = {"id": record["id"].hex, "name": record["name"], "type": record["type"]}
            if not meta_data:
                meta_data = {}
            if format:
                temp_data = json.dumps(meta_data, indent=4)
            else:
                temp_data = json.dumps(meta_data)
            meta["data"] = temp_data
            meta["meta"] = True

            tag["payloads"].append(meta)
            tag["metadata"].update(meta_data)

        for object in objects:
            id = object.object_id.hex
            tree = self.read_object_tree(
                read_session=read_session,
                id=id,
                payload_data=payload_data,
                payload_filter=payload_filter,
                decode=decode,
                format=format,
                depth=depth,
                view=view,
                resolve_payloads=resolve_payloads,
            )
            tag["objects"].append(tree)

        return tag

    def delete_object_tree(self, write_session, id):
        select_stmt = select(ObjectClosure.descendant_id).where(ObjectClosure.ancestor_id == id)
        objects = write_session.execute(select_stmt).all()
        object_id_list = [lst.descendant_id.hex for lst in objects]

        op = aliased(object_payload)
        stmt = select(object_payload.c.payload_id).where(
            and_(
                object_payload.c.object_id.in_(object_id_list),
                not_(
                    exists().where(
                        and_(
                            op.c.payload_id == object_payload.c.payload_id,
                            op.c.object_id.notin_(object_id_list),
                        )
                    )
                ),
            )
        )
        payloads = write_session.execute(stmt).all()
        payload_id_list = [lst.payload_id.hex for lst in payloads]

        om = aliased(object_metadata)
        stmt = select(object_metadata.c.metadata_id).where(
            and_(
                object_metadata.c.object_id.in_(object_id_list),
                not_(
                    exists().where(
                        and_(
                            om.c.metadata_id == object_metadata.c.metadata_id,
                            om.c.object_id.notin_(object_id_list),
                        )
                    )
                ),
            )
        )
        metadatas = write_session.execute(stmt).all()
        metadata_id_list = [lst.metadata_id for lst in metadatas]

        stmt = delete(Object).where(Object.id.in_(object_id_list))
        write_session.execute(stmt)

        stmt = delete(Payload).where(Payload.id.in_(payload_id_list))
        write_session.execute(stmt)

        stmt = delete(Metadata).where(Metadata.id.in_(metadata_id_list))
        write_session.execute(stmt)

        write_session.commit()

    def delete_tag_tree(self, write_session, name):
        stmt = select(Tag.id).where(Tag.name == name)
        tag = write_session.execute(stmt).first()

        if tag is None:
            raise DatasetNotFoundError
        tag_id = tag.id

        stmt = select(tag_object.c.object_id).where(tag_object.c.tag_id == tag_id)
        objects = write_session.execute(stmt).all()
        for object in objects:
            self.delete_object_tree(write_session, object.object_id)

        tp = aliased(tag_payload)
        stmt = select(tag_payload.c.payload_id).where(
            and_(
                tag_payload.c.tag_id == tag_id,
                not_(
                    exists().where(
                        and_(
                            tp.c.payload_id == tag_payload.c.payload_id,
                            tp.c.tag_id != tag_id,
                        )
                    )
                ),
            )
        )
        payloads = write_session.execute(stmt).all()
        payload_id_list = [lst.payload_id for lst in payloads]

        tm = aliased(tag_metadata)
        stmt = select(tag_metadata.c.metadata_id).where(
            and_(
                tag_metadata.c.tag_id == tag_id,
                not_(
                    exists().where(
                        and_(
                            tm.c.metadata_id == tag_metadata.c.metadata_id,
                            tm.c.tag_id != tag_id,
                        )
                    )
                ),
            )
        )
        metadatas = write_session.execute(stmt).all()
        metadata_id_list = [lst.metadata_id for lst in metadatas]

        stmt = delete(Tag).where(Tag.name == name)
        write_session.execute(stmt)

        stmt = delete(Payload).where(Payload.id.in_(payload_id_list))
        write_session.execute(stmt)

        stmt = delete(Metadata).where(Metadata.id.in_(metadata_id_list))
        write_session.execute(stmt)

        write_session.commit()

    def delete_tree(self, write_session, identifier) -> int:
        if check_if_uuid(identifier):
            self.delete_object_tree(write_session, identifier)
        else:
            self.delete_tag_tree(write_session, identifier)

    def insert(self, write_session, table, list, encode=False):
        table = table.lower()
        try:
            tbl_cls = tables[table]
        except KeyError:
            raise WrongTableError

        if list:
            new_list = []
            for dataset in list:
                try:
                    dataset = dataset.model_dump()
                except AttributeError:
                    pass
                new_list.append(dataset)

            if table == "payload" and encode:
                for dataset in new_list:
                    if dataset["data"]:
                        dataset["data"] = compressToBytes(dataset["data"])

            if table == "metadata":
                for dataset in new_list:
                    if isinstance(dataset["data"], str):
                        try:
                            dataset["data"] = json.loads(dataset["data"])
                        except json.JSONDecodeError:
                            raise NotValidJSonError

            if table == "tag":
                for tag in new_list:
                    tag["time"] = datetime.fromisoformat(tag["time"])

            try:
                write_session.execute(insert(tbl_cls), new_list)
            except IntegrityError as e:
                write_session.rollback()
                if str(e.orig).startswith("UNIQUE constraint failed:") or "uplicate" in str(e.orig):
                    raise IDInUseError
                else:
                    raise DatasetNotFoundError

            try:
                write_session.commit()
            except IntegrityError:
                write_session.rollback()
                raise IDInUseError

        # return ids
        # {"dml_strategy": "raw"}

    def write_full_tree(
        self,
        write_session,
        data: "list[dict]",
        encode: bool = True,
        keep_ids: bool = True,
    ) -> str:
        object_list = []
        payload_list = []
        metadata_list = []
        object_payload_list = []
        closure_list = []
        object_metadata_list = []
        payload_ids = []
        metadata_ids = []

        try:
            data = data.model_dump()
        except AttributeError:
            pass

        def __connect_ancestors(object_id, ancestors, depth, view):
            for ancestor, ancestor_depth in ancestors:
                new_depth = depth - ancestor_depth
                if new_depth != 1:
                    view = 1
                if new_depth >= 0:
                    closure_list.append(
                        {
                            "descendant_id": object_id,
                            "ancestor_id": ancestor,
                            "depth": new_depth,
                            "view": view,
                        }
                    )

        def __convert_to_list(data: dict, depth: int, ancestors: list):
            # reuse object
            if "reuse_id" in data:
                object_id = data["reuse_id"]
                if not check_if_uuid(object_id):
                    raise NotValidUUIDError
                conn = (object_id, depth)
                ancestors.append(conn)
                __connect_ancestors(object_id, ancestors, depth, data.get("view", 1))
            # create new object
            else:
                if "id" in data and keep_ids and data["id"]:
                    object_id = data["id"]
                    if not check_if_uuid(object_id):
                        raise NotValidUUIDError
                else:
                    object_id = uuid4().hex
                try:
                    type = data["type"]
                except KeyError:
                    raise DictTypeError
                object_list.append({"type": type, "id": object_id})
                conn = (object_id, depth)
                ancestors.append(conn)
                __connect_ancestors(object_id, ancestors, depth, data.get("view", 1))

                if "payloads" in data:
                    for payload in data["payloads"]:
                        meta = payload.get("meta", False)
                        # reuse payload
                        if "reuse_id" in payload:
                            payload_id = payload["reuse_id"]
                            if not check_if_uuid(payload_id):
                                raise NotValidUUIDError
                            if meta:
                                object_metadata_list.append({"object_id": object_id, "metadata_id": payload_id})
                            else:
                                object_payload_list.append({"object_id": object_id, "payload_id": payload_id})
                        # create new payload
                        else:
                            if "id" in payload and keep_ids and payload["id"]:
                                payload_id = payload["id"]
                                if not check_if_uuid(payload_id):
                                    raise NotValidUUIDError
                            else:
                                payload_id = uuid4().hex
                            if meta:
                                metadata_list.append(
                                    {
                                        "type": payload["type"],
                                        "data": payload.get("data"),
                                        "name": payload.get("name"),
                                        "id": payload_id,
                                    }
                                )
                                metadata_ids.append(payload_id)
                                object_metadata_list.append({"object_id": object_id, "metadata_id": payload_id})
                            else:
                                payload_list.append(
                                    {
                                        "type": payload["type"],
                                        "data": payload.get("data"),
                                        "name": payload.get("name"),
                                        "id": payload_id,
                                    }
                                )
                                payload_ids.append(payload_id)
                                object_payload_list.append({"object_id": object_id, "payload_id": payload_id})

                if "children" in data:
                    depth = depth + 1
                    for dataset in data["children"]:
                        __convert_to_list(dataset, depth, ancestors)
                    depth = depth - 1

            ancestors.remove(conn)
            return object_id

        root_id = __convert_to_list(data, 0, [])

        closure_set = set(tuple(d.items()) for d in closure_list)
        closure_list = [dict(t) for t in closure_set]

        self.insert(write_session, "object", object_list)
        self.insert(write_session, "payload", payload_list, encode=encode)
        self.insert(write_session, "metadata", metadata_list)
        self.insert(write_session, "object_payload", object_payload_list)
        self.insert(write_session, "object_metadata", object_metadata_list)
        self.insert(write_session, "closure", closure_list)

        return root_id

    def read_lists(
        self,
        read_session,
        identifier: str,
        payload_data: bool = False,
        decode: bool = True,
        depth: int = -1,
        view: int = 1,
        ref_payloads: bool = False,
    ) -> CompleteListsModel:
        is_tag = False
        if not check_if_uuid(identifier):
            tag = self.read_tag(read_session, identifier)
            identifiers = tag["objects"]
            is_tag = True
            tag_dict = {tag["name"]: TagFullModel(**tag)}
        else:
            tag = self.read_object(read_session, identifier)
            identifiers = [identifier]
            tag_dict = {}
            tag["objects"] = identifiers

        if depth != -1:
            join_filter = and_(ObjectClosure.ancestor_id.in_(identifiers), ObjectClosure.depth <= depth)
        else:
            join_filter = and_(ObjectClosure.ancestor_id.in_(identifiers))

        parent = aliased(ObjectClosure)
        stmt = (
            select(
                parent.ancestor_id,
                ObjectClosure.descendant_id,
                parent.depth,
                parent.view,
            )
            .join(
                parent,
                and_(
                    join_filter,
                    parent.descendant_id == ObjectClosure.descendant_id,
                    (parent.view.op("&")(view) > 0) | (ObjectClosure.depth == 0),
                ),
            )
            .distinct()
        )

        result = read_session.execute(stmt).all()
        closure_list = [row._asdict() for row in result]
        object_ids = list(OrderedDict.fromkeys(lst["ancestor_id"] for lst in closure_list))

        stmt = select(Object.id, Object.type).where(Object.id.in_(object_ids))
        result = read_session.execute(stmt).all()
        object_list = [row._asdict() for row in result]

        stmt = select(object_payload.c.object_id, object_payload.c.payload_id).where(object_payload.c.object_id.in_(object_ids))
        result = read_session.execute(stmt).all()
        object_payload_list = [row._asdict() for row in result]
        payload_ids = list(OrderedDict.fromkeys(lst["payload_id"] for lst in object_payload_list))

        stmt = select(object_metadata.c.object_id, object_metadata.c.metadata_id).where(object_metadata.c.object_id.in_(object_ids))
        result = read_session.execute(stmt).all()
        object_metadata_list = [row._asdict() for row in result]
        metadata_ids = list(OrderedDict.fromkeys(lst["metadata_id"] for lst in object_metadata_list))

        payload_columns = [Payload.id, Payload.type, Payload.name]
        metadata_columns = [Metadata.id, Metadata.type, Metadata.name]
        # Add the 'data' column if payload_data is True
        if payload_data and not ref_payloads:
            payload_columns.append(Payload.data)
        if not ref_payloads:
            metadata_columns.append(Metadata.data)

        payload_list = []
        metadata_list = []

        if payload_ids:
            stmt = select(*payload_columns).where(Payload.id.in_(payload_ids))
            result = read_session.execute(stmt).all()
            payload_list = [row._asdict() for row in result]

        if metadata_ids:
            stmt = select(*metadata_columns).where(Metadata.id.in_(metadata_ids))
            result = read_session.execute(stmt).all()
            metadata_list = [row._asdict() for row in result]

        tag_payload_ids = []
        tag_metadata_ids = []
        tag_tag_list = []

        if is_tag:
            if tag["payloads"]:
                tag_payload_ids = [UUID(id) for id in tag["payloads"]]
                stmt = select(*payload_columns).where(Payload.id.in_(tag_payload_ids))
                result = read_session.execute(stmt).all()
                tag_payload_list = [row._asdict() for row in result]
                payload_list.extend(tag_payload_list)
                payload_ids.extend(tag_payload_ids)

            if tag["metadata_ids"]:
                tag_metadata_ids = [UUID(id) for id in tag["metadata_ids"]]
                stmt = select(Metadata.id, Metadata.type, Metadata.name, Metadata.data).where(Metadata.id.in_(tag_metadata_ids))
                result = read_session.execute(stmt).all()
                tag_metadata_list = [row._asdict() for row in result]
                metadata_list.extend(tag_metadata_list)
                metadata_ids.extend(tag_metadata_ids)

            for member in tag["members"]:
                tag_tag_list.append({"group_id": tag["id"], "member_id": member})

            for group in tag["groups"]:
                tag_tag_list.append({"group_id": group, "member_id": tag["id"]})

        if payload_data and not ref_payloads and decode:
            for payload in payload_list:
                temp_data = decompressBytes(payload["data"])
                if format:
                    try:
                        temp_data = json.loads(temp_data)
                        temp_data = json.dumps(temp_data, indent=4)
                    except json.JSONDecodeError:
                        pass
                payload["data"] = temp_data
        if ref_payloads:
            for payload in payload_list:
                payload["name"] = f"#ref#{payload['id'].hex}"
                payload["data"] = None
            for metadata in metadata_list:
                metadata["name"] = f"#ref#{metadata['id'].hex}"
                metadata["data"] = None

        # closure_set = set(tuple(d.items()) for d in closure_list)
        # closure_list = [dict(t) for t in closure_set]

        runkey = CompleteListsModel(
            objects={object["id"]: ObjectFullModel(**object) for object in object_list},
            payloads={payload["id"]: PayloadFullModel(**payload) for payload in payload_list},
            metadata={metadata["id"]: PayloadFullModel(**metadata) for metadata in metadata_list},
            tags=tag_dict,
            closure=[ClosureTableModel(**closure) for closure in closure_list],
            tag_tag=[TagTagTableModel(**tag_tag) for tag_tag in tag_tag_list],
            tag_object=[TagObjectTableModel(tag_id=tag["id"], object_id=object_id) for object_id in tag["objects"]],
            tag_payload=[TagPayloadTableModel(tag_id=tag["id"], payload_id=payload_id) for payload_id in tag_payload_ids],
            tag_metadata=[TagMetadataTableModel(tag_id=tag["id"], metadata_id=metadata_id) for metadata_id in tag_metadata_ids],
            object_payload=[ObjectPayloadTableModel(**object_payload) for object_payload in object_payload_list],
            object_metadata=[ObjectMetadataTableModel(**object_metadata) for object_metadata in object_metadata_list],
        )

        return runkey

    def write_lists(
        self, write_session, runkey: CompleteListsModel, encode: bool = True, keep_ids: bool = True, add_tag: bool = False, resolve_payloads: bool = False
    ):
        if resolve_payloads:
            payload_uuids = map_uuids_ref(runkey.payloads, decode=True)
            metadata_uuids = map_uuids_ref(runkey.metadata)
        else:
            for key, value in runkey.payloads.items():
                # only encode if not to be encoded later
                if not encode and not isinstance(value.data, bytes):
                    value.data = compressToBytes(value.data)
        if not keep_ids:
            if not resolve_payloads:
                payload_uuids = map_uuids(runkey.payloads.keys())
                metadata_uuids = map_uuids(runkey.metadata.keys())

            object_uuids = map_uuids(runkey.objects.keys())
            tag_uuids = map_uuids([tag.id for tag in runkey.tags.values()])

            for dataset in runkey.objects.values():
                dataset.id = object_uuids[dataset.id]
            for dataset in runkey.payloads.values():
                dataset.id = payload_uuids[dataset.id]
            for dataset in runkey.metadata.values():
                dataset.id = metadata_uuids[dataset.id]
            for dataset in runkey.tags.values():
                dataset.id = tag_uuids[dataset.id]
            for dataset in runkey.object_payload:
                dataset.object_id = object_uuids[dataset.object_id]
                dataset.payload_id = payload_uuids[dataset.payload_id]
            for dataset in runkey.object_metadata:
                dataset.object_id = object_uuids[dataset.object_id]
                dataset.metadata_id = metadata_uuids[dataset.metadata_id]
            for dataset in runkey.closure:
                dataset.ancestor_id = object_uuids[dataset.ancestor_id]
                dataset.descendant_id = object_uuids[dataset.descendant_id]
            for dataset in runkey.tag_object:
                dataset.object_id = object_uuids[dataset.object_id]
            if add_tag:
                for dataset in runkey.tag_object:
                    dataset.tag_id = tag_uuids[dataset.tag_id]
                for dataset in runkey.tag_payload:
                    dataset.payload_id = payload_uuids[dataset.payload_id]
                    dataset.tag_id = tag_uuids[dataset.tag_id]
                for dataset in runkey.tag_metadata:
                    dataset.metadata_id = metadata_uuids[dataset.metadata_id]
                    dataset.tag_id = tag_uuids[dataset.tag_id]
                for dataset in runkey.tag_tag:
                    dataset.group_id = tag_uuids[dataset.group_id]
                    dataset.member_id = tag_uuids[dataset.member_id]

        runkey_dump = runkey.model_dump()
        self.insert(write_session, "object", list(runkey_dump["objects"].values()))
        self.insert(write_session, "payload", list(runkey_dump["payloads"].values()), encode=encode)
        self.insert(write_session, "metadata", list(runkey_dump["metadata"].values()))
        self.insert(write_session, "object_payload", runkey_dump["object_payload"])
        self.insert(write_session, "object_metadata", runkey_dump["object_metadata"])
        self.insert(write_session, "closure", runkey_dump["closure"])

        if add_tag:
            self.insert(write_session, "tag", list(runkey_dump["tags"].values()))
            self.insert(write_session, "tag_tag", runkey_dump["tag_tag"])
            self.insert(write_session, "tag_object", runkey_dump["tag_object"])
            self.insert(write_session, "tag_payload", runkey_dump["tag_payload"])
            self.insert(write_session, "tag_metadata", runkey_dump["tag_metadata"])

        if keep_ids:
            new_tag_payload_ids = [tag_payload.payload_id for tag_payload in runkey.tag_payload]
            new_tag_metadata_ids = [tag_metadata.metadata_id for tag_metadata in runkey.tag_metadata]
        else:
            new_tag_payload_ids = [payload_uuids[tag_payload.payload_id] for tag_payload in runkey.tag_payload]
            new_tag_metadata_ids = [metadata_uuids[tag_metadata.metadata_id] for tag_metadata in runkey.tag_metadata]

        new_tag_payload_ids.extend(new_tag_metadata_ids)

        return [tag_object.object_id for tag_object in runkey.tag_object], new_tag_payload_ids

    def search_in_tag(
        self,
        read_session,
        name: str,
        payload_types: list[str] = [],
        object_ids: list[str] = [],
        search_dict: dict = None,
        payload_data: bool = False,
        order_by_object: bool = False,
    ) -> Union[list, dict]:
        payload_columns = [Object.id.label("object_id"), Payload.type, Payload.name, Payload.id]

        payload_filters = [Tag.name == name]
        object_filters = []

        # Add the 'data' column if payload_data is True
        if payload_data:
            payload_columns.append(Payload.data)
        if payload_types:
            payload_filters.append(Payload.type.in_(payload_types))

        stmt = select(*payload_columns).join(Tag.payloads.and_(*payload_filters))

        if not object_ids and not search_dict and not order_by_object:
            stmt = stmt.outerjoin(Payload.objects)
        else:
            if object_ids:
                object_filters = [Object.id.in_(object_ids)]
            stmt = stmt.join(Payload.objects.and_(*object_filters))

            if search_dict:
                search_filters = self._get_filters(search_dict)
                stmt = stmt.join(Object.meta.and_(*search_filters))

        payloads = read_session.execute(stmt).all()

        if not order_by_object:
            results = []
            for payload in payloads:
                object_id = payload.object_id
                if object_id:
                    object_id = object_id.hex
                result = {
                    "name": payload.name,
                    "type": payload.type,
                    "id": payload.id.hex,
                    "object_id": object_id,
                }
                if payload_data:
                    result["data"] = decompressBytes(payload.data)
                results.append(result)

        if order_by_object:
            if payload_data:
                results = defaultdict(dict)
                for payload in payloads:
                    object_id = payload.object_id.hex
                    new_key = payload.type
                    counter = 1
                    while new_key in results[object_id]:
                        new_key = f"{payload.type}_{counter}"
                        counter += 1

                    results[object_id][new_key] = {
                        "name": payload.name,
                        "type": payload.type,
                        "id": payload.id.hex,
                        "data": decompressBytes(payload.data),
                    }
            else:
                results = defaultdict(list)
                for payload in payloads:
                    object_id = payload.object_id.hex
                    results[object_id].append(payload.type)

        return results
