from configdb_server.adapter.sqlalchemy.sqlalchemy_base import SQLAlchemyBaseAdapter
from configdb_server.adapter.base_adapter import Connection
from configdb_server.tools import check_if_uuid
from configdb_server.exceptions import DictTypeError, FeatureNotSupported, NotValidUUIDError
from configdb_server.dataclasses import ReuseObjectModel, ReuseConnectionModel, ReuseModel
from typing import Union


class SQLAlchemyRecAdapter(SQLAlchemyBaseAdapter):
    def read_object_tree(self, read_session, id, payload_data=False, payload_filter="", decode=True, format=False, depth=-1, view=1) -> dict:
        object = self.read_orm_object(read_session, id).to_tree(
            payload_data=payload_data, payload_filter=payload_filter, decode=decode, format=format, depth=depth, view=view
        )
        return object

    def read_tag_tree(self, read_session, name, payload_data=False, payload_filter="", decode=True, format=False, depth=-1, view=1) -> dict:
        tag = self.read_orm_tag(read_session, name).to_tree(
            payload_data=payload_data, payload_filter=payload_filter, decode=decode, format=format, depth=depth, view=view
        )
        return tag

    def write_full_tree(self, write_session, data: Union[ReuseConnectionModel, ReuseObjectModel], encode: bool = True, keep_ids: bool = True) -> str:
        return self.__loop_write_tree(write_session, data, keep_ids, encode=encode).id

    def __loop_write_tree(self, write_session, data: Union[ReuseConnectionModel, ReuseObjectModel], keep_ids, encode=True):
        payload_objs = []
        children = []

        if isinstance(data, ReuseConnectionModel):
            raise FeatureNotSupported

        for dataset in data.payloads:
            # reuse payload
            if isinstance(dataset, ReuseModel):
                payload_id = dataset.reuse_id
                if not check_if_uuid(payload_id):
                    NotValidUUIDError
                payload_objs.append(payload_id)
            # create new payload
            else:
                if dataset.id and keep_ids:
                    if not check_if_uuid(dataset.id):
                        raise NotValidUUIDError
                else:
                    dataset.id = None
                try:
                    payl = self.create_payload(write_session, payload=dataset, encode=encode)
                    payload_objs.append(payl)
                except KeyError:
                    raise DictTypeError
                # except IDInUseError:
                #     payl = dataset["id"]
                #     payload_objs.append(payl)

        for dataset in data.children:
            children.append(self.__loop_write_tree(write_session=write_session, data=dataset, keep_ids=keep_ids, encode=encode))

        if data.id and keep_ids:
            if not check_if_uuid(data.id):
                raise NotValidUUIDError
        else:
            data.id = None

        obj = self.create_object(write_session, object=data)
        conn = Connection(id=obj, view=data.view)

        return conn
