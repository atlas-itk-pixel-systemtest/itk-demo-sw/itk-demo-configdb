from sqlalchemy import Column, Table, ForeignKey, Integer
from sqlalchemy.orm import declarative_base, DeclarativeMeta, relationship
from better_abc import ABCMeta
from configdb_server.models.model import ClosureModel
from sqlalchemy_utils import UUIDType


class DeclarativeABCMeta(DeclarativeMeta, ABCMeta):
    pass


Base = declarative_base(metaclass=DeclarativeABCMeta)


class ObjectClosure(Base, ClosureModel):
    __tablename__ = "closure"
    ancestor_id = Column(UUIDType(binary=False), ForeignKey("object.id"), primary_key=True)
    descendant_id = Column(UUIDType(binary=False), ForeignKey("object.id"), primary_key=True)
    depth = Column(Integer, nullable=False, primary_key=True)
    view = Column(Integer, nullable=False, default=1)

    ancestor = relationship("Object", primaryjoin="ObjectClosure.ancestor_id == Object.id", back_populates="descendant_associations", cascade="all, delete")
    descendant = relationship("Object", primaryjoin="ObjectClosure.descendant_id == Object.id", back_populates="ancestor_associations", cascade="all, delete")

    def to_tree(self, payload_data=0, depth=-1):
        return self.to_dict()

    def to_dict(self, payload_data=True, connections=True):
        return {"ancestor": self.ancestor_id.hex, "descendant": self.descendant_id.hex, "depth": self.depth, "view": self.view}


tag_object = Table(
    "tag_object",
    Base.metadata,
    Column("tag_id", UUIDType(binary=False), ForeignKey("tag.id", ondelete="CASCADE"), primary_key=True),
    Column("object_id", UUIDType(binary=False), ForeignKey("object.id", ondelete="CASCADE"), primary_key=True),
)

tag_tag = Table(
    "tag_tag",
    Base.metadata,
    Column("member_id", UUIDType(binary=False), ForeignKey("tag.id", ondelete="CASCADE"), primary_key=True),
    Column("group_id", UUIDType(binary=False), ForeignKey("tag.id", ondelete="CASCADE"), primary_key=True),
)

tag_payload = Table(
    "tag_payload",
    Base.metadata,
    Column("tag_id", UUIDType(binary=False), ForeignKey("tag.id", ondelete="CASCADE"), primary_key=True),
    Column("payload_id", UUIDType(binary=False), ForeignKey("payload.id", ondelete="CASCADE"), primary_key=True),
)

object_payload = Table(
    "object_payload",
    Base.metadata,
    Column("object_id", UUIDType(binary=False), ForeignKey("object.id", ondelete="CASCADE"), primary_key=True),
    Column("payload_id", UUIDType(binary=False), ForeignKey("payload.id", ondelete="CASCADE"), primary_key=True),
)

tag_metadata = Table(
    "tag_metadata",
    Base.metadata,
    Column("tag_id", UUIDType(binary=False), ForeignKey("tag.id", ondelete="CASCADE"), primary_key=True),
    Column("metadata_id", UUIDType(binary=False), ForeignKey("metadata.id", ondelete="CASCADE"), primary_key=True),
)

object_metadata = Table(
    "object_metadata",
    Base.metadata,
    Column("object_id", UUIDType(binary=False), ForeignKey("object.id", ondelete="CASCADE"), primary_key=True),
    Column("metadata_id", UUIDType(binary=False), ForeignKey("metadata.id", ondelete="CASCADE"), primary_key=True),
)
