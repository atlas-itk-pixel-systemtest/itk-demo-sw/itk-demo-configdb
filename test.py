from pyconfigdb import ConfigDB
import json

key = "demi/default/itk-demo-configdb/api/url"
configdb = ConfigDB(key)


def create_module_runkey(rk: dict, module_dict: list[dict]):
    modules = []
    size = rk["metadata"]["nMods"]
    for i in range(size):
        module = module_dict[i % len(module_dict)]
        meta = {
            "type": "metadata",
            "data": json.dumps(
                {
                    "serial": module["serial"],
                    "position": i + 1,
                    "paths": [module["paths"]],
                }
            ),
            "meta": True,
        }
        # config = {"type": "path", "data": module["path"]}
        child = {"type": "module", "name": f"module_{i}", "payloads": [meta]}
        modules.append(child)
    
    tree = {"type": "root", "children": modules}
    root_id = configdb.stage_create(data=tree)

    payl_id = configdb.payload_create(
        type="runkey_meta", data=json.dumps(rk["metadata"]), meta=True
    )
    res2 = configdb.tag_create(
        name=rk["name"],
        type="runkey",
        objects=[root_id],
        payloads=[payl_id],
    )

    res1 = configdb.stage_commit(
        rk["name"], rk["name"], "runkey", "analysis_rk_script", rk["comment"]
    )


def create():
    modules = [
        {
            "serial": "20UPGM22110471",
            "paths": [
                {"type": "as", "path": "20UPGM22110471/Jig1/010464"},
                {"type": "ds", "path": "20UPGM22110471/Jig1/010463"},
                {"type": "ts", "path": "20UPGM22110471/Jig1/010461"},
            ],
        },
        {
            "serial": "20UPGR92110514",
            "paths": [
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig1/000214"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig1/000213"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig1/000215"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig2/000210"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig2/000209"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig2/000211"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig3/000205"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig3/000203"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig3/000207"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig4/000200"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig4/000199"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig4/000201"},
            ],
        },
    ]

    with open("scripts/LLS_OB_CERN_collection.json", "r") as file:
        data = json.load(file)

    rks = data["list"]

    for rk in rks:
        create_module_runkey(rk, modules)


def read():
    all_runkeys = configdb.read_all(
        table="tag",
        stage=False,
        filter="runkey",
        name_filter="LLS",
    )
    with open(f"scripts/all.json", "w") as file:
        json.dump(all_runkeys, file, indent=4)
    one_runkey = configdb.tag_tree("lls_ob_0001", stage=False)
    with open(f"scripts/one.json", "w") as file:
        json.dump(one_runkey, file, indent=4)

if __name__ == "__main__":
    #create()
    read()
