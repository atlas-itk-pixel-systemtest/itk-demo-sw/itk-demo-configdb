import logging

accesslog = "-"


class RequestPathFilter(logging.Filter):
    def filter(self, record):
        return record.getMessage().find("/api/health") == -1
