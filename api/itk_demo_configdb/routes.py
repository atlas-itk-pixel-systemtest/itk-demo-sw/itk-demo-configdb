from uuid import uuid4
from typing import Union, Annotated
from itk_demo_configdb.states.context import Context
from itk_demo_configdb.error_decorator import error_decorator
from fastapi import APIRouter, Query
from configdb_server.dataclasses import (
    TagFullModel,
    InsertModel,
    AddToNodeModel,
    FullTreeModel,
    CopyTreeModel,
    PayloadFullModel,
    PayloadUpdateModel,
    InsertObjectsModel,
    ObjectPayloadModel,
    ObjectFullModel,
    SearchModel,
    UpdateConnectionsModel,
    UpdatePayloadModel,
    OptionalSearchModel,
    IDField,
    StandardResponseModel,
    StandardErrorResponseModel,
    StandardMultiResponseModel,
    StatusResponseModel,
    ObjectTreeResponse,
    SearchResponse,
    FormatingResponseModel,
    TagResponseModel,
    SearchSubtreeResponseModel,
    PayloadResponseModel,
    BundleResponseModel,
    ObjectResponseModel,
    HealthResponseModel,
    GetAllResponseModel,
    TagTreeResponseModel,
    ListsModel,
)
from configdb_server.adapter.sqlalchemy.sqlalchemy_base import TableLiteral
from itk_demo_configdb.config import config

router = APIRouter(prefix="/api")

TableParam = Annotated[TableLiteral, Query(examples="payload", description="table name")]
StageParam = Annotated[bool, Query(description="execute request in the staging area (True) or in the production area (False)")]
PayloadDataParam = Annotated[bool, Query(description="adds payload data")]
PayloadFilterParam = Annotated[str, Query(description="only include payloads which type contains this string")]
DepthParam = Annotated[int, Query(description="depth up to which the tree will be traversed (-1 for full tree)")]
IDParam = Annotated[IDField, Query(examples=uuid4().hex, description="uuid of the dataset as a string without dashes")]
NameParam = Annotated[str, Query(examples="my_runkey_name", description="name of the tag dataset")]
FormatParam = Annotated[bool, Query(description="format json payloads")]
ViewParam = Annotated[int, Query(description="view of the tree (e.g.: 1 for default, 2 for stale, 3 for both)")]
IdentifierParam = Annotated[Union[str, IDField], Query(examples=uuid4().hex, description="name of the root tag or uuid of the root node")]
ObjectTypeParam = Annotated[str, Query(examples="optoboard", description="type of the searched object")]
ConfigTypeParam = Annotated[str, Query(examples="config", description="type of the searched config")]
KeepIDsParam = Annotated[bool, Query(description="keep the ids of the copied datasets")]


@router.get(
    "/read_all",
    summary="get a list of datasets from one table",
    tags=["database"],
    responses={200: {"model": GetAllResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def get_all(
    table: TableParam,  # type: ignore
    stage: StageParam = True,
    filter: Annotated[str, Query(examples="frontend", description="type to filter by")] = "",
    name_filter: Annotated[str, Query(examples="longeron", description="name to filter by")] = "",
    payload_filter: Annotated[
        str,
        Query(
            examples="config",
            description="type of payload to filter by (only has an effect when reading the object or tag table), datasets without a suitable payload will not be listed (only works for direct associations)",
        ),
    ] = "",
    child_filter: Annotated[
        str,
        Query(
            examples="felix",
            description="type of child to filter by, datasets without a suitable child will not be listed (only works for direct associations)",
        ),
    ] = "",
    offset: Annotated[int, Query(description="offset by where to start the list")] = 0,
    limit: Annotated[int, Query(description="limit after which to end the list")] = 0,
    order_by: Annotated[str, Query(examples="id", description="attribute by which to order the list")] = "",
    asc: Annotated[bool, Query(description="order the list ascending or descending")] = True,
    connections: Annotated[bool, Query(description="defines whether to include the connections of the datasets (payloads and children)")] = False,
    payload_data: PayloadDataParam = False,
    depth: DepthParam = 0,
):
    return Context.get_state_class().on_get_all(
        Context,
        table=table,
        stage=stage,
        filter=filter,
        name_filter=name_filter,
        payload_filter=payload_filter,
        child_filter=child_filter,
        offset=offset,
        limit=limit,
        order_by=order_by,
        asc=asc,
        connections=connections,
        payload_data=payload_data,
        depth=depth,
    )


@router.post(
    "/nodes",
    summary="insert multiple objects (optionally including new payloads)",
    tags=["runkey"],
    responses={200: {"model": StandardMultiResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def object_insert(insert: InsertObjectsModel, stage: StageParam = True):
    return Context.get_state_class().on_object_insert(context=Context, insert=insert, stage=stage)


@router.post(
    "/node",
    summary="add an object with payloads",
    tags=["runkey"],
    responses={200: {"model": StandardResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def add_node(object: ObjectPayloadModel, stage: StageParam = True):
    return Context.get_state_class().on_add_node(context=Context, object=object, stage=stage)


@router.post(
    "/on_node",
    summary="add payloads to an existing node",
    tags=["runkey"],
    responses={200: {"model": StandardMultiResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def add_to_node(data: AddToNodeModel, id: IDParam, stage: StageParam = True):
    return Context.get_state_class().on_add_to_node(context=Context, id=id, data=data, stage=stage)


@router.delete(
    "/on_node",
    summary="remove payloads and children from an existing node",
    tags=["runkey"],
    responses={200: {"model": StatusResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def remove_from_object(
    id: IDParam,
    children: Annotated[list[IDField], Query(examples=[uuid4().hex, uuid4().hex], description="list of payload uuids")] = [],
    payloads: Annotated[list[IDField], Query(examples=[uuid4().hex, uuid4().hex], description="list of payload uuids")] = [],
):
    return Context.get_state_class().on_remove_from_object(context=Context, id=id, children=children, payloads=payloads)


@router.post("/stage", summary="create an entire tree in the staging area", tags=["stage"], response_model=StandardResponseModel)
@error_decorator
def create_full_tree(tree: FullTreeModel):
    return Context.get_state_class().on_create_full_tree(context=Context, tree=tree)


@router.post(
    "/lists",
    summary="write data from lists",
    tags=["stage"],
    responses={"200": {"model": StandardMultiResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def write_lists(lists: ListsModel, keep_ids: KeepIDsParam = False, add_tag: Annotated[bool, Query(description="add tag to the datasets")] = False):
    return Context.get_state_class().on_write_lists(context=Context, lists=lists, keep_ids=keep_ids, add_tag=add_tag)


@router.post(
    "/stage/clone",
    summary="clone a tree into the staging area",
    tags=["stage"],
    responses={200: {"model": StandardMultiResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def clone(copy: CopyTreeModel, keep_ids: KeepIDsParam = False):
    return Context.get_state_class().on_clone(context=Context, keep_ids=keep_ids, copy=copy)


@router.post(
    "/stage/commit",
    summary="commit a tree from the staging area",
    tags=["stage"],
    responses={200: {"model": StandardMultiResponseModel}, 400: {"model": StandardErrorResponseModel}},
)
@error_decorator
def commit(copy: CopyTreeModel, keep_ids: KeepIDsParam = False):
    return Context.get_state_class().on_commit(context=Context, keep_ids=keep_ids, copy=copy)


@router.post(
    "/tag", summary="create a tag dataset", tags=["basic"], responses={"200": {"model": StandardResponseModel}, "400": {"model": StandardErrorResponseModel}}
)
@error_decorator
def create_tag(tag: TagFullModel, stage: StageParam = True):
    return Context.get_state_class().on_create_tag(context=Context, tag=tag, stage=stage)


@router.get("/tag", summary="read a tag dataset", tags=["basic"], responses={"200": {"model": TagResponseModel}, "400": {"model": StandardErrorResponseModel}})
@error_decorator
def read_tag(name: NameParam, stage: StageParam = True):
    return Context.get_state_class().on_read_tag(context=Context, name=name, stage=stage)


@router.get(
    "/tag/format",
    summary="read a tag dataset formatted as a string",
    tags=["runkey"],
    responses={"200": {"model": FormatingResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def format_tag(
    name=NameParam,
    stage: StageParam = True,
    include_id: Annotated[bool, Query(description="include the id of the datasets")] = False,
    shorten_data: Annotated[
        int,
        Query(description="shorten the data to a specific amount of characters (-1 to not shorten)"),
    ] = -1,
):
    return Context.get_state_class().on_format_tag(context=Context, name=name, stage=stage, include_id=include_id, shorten_data=shorten_data)


@router.post(
    "/object",
    summary="create an object dataset",
    tags=["basic"],
    responses={"200": {"model": StandardResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def create_object(object: ObjectFullModel, stage: StageParam = True):
    return Context.get_state_class().on_create_object(context=Context, object=object, stage=stage)


@router.get(
    "/object", summary="read an object dataset", tags=["basic"], responses={"200": {"model": ObjectResponseModel}, "400": {"model": StandardErrorResponseModel}}
)
@error_decorator
def read_object(id: IDParam, stage: StageParam = True):
    return Context.get_state_class().on_read_object(context=Context, id=id, stage=stage)


@router.post(
    "/payload",
    summary="create a payload dataset",
    tags=["basic"],
    responses={"200": {"model": StandardResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def create_payload(payload: PayloadFullModel, stage: StageParam = True):
    return Context.get_state_class().on_create_payload(context=Context, payload=payload, stage=stage)


@router.get(
    "/payload",
    summary="read a payload dataset",
    tags=["basic"],
    responses={"200": {"model": PayloadResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def read_payload(
    id: IDParam,
    stage: StageParam = True,
    meta: Annotated[bool, Query(description="save payload as searchable metadata")] = False,
    format: FormatParam = False,
    jsroot: Annotated[bool, Query(description="transform payload to tbuffer json for jsroot compatibility")] = False,
):
    return Context.get_state_class().on_read_payload(context=Context, id=id, stage=stage, meta=meta, format=format, jsroot=jsroot)


@router.post(
    "/payload/update",
    summary="update a payload dataset",
    tags=["basic"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def update_payload(payload: PayloadUpdateModel, stage: StageParam = True):
    return Context.get_state_class().on_update_payload(context=Context, payload=payload, stage=stage)


@router.post(
    "/insert",
    summary="insert multiple datasets into specified table",
    tags=["basic"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def insert(insert: InsertModel, table: TableParam, stage: StageParam = True):  # type: ignore
    return Context.get_state_class().on_insert(context=Context, insert=insert, table=table, stage=stage)


@router.get(
    "/object/tree",
    summary="read object dataset as nested dict",
    tags=["runkey"],
    responses={"200": {"model": ObjectTreeResponse}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def read_object_tree(
    id: IDParam,
    stage: StageParam = True,
    payload_data: PayloadDataParam = False,
    payload_filter: PayloadFilterParam = "",
    depth: DepthParam = -1,
    view: ViewParam = 1,
    format: FormatParam = False,
):
    return Context.get_state_class().on_read_object_tree(
        context=Context, id=id, stage=stage, payload_data=payload_data, payload_filter=payload_filter, depth=depth, view=view, format=format
    )


@router.get(
    "/tag/tree",
    summary="read tag dataset as nested dict",
    tags=["runkey"],
    responses={"200": {"model": TagTreeResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def read_tag_tree(
    name: NameParam,
    stage: StageParam = True,
    payload_data: PayloadDataParam = False,
    payload_filter: PayloadFilterParam = "",
    depth: DepthParam = -1,
    view: ViewParam = 1,
    format: FormatParam = False,
):
    return Context.get_state_class().on_read_tag_tree(
        context=Context, name=name, stage=stage, payload_data=payload_data, payload_filter=payload_filter, depth=depth, view=view, format=format
    )


@router.get(
    "/lists",
    summary="read tag dataset in flat lists",
    tags=["runkey"],
    responses={"200": {"model": ListsModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def read_lists(
    name: NameParam,
    stage: StageParam = True,
    payload_data: PayloadDataParam = False,
    depth: DepthParam = -1,
    view: ViewParam = 1,
    format: FormatParam = False,
):
    return Context.get_state_class().on_read_lists(context=Context, name=name, stage=stage, payload_data=payload_data, depth=depth, view=view, format=format)


@router.post(
    "/search",
    summary="search for payload within a runkey",
    tags=["runkey"],
    responses={"200": {"model": SearchResponse}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def search_for_config(search: SearchModel, identifier: IdentifierParam, object_type: ObjectTypeParam = "", config_type: ConfigTypeParam = ""):
    return Context.get_state_class().on_search_for_config(
        context=Context, search=search, identifier=identifier, object_type=object_type, config_type=config_type
    )


@router.post(
    "/search/tag",
    summary="search for multiple payloads within a runkey",
    tags=["runkey"],
    responses={"200": {"model": SearchResponse}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def search_in_tag(
    search: OptionalSearchModel,
    name: NameParam,
    payload_data: PayloadDataParam = False,
    order_by_object: Annotated[bool, Query(description="order by object ids")] = False,
):
    return Context.get_state_class().on_search_in_tag(context=Context, name=name, payload_data=payload_data, search=search, order_by_object=order_by_object)


@router.post(
    "/search/subtree",
    summary="search for subtree within a runkey",
    tags=["runkey"],
    responses={"200": {"model": SearchSubtreeResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def search_for_subtree(
    search: SearchModel,
    identifier: IdentifierParam,
    object_type: ObjectTypeParam = "",
    payload_data: PayloadDataParam = False,
    depth: DepthParam = -1,
    view: ViewParam = 1,
):
    return Context.get_state_class().on_search_for_subtree(
        context=Context, search=search, identifier=identifier, object_type=object_type, payload_data=payload_data, depth=depth, view=view
    )


@router.delete(
    "/stage",
    summary="delete a runkey from the staging area",
    tags=["stage"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def delete_runkey(identifier: IdentifierParam, stage: StageParam = True):
    return Context.get_state_class().on_delete_runkey(context=Context, identifier=identifier, stage=stage)


@router.post(
    "/connections/update",
    summary="update view of a connection",
    tags=["basic"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def update_connections(data: UpdateConnectionsModel):
    return Context.get_state_class().on_update_connections(context=Context, data=data)


@router.get(
    "/payloads",
    summary="bundle content of multiple json payloads into one",
    tags=["runkey"],
    responses={"200": {"model": BundleResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def bundle_payloads(ids: Annotated[list[IDField], Query(examples=[uuid4().hex, uuid4().hex], description="list of payload uuids")]):
    return Context.get_state_class().on_bundle_payloads(context=Context, ids=ids)


@router.post(
    "/payloads",
    summary="change values of mulitple payloads at once",
    tags=["runkey"],
    responses={"200": {"model": StandardMultiResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def change_connected_payloads(data: UpdatePayloadModel):
    return Context.get_state_class().on_change_connected_payloads(context=Context, data=data)


@router.get(
    "/database/connect",
    summary="connect to the backend-database",
    tags=["database"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def connect_db():
    return Context.get_state_class().on_connect_db(context=Context)


if config.developer_mode:

    @router.get(
        "/database/reset",
        summary="reset the backend-database (empty the tables)",
        tags=["database"],
        responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
    )
    @error_decorator
    def clear_db(stage: StageParam = True):
        return Context.get_state_class().on_reset_db(context=Context, stage=stage)


@router.get(
    "/database/create",
    summary="create the backend-database",
    tags=["database"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def create_db():
    return Context.get_state_class().on_create_db(context=Context)


@router.get(
    "/database/upgrade",
    summary="upgrade tables in the backend-database",
    tags=["database"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def upgrade_db():
    return Context.get_state_class().on_upgrade_db(context=Context)


@router.get(
    "/database/downgrade",
    summary="downgrade tables in the backend-database",
    tags=["database"],
    responses={"200": {"model": StatusResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def downgrade_db():
    return Context.get_state_class().on_downgrade_db(context=Context)


@router.get(
    "/health",
    summary="Get health information",
    description="Obtain health information of the configdb microservice.",
    responses={"200": {"model": HealthResponseModel}, "400": {"model": StandardErrorResponseModel}},
)
@error_decorator
def health():
    return Context.get_state_class().on_health(context=Context)
