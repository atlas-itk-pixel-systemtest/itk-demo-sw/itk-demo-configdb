import logging

from itk_demo_configdb.states.dbm import set_dbm_value
from itk_demo_configdb.config import config
from pyregistry import ServiceRegistry
from pyregistry.exceptions import SRResponseError, SRConnectionError

log = logging.getLogger(__name__)


def set_connection():
    env_uri = config.configdb_database_uri
    env_stage_uri = config.configdb_database_stage_uri

    if env_stage_uri is not None and env_stage_uri != "":
        log.info(f" Using environmental variable CONFIGDB_DATABASE_STAGE_URI '{env_stage_uri}' for staging area database connection.")
        set_dbm_value("stage_uri", env_stage_uri)
    if env_uri is not None and env_uri != "":
        log.info(f" Using environmental variable CONFIGDB_DATABASE_URI '{env_uri}' for database backend connection.")
        set_dbm_value("uri", env_uri)
        return True
    else:
        try:
            sr = ServiceRegistry(config.sr_url)
        except SRConnectionError as e:
            log.error(f"Error connecting to Service Registry: {e}")
            return False
        try:
            url = sr.get(config.database_key)
            uri = f"{config.database_user}:{config.database_pw}@{url.replace('http://', '').replace('/api', '')}/configdb"
        except SRResponseError as e:
            log.error(f"Key not found: {e}")
            return False
        set_dbm_value("uri", uri)
        return True
