import os
from itk_demo_configdb.states.context import Context
from itk_demo_configdb.tools import set_connection
from configdb_server.exceptions import DatabaseNotConnectedError
import logging


def initialize():
    logging.basicConfig(level=logging.INFO)
    path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    os.makedirs(f"{path}/database", exist_ok=True)

    migration_dir = os.path.dirname(__file__)
    migration_dir = os.path.join(migration_dir, "../migrations")
    if not set_connection():
        raise DatabaseNotConnectedError
    Context.state_check(migration_dir)
