from itk_demo_configdb.states.context import Context, States
from configdb_server.exceptions import CustomError, DatabaseNotConnectedError
from itk_demo_configdb.config import config
from sqlalchemy.exc import OperationalError
from fastapi.responses import JSONResponse
from rcf_response import Error
from functools import wraps

import logging

log = logging.getLogger(__name__)


def error_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if config.debug_mode:
            # try:
            response = func(*args, **kwargs)
            # except CustomError as error:
            #     err = Error(error.id, error.title, error.text)
            #     err = err.to_conn_response()
            #     log.error(err)
            #     return JSONResponse(*err)
        else:
            try:
                response = func(*args, **kwargs)
            except CustomError as error:
                err = Error(error.id, error.title, error.text)
                err = err.to_conn_response()
                log.error(err)
                return JSONResponse(*err)
            except OperationalError as err:
                Context.set_state(States.DATABASENOTCONNECTED)
                err = Error(
                    DatabaseNotConnectedError.id,
                    DatabaseNotConnectedError.title,
                    str(err),
                )
                err = err.to_conn_response()
                log.error(err)
                return JSONResponse(*err)
            except Exception as err:
                err = Error(
                    500,
                    "Internal Server Error",
                    err.__class__.__name__ + " " + str(err),
                )
                err = err.to_conn_response()
                log.error(err)
                return JSONResponse(*err)
        return response

    return wrapper
