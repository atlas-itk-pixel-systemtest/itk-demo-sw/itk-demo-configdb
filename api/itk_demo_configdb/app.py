import logging

from rcf_response import Error
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse

from itk_demo_configdb.routes import router
from itk_demo_configdb.initialize import initialize

logging.basicConfig(level=logging.INFO)

tags_metadata = [
    {
        "name": "stage",
        "description": "Endpoints to interact with runkeys in the staging area.",
    },
    {
        "name": "runkey",
        "description": "High-level endpoints to interact with runkeys.",
    },
    {
        "name": "basic",
        "description": "Low-level endpoints to interact with database tables directly.",
    },
    {
        "name": "database",
        "description": "Endpoints to interact with and manage the database state",
    },
]


def create_app_with_init():
    initialize()
    app = create_app()
    return app


def create_app():
    app = FastAPI(
        title="configDB API",
        description="HTTP API for the configDB",
        version="2.5.0",
        docs_url="/api/docs",  # custom location for swagger UI
        openapi_tags=tags_metadata,
    )

    app.include_router(router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    @app.get("/", include_in_schema=False)
    async def index():
        return RedirectResponse("/api/docs")

    @app.exception_handler(404)
    async def not_found_exception_handler(request: Request, exc: Exception):
        return http_error_response(request, exc)

    @app.exception_handler(405)
    async def method_not_allowed_exception_handler(request: Request, exc: Exception):
        return http_error_response(request, exc)

    return app


def internal_error_response(request, exc):
    error = Error(exc.id, exc.title, exc.text)
    err = error.to_conn_response()
    return JSONResponse(*err)


def http_error_response(request, exc):
    error = Error(exc.status_code, exc.detail, exc.detail)
    err = error.to_conn_response()
    return JSONResponse(*err)
