from itk_demo_configdb.states.base import BaseState, States
from itk_demo_configdb.config import config
from itk_demo_configdb.exceptions import NotAllowedError
from itk_demo_configdb.resultviewer import create_tbufferjson
from configdb_server.dataclasses import (
    TagFullModel,
    InsertModel,
    ObjectFullModel,
    AddToNodeModel,
    FullTreeModel,
    CopyTreeModel,
    PayloadFullModel,
    PayloadUpdateModel,
    InsertObjectsModel,
    ObjectPayloadModel,
    SearchModel,
    UpdateConnectionsModel,
    UpdatePayloadModel,
    OptionalSearchModel,
    ListsModel,
)
# create_tag as an example for on_create_object/on_create_payload
# on_add_node,,on_update_payload
# on_create_full_tree,on_clone,on_commit,on_search_in_tag,on_search_for_subtree,on_search_for_config


class Active(BaseState):
    name = "Active"

    @classmethod
    def state_check(cls, context):
        context.set_state(States.ACTIVE)
        return True

    @classmethod
    def on_get_all(
        cls,
        context,
        table,
        stage=True,
        filter="",
        name_filter="",
        payload_filter="",
        child_filter="",
        offset="",
        limit="",
        order_by="",
        asc=True,
        connections=False,
        payload_data=False,
        depth=0,
    ):
        db = context.get_db(stage)
        lst = db.get_all(
            table=table,
            filter=filter,
            name_filter=name_filter,
            payload_filter=payload_filter,
            child_filter=child_filter,
            offset=offset,
            limit=limit,
            order_by=order_by,
            asc=asc,
            connections=connections,
            payload_data=payload_data,
            depth=depth,
        )
        return {"status": 200, "list": lst}

    @classmethod
    def on_object_insert(cls, context, insert: InsertObjectsModel, stage=True):
        db = context.get_db(stage)
        lst = db.object_insert(insert.list)
        return {"status": 200, "ids": lst}

    @classmethod
    def on_add_node(cls, context, object: ObjectPayloadModel, stage=True):
        db = context.get_db(stage)
        id = db.add_node(object=object)
        return {"status": 200, "id": id}

    @classmethod
    def on_add_to_node(cls, context, id, data: AddToNodeModel, stage=True):
        db = context.get_db(stage)
        ids = db.add_to_node(id=id, payloads=data.payloads, children=data.children)
        return {"status": 200, "ids": ids}

    @classmethod
    def on_remove_from_object(cls, context, id, children, payloads):
        write_session = context.stage_db.backend.create_write_session()
        context.stage_db.backend.remove_from_object(
            write_session,
            id=id,
            children=children,
            payloads=payloads,
        )
        write_session.close()
        return {"status": 200}

    @classmethod
    def on_create_full_tree(cls, context, tree: FullTreeModel):
        id = context.stage_db.create_full_tree(tree=tree)
        return {"status": 200, "id": id}

    @classmethod
    def on_clone(cls, context, keep_ids, copy: CopyTreeModel):
        ids = context.stage_db.clone(db=context.db, copy=copy, keep_ids=keep_ids)
        return {"status": 200, "ids": ids}

    @classmethod
    def on_commit(cls, context, keep_ids, copy: CopyTreeModel):
        ids = context.stage_db.commit(db=context.db, copy=copy, keep_ids=keep_ids)
        return {"status": 200, "ids": ids}

    @classmethod
    def on_create_tag(cls, context, tag: TagFullModel, stage=True):
        tag_latest = not stage
        db = context.get_db(stage).backend
        write_session = db.create_write_session()
        id = db.create_tag(write_session=write_session, tag=tag, tag_latest=tag_latest)
        write_session.close()
        return {"status": 200, "id": id}

    @classmethod
    def on_read_tag(cls, context, name, stage=True):
        db = context.get_db(stage).backend
        read_session = db.create_read_session()
        dct = db.read_tag(read_session, name=name)
        read_session.close()
        dct["status"] = 200
        return dct

    @classmethod
    def on_format_tag(cls, context, name, stage=True, include_id=False, shorten_data=False):
        db = context.get_db(stage)
        str = db.format_tag(name=name, include_id=include_id, shorten_data=shorten_data)
        return {"status": 200, "string": str}

    @classmethod
    def on_create_object(cls, context, object: ObjectFullModel, stage=True):
        db = context.get_db(stage).backend
        write_session = db.create_write_session()
        id = db.create_object(write_session, object=object)
        write_session.close()
        return {"status": 200, "id": id}

    @classmethod
    def on_read_object(cls, context, id, stage=True):
        db = context.get_db(stage).backend
        read_session = db.create_read_session()
        dct = db.read_object(read_session, id)
        read_session.close()
        dct["status"] = 200
        return dct

    @classmethod
    def on_create_payload(cls, context, payload: PayloadFullModel, stage=True):
        db = context.get_db(stage).backend
        write_session = db.create_write_session()
        id = db.create_payload(write_session, payload=payload)
        write_session.close()
        return {"status": 200, "id": id}

    @classmethod
    def on_read_payload(cls, context, id, stage=True, meta=False, format=False, jsroot=False):
        db = context.get_db(stage).backend
        read_session = db.create_read_session()
        resolve_payloads = None
        if stage:
            resolve_payloads = context.db
        dct = db.read_payload(read_session, id=id, meta=meta, format=format, resolve_payloads=resolve_payloads)
        read_session.close()
        dct["status"] = 200
        if jsroot:
            jsroot_data = create_tbufferjson(dct["data"])
            dct = jsroot_data
        return dct

    @classmethod
    def on_update_payload(cls, context, payload: PayloadUpdateModel, stage=True):
        db = context.get_db(stage).backend

        if not stage:
            if not payload.meta:
                raise Exception("Only metadata can be updated in the main database.")

        write_session = db.create_write_session()
        db.update_payload(write_session, payload)
        write_session.close()
        return {"status": 200}

    @classmethod
    def on_insert(cls, context, insert: InsertModel, table, stage=True):
        db = context.get_db(stage).backend
        write_session = db.create_write_session()
        db.insert(write_session, table, insert.list, True)
        write_session.close()
        return {"status": 200}

    @classmethod
    def on_read_object_tree(cls, context, id, stage=True, payload_data=False, payload_filter="", depth=-1, view=1, format=False):
        db = context.get_db(stage).backend
        resolve_payloads = None
        if stage:
            resolve_payloads = context.db
        read_session = db.create_read_session()
        dct = db.read_object_tree(
            read_session,
            id=id,
            payload_data=payload_data,
            payload_filter=payload_filter,
            depth=depth,
            view=view,
            format=format,
            resolve_payloads=resolve_payloads,
        )
        read_session.close()
        dct["status"] = 200
        return dct

    @classmethod
    def on_read_tag_tree(cls, context, name, stage=True, payload_data=False, payload_filter="", depth=-1, view=1, format=False):
        db = context.get_db(stage).backend
        resolve_payloads = None
        if stage:
            resolve_payloads = context.db
        read_session = db.create_read_session()
        dct = db.read_tag_tree(
            read_session,
            name=name,
            payload_data=payload_data,
            payload_filter=payload_filter,
            depth=depth,
            view=view,
            format=format,
            resolve_payloads=resolve_payloads,
        )
        read_session.close()
        dct["status"] = 200
        return dct

    @classmethod
    def on_search_for_config(cls, context, identifier, object_type, config_type, search: SearchModel):
        data = context.db.search_for_config(
            identifier=identifier,
            object_type=object_type,
            config_type=config_type,
            search_dict=search.search_dict,
        )
        dct = {"status": 200, "data": data}
        return dct

    @classmethod
    def on_search_in_tag(cls, context, name, payload_data, order_by_object, search: OptionalSearchModel):
        data = context.db.search_in_tag(
            name=name,
            payload_types=search.payload_types,
            object_ids=search.object_ids,
            search_dict=search.search_dict,
            payload_data=payload_data,
            order_by_object=order_by_object,
        )
        dct = {"status": 200, "data": data}
        return dct

    @classmethod
    def on_search_for_subtree(cls, context, identifier, object_type, search: SearchModel, payload_data=False, depth=-1, view=1):
        data = context.db.search_for_subtree(
            identifier=identifier,
            object_type=object_type,
            search_dict=search.search_dict,
            payload_data=payload_data,
            depth=depth,
            view=view,
        )
        dct = {"status": 200, "data": data}
        return dct

    @classmethod
    def on_delete_runkey(cls, context, identifier, stage=True):
        if not stage and not config.developer_mode:
            raise NotAllowedError
        db = context.get_db(stage).backend
        write_session = db.create_write_session()
        db.delete_tree(write_session, identifier)
        write_session.close()
        dct = {"status": 200}
        return dct

    @classmethod
    def on_update_connections(cls, context, data: UpdateConnectionsModel):
        write_session = context.stage_db.backend.create_write_session()
        context.stage_db.backend.update_connections(write_session, connections=data.connections)
        write_session.close()
        dct = {"status": 200}
        return dct

    @classmethod
    def on_bundle_payloads(cls, context, ids):
        bundle = context.stage_db.bundle_payloads(payload_ids=ids)
        dct = {"status": 200, "bundle": bundle}
        return dct

    @classmethod
    def on_change_connected_payloads(cls, context, data: UpdatePayloadModel):
        list = context.stage_db.change_connected_payloads(associations=data.associations, changes=data.changes)
        dct = {"status": 200, "ids": list}
        return dct

    @classmethod
    def on_read_lists(cls, context, name, stage=True, payload_data=False, depth=-1, view=1, format=False):
        db = context.get_db(stage)
        lists = db.read_lists(name=name, payload_data=payload_data, depth=depth, view=view, format=format)
        return lists

    @classmethod
    def on_write_lists(cls, context, lists: ListsModel, keep_ids=False, add_tag=False):
        root_ids, payload_ids = context.stage_db.write_lists(lists=lists, keep_ids=keep_ids, add_tag=add_tag)
        dct = {"status": 200, "ids": root_ids}
        return dct

    @classmethod
    def on_reset_db(cls, context, stage=True):
        context.get_db(stage).backend.clear_database()
        context.get_db(stage).backend.upgrade_database()
        dic = {"status": 200}
        return dic
