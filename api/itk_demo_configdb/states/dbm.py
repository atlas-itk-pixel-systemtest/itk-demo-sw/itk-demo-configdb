from itk_demo_configdb.exceptions import DbmError
import dbm
import logging
import os
import time

path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
os.makedirs(f"{path}/dbm", exist_ok=True)
dbm_name = f"{path}/dbm/data_store"
log = logging.getLogger(__name__)


def get_dbm_value(key, default=None, logging=True):
    while True:
        try:
            with dbm.open(dbm_name, "c") as db:
                try:
                    value = str(db[key], encoding="utf-8")
                    # print(f"get key {key}, value {value}")
                    return value
                except KeyError:
                    if default is not None:
                        return default
                    else:
                        raise DbmError(f"The key '{key}' does not exist in the data store " f"'{dbm_name}'.")
        except dbm.error:
            if logging:
                log.info(f" DBM Error for {dbm_name} - {dbm.error} - Trying again in 1 seconds.")
            time.sleep(0.1)


def set_dbm_value(key, value, logging=True):
    while True:
        try:
            with dbm.open(dbm_name, "c") as db:
                try:
                    db[key] = value.encode("utf-8")
                    # print(f"set key {key}, value {value}")
                    return
                except KeyError:
                    raise DbmError(f"The key '{key}' does not exist in the data store " f"'{dbm_name}'.")
        except dbm.error:
            if logging:
                log.info(f" DBM Error for {dbm_name} - {dbm.error} - Trying again in 1 seconds.")
        time.sleep(0.1)
