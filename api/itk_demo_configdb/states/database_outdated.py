from itk_demo_configdb.states.base import BaseState, States
from itk_demo_configdb.states.active import Active
from configdb_server.exceptions import DatabaseOutdatedError
import logging

log = logging.getLogger(__name__)


class DatabaseOutdated(BaseState):
    name = "DatabaseOutdated"

    @classmethod
    def state_check(cls, context):
        context.set_state(States.BUSY, "Checking DB status")

        read_session = context.db.backend.create_read_session()
        context.db.backend.upgrade_database()
        backend_status = context.db.backend.database_migration_status(read_session)
        read_session.close()
        if backend_status is False:
            log.info(" configdb database tables are outdated in backend database, updating them")
            context.db.backend.upgrade_database()

        stage_read_session = context.stage_db.backend.create_read_session()
        context.stage_db.backend.upgrade_database()
        stage_status = context.stage_db.backend.database_migration_status(stage_read_session)
        stage_read_session.close()
        if stage_status is False:
            log.info(" configdb database tables are outdated in staging area database, updating them")
            context.stage_db.backend.upgrade_database()

        read_session = context.db.backend.create_read_session()
        stage_read_session = context.stage_db.backend.create_read_session()
        backend_status = context.db.backend.database_migration_status(read_session)
        stage_status = context.stage_db.backend.database_migration_status(stage_read_session)
        stage_read_session.close()
        read_session.close()
        if backend_status is True and stage_status is True:
            return Active.state_check(context)
        else:
            context.set_state(States.DATABASEOUTDATED)
            raise DatabaseOutdatedError

    @classmethod
    def on_upgrade_db(cls, context):
        status = cls.state_check(context)
        if not status:
            raise DatabaseOutdatedError

        dic = {"status": 200}
        return dic

    @classmethod
    def on_downgrade_db(cls, context):
        context.set_state(States.BUSY, "Creating/Updating Tables")
        context.db.backend.downgrade_database()
        read_session = context.db.backend.create_read_session()
        status = context.db.backend.database_migration_status(read_session)
        read_session.close()
        if status is True:
            context.set_state(States.ACTIVE)
        else:
            context.set_state(States.DATABASEOUTDATED)
            raise DatabaseOutdatedError
        dic = {"status": 200}
        return dic
