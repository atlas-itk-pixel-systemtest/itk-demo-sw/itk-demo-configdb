import logging
from configdb_server.database_tools import Database
from itk_demo_configdb.states.dbm import get_dbm_value, set_dbm_value
from itk_demo_configdb.config import config
from itk_demo_configdb.states.busy import Busy
from itk_demo_configdb.states.database_not_connected import DatabaseNotConnected
from itk_demo_configdb.states.database_not_created import DatabaseNotCreated
from itk_demo_configdb.states.database_outdated import DatabaseOutdated
from itk_demo_configdb.states.active import Active
from itk_demo_configdb.states.base import BaseState, States


log = logging.getLogger(__name__)


class Context:
    states_dict = {
        "1": Busy,
        "2": DatabaseNotConnected,
        "3": DatabaseNotCreated,
        "4": DatabaseOutdated,
        "5": Active,
    }
    migration_dir = ""
    db: Database = None
    stage_db: Database = None
    uri = None

    @classmethod
    def state_check(cls, migration_dir, init_state=States.DATABASENOTCONNECTED):
        cls.migration_dir = migration_dir
        cls.set_state(init_state)
        cls.get_state_class().state_check(cls)

    @classmethod
    def set_state(cls, state: States, status=""):
        set_dbm_value("state", state.value)
        log.info(f" set_state {state.name}")
        if status != "":
            set_dbm_value("status", status)

    @classmethod
    def get_state_class(cls) -> BaseState:
        state = cls.states_dict[get_dbm_value("state")]
        return state

    @classmethod
    def connect_db(cls):
        log.info(" Connecting to database.")
        cls.uri = get_dbm_value("uri")
        cls.stage_uri = get_dbm_value("stage_uri", False)

        log.info(
            f" Backend database: type {config.configdb_database_type}, uri {cls.uri}"
        )
        log.info(
            f" Stage database: type {config.configdb_database_stage_type}, uri {cls.stage_uri}"
        )

        cls.db = Database(config.get_backend_type(), cls.uri)
        cls.stage_db = Database(config.get_stage_type(), cls.stage_uri)
        cls.db.backend.create_database()
        cls.stage_db.backend.create_database()

    @classmethod
    def connect_worker(cls):
        log.info(" Connecting worker to database.")
        cls.uri = get_dbm_value("uri", logging=False)
        cls.stage_uri = get_dbm_value("stage_uri", False)
        cls.db = Database(config.get_backend_type(), cls.uri)
        cls.stage_db = Database(config.get_stage_type(), cls.stage_uri)

    @classmethod
    def get_db(cls, stage):
        if stage:
            return cls.stage_db
        else:
            return cls.db
