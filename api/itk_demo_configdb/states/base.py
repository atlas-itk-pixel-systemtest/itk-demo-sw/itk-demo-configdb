from abc import ABC
from enum import Enum
from typing import TYPE_CHECKING
from configdb_server.exceptions import EndpointNotAvailableError
from configdb_server.dataclasses import (
    TagFullModel,
    InsertModel,
    ObjectFullModel,
    AddToNodeModel,
    FullTreeModel,
    CopyTreeModel,
    PayloadFullModel,
    PayloadUpdateModel,
    InsertObjectsModel,
    ObjectPayloadModel,
    SearchModel,
    OptionalSearchModel,
    UpdateConnectionsModel,
    UpdatePayloadModel,
    ListsModel,
)

if TYPE_CHECKING:
    from itk_demo_configdb.states.context import Context


class States(Enum):
    """map states to integers"""

    BUSY = "1"
    DATABASENOTCONNECTED = "2"
    DATABASENOTCREATED = "3"
    DATABASEOUTDATED = "4"
    ACTIVE = "5"


class BaseState(ABC):
    name = "NoneState"

    def __init__(self):
        pass

    @classmethod
    def state_check(cls, context: "Context"):
        pass

    @classmethod
    def on_health(cls, context: "Context"):
        dic = {"status": 200, "state": cls.name}
        if cls.name != "Active":
            context.get_state_class().state_check(context)
        return dic

    @classmethod
    def on_connect_db(cls, context: "Context"):
        raise EndpointNotAvailableError

    @classmethod
    def on_create_db(cls, context: "Context"):
        raise EndpointNotAvailableError

    @classmethod
    def on_upgrade_db(cls, context: "Context"):
        raise EndpointNotAvailableError

    @classmethod
    def on_reset_db(cls, context: "Context", stage: bool = True):
        raise EndpointNotAvailableError

    @classmethod
    def on_downgrade_db(cls, context: "Context"):
        raise EndpointNotAvailableError

    @classmethod
    def on_get_all(
        cls,
        context: "Context",
        table,
        stage=True,
        filter="",
        name_filter="",
        payload_filter="",
        child_filter="",
        offset="",
        limit="",
        order_by="",
        asc=True,
        connections=False,
        payload_data=False,
        depth=0,
    ):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_get_all(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_object_insert(cls, context: "Context", insert: InsertObjectsModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_object_insert(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_add_node(cls, context: "Context", object: ObjectPayloadModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_add_node(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_add_to_node(cls, context: "Context", id, data: AddToNodeModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_add_to_node(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_remove_from_object(cls, context: "Context", id, children, payloads):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_remove_from_object(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_create_full_tree(cls, context: "Context", tree: FullTreeModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_create_full_tree(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_clone(cls, context: "Context", keep_ids, copy: CopyTreeModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_clone(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_commit(cls, context: "Context", keep_ids, copy: CopyTreeModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_commit(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_create_tag(cls, context: "Context", tag: TagFullModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_create_tag(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_tag(cls, context: "Context", name, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_tag(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_format_tag(cls, context: "Context", name, stage=True, include_id=False, shorten_data=False):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_format_tag(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_create_object(cls, context: "Context", object: ObjectFullModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_create_object(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_object(cls, context: "Context", id, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_object(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_create_payload(cls, context: "Context", payload: PayloadFullModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_create_payload(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_update_payload(cls, context: "Context", payload: PayloadUpdateModel, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_update_payload(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_payload(cls, context: "Context", id, stage=True, meta=False, format=False, jsroot=False):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_payload(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_insert(cls, context: "Context", insert: InsertModel, table, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_insert(**locals())
        else:
            raise EndpointNotAvailableError
        
    @classmethod
    def on_change_connected_payloads(cls, context: "Context", data: UpdatePayloadModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_change_connected_payloads(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_lists(cls, context: "Context", name, stage=True, payload_data=False, payload_filter="", depth=-1, view=1, format=False):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_lists(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_object_tree(cls, context: "Context", id, stage=True, payload_data=False, payload_filter="", depth=-1, view=1, format=False):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_object_tree(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_read_tag_tree(cls, context: "Context", name, stage=True, payload_data=False, payload_filter="", depth=-1, view=1, format=False):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_read_tag_tree(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_search_for_config(cls, context: "Context", identifier, object_type, config_type, search: SearchModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_search_for_config(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_search_in_tag(cls, context: "Context", name, payload_data, order_by_object, search: OptionalSearchModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_search_in_tag(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_search_for_subtree(cls, context: "Context", identifier, object_type, search: SearchModel, payload_data=False, depth=-1, view=1):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_search_for_subtree(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_delete_runkey(cls, context: "Context", identifier, stage=True):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_delete_runkey(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_update_connections(cls, context: "Context", data: UpdateConnectionsModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_update_connections(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_bundle_payloads(cls, context: "Context", ids):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_bundle_payloads(**locals())
        else:
            raise EndpointNotAvailableError

    @classmethod
    def on_write_lists(cls, context: "Context", lists: ListsModel):
        if context.get_state_class().state_check(context):
            return context.get_state_class().on_write_lists(**locals())
        else:
            raise EndpointNotAvailableError
