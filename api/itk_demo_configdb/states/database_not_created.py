from itk_demo_configdb.states.base import BaseState, States
from itk_demo_configdb.states.database_outdated import DatabaseOutdated
from configdb_server.exceptions import DatabaseNotExistingError
import logging

log = logging.getLogger(__name__)


class DatabaseNotCreated(BaseState):
    name = "DatabaseNotCreated"

    # @classmethod
    # def on_create_db(cls):
    #     context.db.backend.create_database()

    @classmethod
    def state_check(cls, context):
        context.set_state(States.BUSY, "Checking DB status")
        if context.db.backend.database_exists() is False:
            log.info(" configdb database does not exist in backend database, creating it")
            context.db.backend.create_database()
        if context.stage_db.backend.database_exists() is False:
            log.info(" configdb database does not exist in staging area database, creating it")
            context.stage_db.backend.create_database()
        if (
            context.db.backend.database_exists() is True
            and context.stage_db.backend.database_exists() is True
        ):
            context.set_state(States.DATABASEOUTDATED)
            return DatabaseOutdated.state_check(context)
        else:
            context.set_state(States.DATABASENOTCREATED)
            raise DatabaseNotExistingError

    @classmethod
    def on_create_db(cls, context):
        status = cls.state_check(context)
        if not status:
            raise DatabaseNotExistingError

        dic = {"status": 200}
        return dic
