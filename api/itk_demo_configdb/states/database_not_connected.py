from itk_demo_configdb.states.base import BaseState, States
from itk_demo_configdb.states.database_not_created import DatabaseNotCreated
from configdb_server.exceptions import DatabaseNotConnectedError
from typing import override


class DatabaseNotConnected(BaseState):
    name = "DatabaseNotConnected"

    @classmethod
    @override
    def state_check(cls, context):
        context.set_state(States.BUSY, "Connecting")
        context.connect_db()
        if (
            context.db.backend.database_running() is True
            and context.stage_db.backend.database_running() is True
        ):
            context.set_state(States.DATABASENOTCREATED)
            return DatabaseNotCreated.state_check(context)
        else:
            context.set_state(States.DATABASENOTCONNECTED)
            raise DatabaseNotConnectedError

    @classmethod
    @override
    def on_connect_db(cls, context):
        status = cls.state_check(context)
        if not status:
            raise DatabaseNotConnectedError

        dic = {"status": 200}
        return dic
