from itk_demo_configdb.states.base import BaseState
from itk_demo_configdb.states.dbm import get_dbm_value


class Busy(BaseState):
    name = "Busy"

    @classmethod
    def state_check(cls, context):
        return False

    @classmethod
    def on_health(cls, context):
        try:
            status = get_dbm_value("status")
        except KeyError:
            status = "Unknown"
        dic = {"status": 200, "state": cls.name, "state_status": status}
        return dic
