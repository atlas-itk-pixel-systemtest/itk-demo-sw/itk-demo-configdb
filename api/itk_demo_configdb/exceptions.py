from configdb_server.exceptions import CustomError


class DbmError(CustomError):
    def __init__(self, text="Error in FSM Data storage. Please try again."):
        super().__init__()
        self.id = 490
        self.title = "DBM Error"
        self.text = text

class NotAllowedError(CustomError):
    def __init__(self, text="Action not allowed, check if db is in debug_mode."):
        super().__init__()
        self.id = 491
        self.title = "Not Allowed Error"
        self.text = text
