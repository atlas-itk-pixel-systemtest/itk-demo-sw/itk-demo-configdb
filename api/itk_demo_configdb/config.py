from configdb_server.database_tools import Backends
from config_checker import BaseConfig, EnvSource
import os

backend_dict = {
    "mariadb": Backends.SQLALCHEMY_MARIADB,
    "sqlite": Backends.SQLALCHEMY_SQLITE,
    "oracle": Backends.SQLALCHEMY_ORACLE,
    "postgres": Backends.SQLALCHEMY_POSTGRES,
    "recursive": Backends.SQLALCHEMY_REC,
}


class ConfigDB_Config(BaseConfig):
    database_key: str = ""
    database_user: str = "configdb"
    database_pw: str = "test"
    debug_mode: bool = False
    developer_mode: bool = False
    configdb_database_uri: str = ""
    configdb_database_type: str = "postgres"
    configdb_database_stage_uri: str = f"/{os.path.dirname(os.path.dirname(os.path.realpath(__file__)))}/database/stage.db"
    configdb_database_stage_type: str = "sqlite"
    sr_url: str = "http://localhost:5111/api"

    CONFIG_SOURCES = [EnvSource(allow_all=True, file="../.env")]

    def get_backend_type(self):
        return backend_dict[self.configdb_database_type]

    def get_stage_type(self):
        return backend_dict[self.configdb_database_stage_type]


config = ConfigDB_Config()
