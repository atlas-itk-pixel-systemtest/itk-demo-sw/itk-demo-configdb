from itk_demo_configdb.app import create_app
from itk_demo_configdb.states.context import Context

Context.connect_worker()
app = create_app()
