from pyregistry import ServiceRegistry
from dotenv import load_dotenv
import os
import uvicorn



load_dotenv()
host = os.environ.get("HOST", "localhost")
os.environ["DEBUG_MODE"] = "1"
os.environ["DEVELOPER_MODE"] = "1"
os.environ["CONFIGDB_DATABASE_URI"] = f"configdb:test@{host}:5432/configdb"
# os.environ["MARIADB_KEY"] = "demi/default/itk-demo-configdb/mariadb/url"
# os.environ["MARIADB_KEY"] = "demi/default/mariadb/url"
# os.environ["MARIADB_KEY"] = "demi/api_test/pyconfigdb/test_mariadb/url"

from itk_demo_configdb.app import create_app_with_init  # noqa: E402, needed here to get env vars

sr_url = f"http://{host}:5111/api"
sr = ServiceRegistry(sr_url)
sr.set("demi/jonas-laptop/itk-demo-configdb/api/url", "http://localhost:5000/api")
sr.set("demi/jonas-laptop/itk-demo-configdb/api/hostport", "5000")

if __name__ == "__main__":
    app = create_app_with_init()
    uvicorn.run(app, host="0.0.0.0", port=5000)
