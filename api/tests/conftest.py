import pytest
from typing import Tuple
from configdb_server.database_tools import Database, Backends
from configdb_server.testing_tools import std_data, std_data_meta, std_data_scan
from tests.tools import execute_get, execute_post
from fastapi.testclient import TestClient
import os


backend_type = "sqlite"
backend_url = "/test.db"
stage_url = "/test_stage.db"
# stage_url = "/file:a.db?mode=memory&cache=shared&uri=true"
# backend_url = "/file:b.db?mode=memory&cache=shared&uri=true"


@pytest.fixture
def database_connection() -> Database:  # type: ignore
    old_uri = os.environ.get("CONFIGDB_DATABASE_URI")
    os.environ["CONFIGDB_DATABASE_URI"] = backend_url
    old_type = os.environ.get("CONFIGDB_DATABASE_TYPE")
    os.environ["CONFIGDB_DATABASE_TYPE"] = backend_type

    old_stage_uri = os.environ.get("CONFIGDB_DATABASE_STAGE_URI")
    os.environ["CONFIGDB_DATABASE_STAGE_URI"] = stage_url
    old_stage_type = os.environ.get("CONFIGDB_DATABASE_STAGE_TYPE")
    os.environ["CONFIGDB_DATABASE_STAGE_TYPE"] = backend_type

    os.environ["DEBUG_MODE"] = "0"

    from itk_demo_configdb.app import create_app_with_init as create_app

    app = create_app()
    client = TestClient(app)
    res3 = execute_get(client, "/health")

    db = Database(Backends.SQLALCHEMY_SQLITE, backend_url)
    db.backend.upgrade_database()
    db.backend.clear_database()
    db = Database(Backends.SQLALCHEMY_SQLITE, stage_url)
    db.backend.upgrade_database()
    db.backend.clear_database()

    assert res3["state"] == "Active"

    yield client

    if old_uri is None:
        del os.environ["CONFIGDB_DATABASE_URI"]
    else:
        os.environ["CONFIGDB_DATABASE_URI"] = old_uri
    if old_type is None:
        del os.environ["CONFIGDB_DATABASE_TYPE"]
    else:
        os.environ["CONFIGDB_DATABASE_TYPE"] = old_type

    if old_stage_uri is None:
        del os.environ["CONFIGDB_DATABASE_STAGE_URI"]
    else:
        os.environ["CONFIGDB_DATABASE_STAGE_URI"] = old_stage_uri
    if old_stage_type is None:
        del os.environ["CONFIGDB_DATABASE_STAGE_TYPE"]
    else:
        os.environ["CONFIGDB_DATABASE_STAGE_TYPE"] = old_stage_type

    os.environ["DEBUG_MODE"] = "0"


@pytest.fixture
def example_runkey_data(database_connection: Database) -> Tuple[Database, list[str]]:
    client = database_connection
    data = std_data
    res1 = execute_post(client, "/stage/", {"data": data, "name": "root_tag", "author": "pytest"})
    assert res1["status"] == 200
    return client, data


@pytest.fixture
def example_runkey_data_scan(
    database_connection: Database,
) -> Tuple[Database, list[str]]:
    client = database_connection
    data = std_data_scan
    res1 = execute_post(client, "/stage", {"data": data, "name": "root_tag", "author": "pytest"})
    assert res1["status"] == 200
    return client, data


@pytest.fixture
def example_runkey_data_meta(
    database_connection: Database,
) -> Tuple[Database, list[str]]:
    client = database_connection
    data = std_data_meta
    res1 = execute_post(client, "/stage", {"data": data, "name": "root_tag", "author": "pytest"})
    res1 = execute_post(
        client,
        "/stage/commit",
        {"identifier": "root_tag", "author": "pytest", "name": "root_tag"},
    )
    assert res1["status"] == 200
    return client, data
