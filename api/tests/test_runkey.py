from configdb_server.database_tools import Database
from configdb_server.testing_tools import extract_dict, sort_list, std_data
from configdb_server.tools import ListGenerator
from tests.tools import execute_post, execute_get, execute_delete


def test_clone(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    execute_post(client, "/stage/commit", {"identifier": "root_tag", "name": "new_tree", "author": "pytest"})

    root_uuid = execute_post(client, "/stage/clone", {"identifier": "new_tree", "name": "root_tag", "author": "pytest", "type": "runkey"})
    tree = execute_get(client, "object/tree", {"id": root_uuid["ids"][0], "stage": True, "payload_data": True})

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_commit(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    execute_post(client, "/stage/commit", {"identifier": "root_tag", "name": "new tree", "author": "pytest", "comment": "test"})
    tag = execute_get(client, "/tag/tree", {"name": "new tree", "stage": False, "payload_data": True})["objects"][0]

    output = extract_dict(tag)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_runkey_from_dict(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    tree = execute_get(client, "/tag/tree", {"payload_data": True, "name": "root_tag", "stage": True})["objects"][0]

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_runkey_manual(database_connection: Database):
    client = database_connection

    execute_post(client, "/tag", {"type": "runkey", "author": "pytest", "name": "root_tag"})

    root_uuid = execute_post(
        client,
        "/node",
        {
            "type": "felix",
            "payloads": [
                {"data": "root_node payload", "type": "config", "name": "test"},
                {"data": "connectivity data", "type": "connect", "name": "test"},
            ],
            "tags": ["root_tag"],
        },
        {"stage": True},
    )["id"]

    felix1_payloads = [
        {"data": "payload_felix01", "type": "felix", "name": "test"},
        {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
    ]
    felix1_uuid = execute_post(client, "/node", {"type": "felix", "payloads": felix1_payloads, "parents": [{"id": root_uuid}]}, {"stage": True})
    felix1_uuid = felix1_uuid["id"]

    felix2_payloads = [{"data": "payload_felix02", "type": "felix", "name": "test"}]
    execute_post(client, "/node", {"type": "felix", "payloads": felix2_payloads, "parents": [{"id": root_uuid}]}, {"stage": True})

    lpgbt1_payload = [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}]
    lpgbt1_uuid = execute_post(client, "/node", {"type": "lpgbt", "payloads": lpgbt1_payload, "parents": [{"id": felix1_uuid}]}, {"stage": True})["id"]

    lpgbt2_payload = [{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}]
    execute_post(client, "/node", {"type": "lpgbt", "payloads": lpgbt2_payload, "parents": [{"id": felix1_uuid}]}, {"stage": True})

    lpgbt3_payload = [{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}]
    execute_post(client, "/node", {"type": "lpgbt", "payloads": lpgbt3_payload, "parents": [{"id": felix1_uuid}]}, {"stage": True})

    fe1_payload = [{"data": "payload_fe01", "type": "frontend", "name": "test"}]
    execute_post(client, "/node", {"type": "frontend", "payloads": fe1_payload, "parents": [{"id": lpgbt1_uuid}]}, {"stage": True})

    fe2_payload = [{"data": "payload_fe02", "type": "frontend", "name": "test"}]
    execute_post(client, "/node", {"type": "frontend", "payloads": fe2_payload, "parents": [{"id": lpgbt1_uuid}]}, {"stage": True})

    tree = execute_get(client, "/tag/tree", {"payload_data": True, "name": "root_tag", "stage": True})["objects"][0]

    output = extract_dict(tree)
    input = extract_dict(std_data)

    t1 = sort_list(input)
    t2 = sort_list(output)

    assert t1 == t2


def test_insert(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lists = ListGenerator("fe_config", "felix_config", "felix_config", "scan_config", "result_config")
    lists.create_runkey_lists(5, 2, 1, 0, 0)

    execute_post(client, "insert", {"list": lists.object_list}, {"table": "object", "stage": False})
    execute_post(client, "insert", {"list": lists.payload_list}, {"table": "payload", "stage": False})
    execute_post(client, "insert", {"list": lists.object_payload_list}, {"table": "object_payload", "stage": False})
    execute_post(client, "insert", {"list": lists.closure_list}, {"stage": False, "table": "closure"})

    execute_post(
        client,
        "tag",
        {"type": "runkey", "name": "test_runkey", "objects": [lists.root_id], "author": "pytest"},
        {"stage": False},
    )
    tree = execute_get(client, "/tag/tree", {"name": "test_runkey", "stage": False})

    extract_dict(data)
    output = extract_dict(tree["objects"][0])

    assert len(output) == 1
    assert len(output[0]) == 2


def test_delete_runkey(example_runkey_data: Database):
    client, data = example_runkey_data

    execute_delete(client, "/stage", {"identifier": "root_tag"})
    res2 = execute_get(client, "/tag/tree", {"name": "root_tag", "stage": True})

    assert res2["status"] == 463


def test_read_write_lists(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    execute_post(client, "/stage/commit", {"identifier": "root_tag", "name": "root_tag", "author": "pytest"})
    res1 = execute_get(client, "/lists", {"payload_data": True, "stage": False, "name": "root_tag"})

    execute_post(client, "/lists", res1, {"add_tag": True})
    tree = execute_get(client, "tag/tree", {"name": "root_tag", "stage": True, "payload_data": True})

    output = extract_dict(tree["objects"][0])
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)
