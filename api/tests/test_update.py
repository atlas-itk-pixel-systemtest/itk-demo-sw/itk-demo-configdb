from configdb_server.database_tools import Database
from configdb_server.testing_tools import extract_dict, sort_list, std_data
from tests.tools import execute_post, execute_get, execute_delete
from json import dumps
import copy


def test_update_nodes(database_connection: Database):
    client = database_connection

    payloads = [{"type": "payload", "data": "test_data", "name": "test"}]
    object_id = execute_post(client, "/node", {"type": "parent", "payloads": payloads}, {"stage": True})["id"]
    execute_post(client, "/node", {"type": "child", "parents": [{"id": object_id}]}, {"stage": True})

    object = execute_get(client, "object", {"stage": True, "id": object_id})
    child_id = object["children"][0]
    payload_id = object["payloads"][0]

    execute_delete(client, "/on_node", {"children": [child_id], "payloads": [payload_id], "id": object_id})
    empty_object = execute_get(client, "object", {"stage": True, "id": object_id})

    assert not empty_object["payloads"]
    assert not empty_object["children"]


def test_update_nodes2(database_connection: Database):
    client = database_connection

    payloads = [{"type": "payload", "data": "test_data", "name": "test"}]
    object_id = execute_post(client, "/node", {"type": "parent", "payloads": payloads})["id"]
    execute_post(client, "/node", {"type": "child", "parents": [object_id]})

    object = execute_get(client, "object", {"id": object_id})
    child_id = object["children"][0]
    payload_id = object["payloads"][0]

    execute_delete(client, "/on_node", {"children": [child_id], "payloads": [payload_id], "id": object_id})
    empty_object = execute_get(client, "object", {"stage": True, "id": object_id})

    assert not empty_object["payloads"]
    assert not empty_object["children"]


def test_update_connection(database_connection: Database):
    client = database_connection

    payloads = [{"type": "payload", "data": "test_data", "name": "test"}]
    object_id = execute_post(client, "/node", {"type": "parent", "payloads": payloads})["id"]
    execute_post(client, "/node", {"type": "child", "parents": [{"id": object_id}]})

    object = execute_get(client, "object", {"id": object_id})
    child_id = object["children"][0]

    execute_delete(client, "/on_node", {"children": [child_id], "id": object_id})
    empty_object = execute_get(client, "object", {"id": object_id})

    execute_post(client, "/connections/update", {"connections": [{"parent": object_id, "child": child_id, "view": 1}]})
    new_object = execute_get(client, "object", {"id": object_id})

    assert not empty_object["children"]
    assert object == new_object


def test_update_payload(database_connection: Database):
    client = database_connection

    payload_id = execute_post(
        client,
        "/payload",
        {"type": "test_type", "data": "test_data", "name": "test_name", "stage": True},
    )["id"]
    execute_post(
        client,
        "/payload/update",
        {"type": "test_type2", "data": "test_data2", "name": "test_name2", "id": payload_id},
    )
    payload = execute_get(client, "payload", {"id": payload_id, "stage": True})

    assert payload["type"] == "test_type2"
    assert payload["data"] == "test_data2"
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_update_payload_meta(database_connection: Database):
    client = database_connection

    payload_id = execute_post(
        client,
        "/payload",
        {"type": "test_type", "data": '{"a": 1}', "name": "test_name", "meta": True},
    )["id"]
    execute_post(
        client,
        "/payload/update",
        {"type": "test_type2", "data": '{"a": 2}', "name": "test_name2", "id": payload_id, "meta": True},
    )
    payload = execute_get(client, "payload", {"id": payload_id, "stage": True, "meta": True, "format": True})

    assert payload["type"] == "test_type2"
    assert payload["data"] == '{\n    "a": 2\n}'
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_update_payload_meta2(database_connection: Database):
    client = database_connection

    payload_id = execute_post(
        client,
        "/payload",
        {"type": "test_type", "data": '{"a": 1}', "name": "test_name", "meta": True},
    )["id"]
    execute_post(client, "/payload/update", {"type": "test_type2", "data": '{"a": 2}', "name": "test_name2", "id": payload_id})
    payload = execute_get(client, "payload", {"id": payload_id, "stage": True, "format": True})

    assert payload["type"] == "test_type2"
    assert payload["data"] == '{\n    "a": 2\n}'
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_update_payload_meta_backend(database_connection: Database):
    client = database_connection

    payload_id = execute_post(client, "/payload", {"type": "test_type", "data": '{"a": 1}', "name": "test_name", "meta": True}, {"stage": False})["id"]
    execute_post(client, "/payload/update", {"type": "test_type2", "data": '{"a": 2}', "name": "test_name2", "id": payload_id, "meta": True}, {"stage": False})
    payload = execute_get(client, "/payload", {"id": payload_id, "stage": False, "format": True, "meta": True})

    assert payload["type"] == "test_type2"
    assert payload["data"] == '{\n    "a": 2\n}'
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_update_payload_commit(database_connection: Database):
    client = database_connection

    payload_id = execute_post(client, "/payload", {"type": "test_type", "data": '{"a": 1}', "name": "test_name", "meta": True}, {"stage": False})["id"]

    execute_post(client, "/payload/update", {"type": "test_type2", "data": '{"a": 2}', "name": "test_name2", "id": payload_id, "meta": True}, {"stage": False})
    payload = execute_get(client, "/payload", {"id": payload_id, "stage": False, "format": True})

    assert payload["type"] == "test_type2"
    assert payload["data"] == '{\n    "a": 2\n}'
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_bundle_payloads(database_connection: Database):
    client = database_connection

    payl1 = execute_post(client, "/payload", {"type": "config", "data": dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1})})["id"]
    payl2 = execute_post(client, "/payload", {"type": "config", "data": dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2})})["id"]
    payl3 = execute_post(client, "/payload", {"type": "config", "data": dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1})})["id"]

    obj = execute_post(client, "/object", {"type": "frontend", "payloads": [payl1, payl2, payl3]})["id"]

    changes = {"b": {0: 2}, "e": 5}
    connections = [{"payload_id": payl1, "object_id": obj}, {"payload_id": payl2, "object_id": obj}, {"payload_id": payl3, "object_id": obj}]
    ids = execute_post(client, "/payloads", {"changes": changes, "associations": connections})
    ids = ids["ids"]
    bundle = execute_get(client, "/payloads", {"ids": ids})["bundle"]

    assert bundle == {
        "a": [1, 2, 3],
        "b": [[2, 2, 2], [3, 3, 5]],
        "c": {"d": [4, 4, 3]},
        "e": [5, 5, 5],
    }


def test_add_nodes(database_connection: Database):
    client = database_connection

    child_id1 = execute_post(client, "/object", {"type": "test_child1"}, {"stage": False})["id"]
    child_id2 = execute_post(client, "/object", {"type": "test_child2"}, {"stage": False})["id"]
    parent_id1 = execute_post(client, "/object", {"type": "test_parent1"}, {"stage": False})["id"]
    parent_id2 = execute_post(client, "/object", {"type": "test_parent2"}, {"stage": False})["id"]

    list = [
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
    ]

    res = execute_post(client, "/nodes", {"list": list}, {"stage": False})
    print(f"nodes response {res}")
    object_ids = res["ids"]
    payloads = execute_get(client, "/read_all", {"stage": False, "table": "payload"})["list"]
    objects = execute_get(client, "/read_all", {"stage": False, "table": "object"})["list"]

    root1 = execute_get(client, "/object/tree", {"stage": False, "id": parent_id1, "payload_data": True})
    root2 = execute_get(client, "/object/tree", {"stage": False, "id": parent_id2, "payload_data": True})

    assert len(object_ids) == 3
    assert len(payloads) == 6
    assert len(objects) == 7
    assert len(root1["children"]) == 3
    assert len(root1["children"][0]["payloads"]) == 2
    assert root1["children"][0]["payloads"][0]["data"] == "payload data"
    assert len(root1["children"][0]["children"]) == 2
    assert root1["children"] == root2["children"]


def test_add_to_node(database_connection: Database):
    client = database_connection

    execute_post(client, "/tag", {"type": "runkey", "name": "root_tag", "author": "pytest"})

    root_uuid = execute_post(
        client,
        "/node",
        {
            "type": "felix",
            "payloads": [
                {"data": "root_node payload", "type": "config", "name": "test"},
                {"data": "connectivity data", "type": "connect", "name": "test"},
            ],
            "tags": ["root_tag"],
        },
        {"stage": True},
    )["id"]

    felix1_payloads = [
        {"data": "payload_felix01", "type": "felix", "name": "test"},
        {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
    ]
    felix1_uuid = execute_post(
        client,
        "/node",
        {"type": "felix", "payloads": felix1_payloads, "parents": [{"id": root_uuid}]},
        {"stage": True},
    )["id"]

    felix2_payloads = [{"data": "payload_felix02", "type": "felix", "name": "test"}]
    execute_post(
        client,
        "/node",
        {"type": "felix", "payloads": felix2_payloads, "parents": [{"id": root_uuid}]},
        {"stage": True},
    )

    lpgbt1_payload = [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}]
    lpgbt1_uuid = execute_post(
        client,
        "/node",
        {"type": "lpgbt", "payloads": lpgbt1_payload, "parents": [{"id": felix1_uuid}]},
        {"stage": True},
    )["id"]

    lpgbt2_payload = [{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}]
    execute_post(
        client,
        "/node",
        {"type": "lpgbt", "payloads": lpgbt2_payload, "parents": [{"id": felix1_uuid}]},
        {"stage": True},
    )

    lpgbt3_payload = [{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}]
    execute_post(
        client,
        "/node",
        {"type": "lpgbt", "payloads": lpgbt3_payload, "parents": [{"id": felix1_uuid}]},
        {"stage": True},
    )

    fe1_payload = [{"data": "payload_fe01", "type": "frontend", "name": "test"}]
    execute_post(
        client,
        "/node",
        {"type": "frontend", "payloads": fe1_payload, "parents": [{"id": lpgbt1_uuid}]},
        {"stage": True},
    )

    fe2_payload = [{"data": "payload_fe02", "type": "frontend", "name": "test"}]
    fe2 = execute_post(
        client,
        "/node",
        {"type": "frontend", "payloads": fe2_payload, "parents": [{"id": lpgbt1_uuid}]},
        {"stage": True},
    )

    new_payload = {"data": "additional payload_fe02", "type": "front", "name": "test"}
    execute_post(client, "/on_node", {"payloads": [new_payload]}, {"stage": True, "id": fe2["id"]})
    new_data = copy.deepcopy(std_data)
    new_data["children"][0]["children"][0]["children"][1]["payloads"].append(new_payload)

    tree = execute_get(client, "/tag/tree", {"payload_data": True, "name": "root_tag", "stage": True})["objects"][0]

    output = extract_dict(tree)
    input = extract_dict(new_data)

    t1 = sort_list(input)
    t2 = sort_list(output)

    assert t1 == t2
