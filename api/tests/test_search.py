from configdb_server.database_tools import Database
from tests.tools import execute_post


def test_search_opto(example_runkey_data_meta: Database):
    client, data = example_runkey_data_meta

    payloads = execute_post(client, "/search", {"search_dict": {"serial": "123"}}, {"identifier": "root_tag", "object_type": "optoboard"})

    assert len(payloads["data"][0]["payloads"]) == 2

    assert payloads["data"][0]["payloads"][0]["data"] == "payload_lpgbt03"


def test_search_for_subtree(example_runkey_data_meta: Database):
    client, data = example_runkey_data_meta

    trees = execute_post(client, "/search/subtree", {"search_dict": {"serial": "100"}}, {"identifier": "root_tag", "object_type": "optoboard"})

    trees = trees["data"]

    assert len(trees) == 2
    assert len(trees[0]["children"]) == 2
    assert len(trees[1]["children"]) == 2


def test_search_in_tag(database_connection: Database):
    client = database_connection

    payl1 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as01"}, {"stage": False})["id"]
    payl2 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds01"}, {"stage": False})["id"]
    payl3 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts01"}, {"stage": False})["id"]

    payl4 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as02"}, {"stage": False})["id"]
    payl5 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds02"}, {"stage": False})["id"]
    payl6 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts02"}, {"stage": False})["id"]

    payl7 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as03"}, {"stage": False})["id"]
    payl8 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds03"}, {"stage": False})["id"]
    payl9 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts03"}, {"stage": False})["id"]

    meta1 = execute_post(client, "/payload", {"type": "meta", "data": '{"serial": "eins"}', "meta": True}, {"stage": False})["id"]
    meta2 = execute_post(client, "/payload", {"type": "meta", "data": '{"serial": "zwei"}', "meta": True}, {"stage": False})["id"]

    obj1 = execute_post(client, "/object", {"type": "frontend", "payloads": [payl1, payl2, payl3, meta1]}, {"stage": False})["id"]
    execute_post(client, "/object", {"type": "frontend", "payloads": [payl4, payl5, payl6, meta2]}, {"stage": False})["id"]

    execute_post(
        client,
        "/tag",
        {"name": "tag1", "type": "scans", "author": "pytest", "payloads": [payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9]},
        {"stage": False},
    )["id"]

    payloads = execute_post(client, "/search/tag", {"payload_types": ["as", "ds"], "object_ids": [obj1]}, {"name": "tag1"})

    assert len(payloads["data"]) == 2
    assert payloads["data"][0]["object_id"] == obj1
    assert "data" not in payloads["data"][0]


def test_search_in_tag_order(database_connection: Database):
    client = database_connection

    payl1 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as01"}, {"stage": False})["id"]
    payl2 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds01"}, {"stage": False})["id"]
    payl3 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts01"}, {"stage": False})["id"]

    payl4 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as02"}, {"stage": False})["id"]
    payl5 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds02"}, {"stage": False})["id"]
    payl6 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts02"}, {"stage": False})["id"]

    payl7 = execute_post(client, "/payload", {"type": "as", "data": "as_data", "name": "as03"}, {"stage": False})["id"]
    payl8 = execute_post(client, "/payload", {"type": "ds", "data": "ds_data", "name": "ds03"}, {"stage": False})["id"]
    payl9 = execute_post(client, "/payload", {"type": "ts", "data": "ts_data", "name": "ts03"}, {"stage": False})["id"]

    meta1 = execute_post(client, "/payload", {"type": "meta", "data": '{"serial": "eins"}', "meta": True}, {"stage": False})["id"]
    meta2 = execute_post(client, "/payload", {"type": "meta", "data": '{"serial": "zwei"}', "meta": True}, {"stage": False})["id"]

    obj1 = execute_post(client, "/object", {"type": "frontend", "payloads": [payl1, payl2, payl3, meta1]}, {"stage": False})["id"]
    execute_post(client, "/object", {"type": "frontend", "payloads": [payl4, payl5, payl6, meta2]}, {"stage": False})["id"]

    execute_post(
        client,
        "/tag",
        {"name": "tag1", "author": "pytest", "type": "scans", "payloads": [payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9]},
        {"stage": False},
    )["id"]

    payloads = execute_post(client, "/search/tag", {"payload_types": ["as", "ds"], "object_ids": [obj1]}, {"name": "tag1", "order_by_object": True})

    assert len(payloads["data"]) == 1
    assert len(list(iter(payloads["data"].values()))[0]) == 2
    assert "data" not in list(iter(payloads["data"].values()))[0]


# with open('sales.txt', 'w') as fp:
#     fp.write(output.__repr__())
