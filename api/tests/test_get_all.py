from configdb_server.database_tools import Database
from configdb_server.testing_tools import extract_dict, flatten_list, extract_list, pop_ids
from tests.tools import execute_get

def test_get_all(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lst = execute_get(client, "/read_all", {"stage": True, "table": "payload", "payload_data": True})

    output = extract_list(lst["list"])
    input = flatten_list(extract_dict(data))

    assert input == output


def test_get_all_object(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lst = execute_get(client, "/read_all", {"stage": True, "table": "payload", "payload_data": True})

    output = extract_list(lst["list"])
    input = flatten_list(extract_dict(data))
    assert input == output


def test_get_all_filter(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lst = execute_get(client, "/read_all", {"stage": True, "filter": "config", "table": "payload", "payload_data": True})

    output = extract_list(lst["list"])

    assert [{"type": "config", "name": "test", "data": "root_node payload"}] == output


def test_get_all_payload_filter(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lst = execute_get(client, "/read_all", {"stage": True, "payload_filter": "config", "table": "object", "depth": 0})

    output = []
    for item in lst["list"]:
        output.append(pop_ids(item))

    assert len(output[0]["payloads"]) == 2
    assert output[0]["payloads"][0] == {"type": "config", "name": "test", "meta": False} or output[0]["payloads"][0] == {
        "type": "connect",
        "name": "test",
        "meta": False,
    }
    assert output[0]["payloads"][1] == {"type": "config", "name": "test", "meta": False} or output[0]["payloads"][1] == {
        "type": "connect",
        "name": "test",
        "meta": False,
    }


def test_get_all_child_filter(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    lst = execute_get(client, "/read_all", {"stage": True, "child_filter": "felix", "table": "object", "depth": 0})

    output = []
    for item in lst["list"]:
        output.append(pop_ids(item))

    assert len(output[0]["payloads"]) == 2
    assert output[0]["payloads"][0] == {"type": "config", "name": "test", "meta": False} or output[0]["payloads"][0] == {
        "type": "connect",
        "name": "test",
        "meta": False,
    }
    assert output[0]["payloads"][1] == {"type": "config", "name": "test", "meta": False} or output[0]["payloads"][1] == {
        "type": "connect",
        "name": "test",
        "meta": False,
    }