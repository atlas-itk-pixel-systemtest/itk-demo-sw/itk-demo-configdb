from configdb_server.database_tools import Database
from tests.tools import execute_post, execute_get


def test_read_payload(database_connection: Database):
    client = database_connection
    id = execute_post(client, "payload", {"type": "test", "data": "test_data", "name": "test.txt"}, {"stage": False})["id"]
    payload = execute_get(client, "payload", {"stage": False, "id": id})

    assert payload["data"] == "test_data"
    assert payload["type"] == "test"
    assert payload["name"] == "test.txt"


def test_read_payload_error(database_connection: Database):
    client = database_connection
    id = execute_post(client, "payload", {"type": "test", "data": "test_data", "name": "test.txt"}, {"stage": False})["id"]

    error = execute_get(client, "payload", {"stage": False, "id": id, "meta": True})

    error["title"] = "Dataset not found"


def test_read_payload_stage(example_runkey_data: tuple[Database, list[str]]):
    client = example_runkey_data[0]

    execute_post(client, "/stage/commit", {"identifier": "root_tag", "name": "backend_tag", "author": "pytest"})
    execute_post(client, "/stage/clone", {"identifier": "backend_tag", "name": "root_tag", "author": "pytest"})

    tag = execute_get(client, "/tag/tree", {"name": "root_tag", "stage": True})

    object_payload = tag["objects"][0]["payloads"][0]

    object_payload_data = execute_get(client, "/payload", {"id": object_payload["id"], "stage": True})

    assert object_payload_data["data"] != "null"