from configdb_server.database_tools import Database
from configdb_server.testing_tools import extract_dict, sort_list
from tests.tools import execute_post, execute_get


def test_get_tag_metadata(example_runkey_data_scan: tuple[Database, list[str]]):
    client, data = example_runkey_data_scan

    tag = execute_get(client, "/tag/tree", {"name": "root_tag", "stage": True, "payload_data": True})["objects"][0]

    extract_dict(tag)
    extract_dict(data)

    for children in tag["children"]:
        if children["type"] == "scan":
            assert children["metadata"]["scan_type"] == "digitalscan"

def test_get_tag_comment(database_connection: Database):
    client = database_connection

    execute_post(client, "/tag", {"name": "my_tag", "type": "runkey", "comment": "test_comment", "author": "pytest"}, {"stage": True})

    tag = execute_get(client, "/tag", {"name": "my_tag", "stage": True})

    assert tag["comment"] == "test_comment"


def test_get_tag(example_runkey_data_scan: tuple[Database, list[str]]):
    client, data = example_runkey_data_scan

    id = execute_get(client, "/tag", {"payload_data": True, "name": "root_tag", "stage": True})["objects"][0]
    execute_post(client, "/tag", {"name": "my_tag", "objects": [id], "type": "runkey", "author": "pytest"}, {"stage": True})

    tag = execute_get(client, "/tag/tree", {"name": "my_tag", "stage": True, "payload_data": True})
    tree = tag["objects"][0]

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_get_tree_object(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    id = execute_get(client, "/tag", {"name": "root_tag", "stage": True})["objects"][0]
    tree = execute_get(client, "/object/tree", {"id": id, "stage": True, "payload_data": True})

    input = extract_dict(data)
    output = extract_dict(tree)

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    tree = execute_get(client, "/tag/tree", {"name": "root_tag", "stage": True, "payload_data": True})

    input = extract_dict(data)
    output = extract_dict(tree["objects"][0])

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag_filter(example_runkey_data: tuple[Database, list[str]]):
    client, data = example_runkey_data

    tree = execute_get(client, "/tag/tree", {"name": "root_tag", "stage": True, "payload_data": True, "payload_filter": "frontend"})

    output = extract_dict(tree["objects"][0])
    sort = sort_list(output)

    assert len(sort[1]) == 0
    assert len(sort[0][1]) == 0
    assert len(sort[0][2]) == 0
    assert len(sort[0][0][0]) == 1
    assert len(sort[0][0][1]) == 1