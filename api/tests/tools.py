from json import loads, dumps


def execute_post(client, url, body={}, parameter={}):
    if not url.startswith("/"):
        url = "/" + url
    res = client.post(
        "api" + url,
        data=dumps(body),
        params=parameter,
        headers={"content-type": "application/json"},
    )
    return loads(res.text)


def execute_get(client, url, parameter={}):
    if not url.startswith("/"):
        url = "/" + url

    res = client.get(
        "api" + url, params=parameter, headers={"content-type": "application/json"}
    )
    return loads(res.text)

def execute_delete(client, url, parameter={}):
    if not url.startswith("/"):
        url = "/" + url

    res = client.delete(
        "api" + url, params=parameter, headers={"content-type": "application/json"}
    )
    return loads(res.text)
