import React from 'react'
import ReactDOM from 'react-dom/client'
import '@patternfly/react-core/dist/styles/base.css'
import './style.css'
import ConfigdbDashboard from './screens/dashboard'
import PanelBadge from './admin-panel/panel-badge'
import TreePanel from './screens/tree-panel'

const searchString = window.location.search
let search = { path: '/' }
if (searchString.length > 0) {
  search = searchString.slice(1).split('&').map(s => s.split('=')).reduce((obj, [param, value]) => {
    obj[param] = value
    return obj
  }, {})
}

const sites = {
  '/':
    <div className='fill-window'>
      <ConfigdbDashboard />
      <PanelBadge />
    </div>,
  tree:
    <div className='fill-window'>
      <ConfigdbDashboard panels={[{
        title: 'Tag Tree',
        Content: TreePanel,
        props: { searchParams: search }
      }]} />
      <PanelBadge />
    </div>
}

if (!('path' in search && search.path in sites)) window.location.search = ''

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  sites[search.path]
)
