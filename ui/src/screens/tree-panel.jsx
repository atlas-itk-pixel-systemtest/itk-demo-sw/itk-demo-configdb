import React, { useEffect } from 'react' //, { useEffect, useState } from 'react'able
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Components
import { Tree } from '@itk-demo-sw/components'
import PayloadView from '../components/payload-view'
import PanelTitle from '../components/panel-title'
import tagTreeElements from '../components/tag-tree-elements'
// Hooks
import { useTree } from '@itk-demo-sw/hooks'
import useTagList from '../hooks/useTagList'
import useTagLoader from '../hooks/useTagLoader'
import usePayloadLoader from '../hooks/usePayloadLoader'
// Proptypes
import PropTypes from 'prop-types'

const TreePanel = (props) => {
  // const navigate = useNavigate()
  // States and custom Hooks
  const [tagList, updateTagList] = useTagList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState)
  const [
    curTagData,
    curTag,
    curTagType,
    loadTag,
    loadObject,
    lastObject
  ] = useTagLoader(props.dbApiUrl, props.onError)
  const [
    curPayloadData,
    curPayload,
    curPayloadType,
    loadPayload
  ] = usePayloadLoader(props.dbApiUrl, props.onError)
  console.log(curTag)

  const metaDataAttributes = ['type']
  const {
    loadTree,
    toggleComp,
    tree,
    metaData,
    expandedMap,
    addSubTree,
    hasComponentChildren
  } = useTree({ metaDataAttributes, sortBy: metaDataAttributes[0], features: ['dynamic'] })
  const [TagTreeLayer, TagTreePayload] = tagTreeElements()
  const treeComp = (
    <Tree
      tree={tree}
      expandedMap={expandedMap}
      toggleComp={toggleComp}
      metaData={metaData}
      createLevel={(id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton) =>
        TagTreeLayer(id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton, hasComponentChildren, loadObject)}
      createLowestLevel={TagTreePayload}
      onCompSelect={(id) => { loadPayload(id) }}
      isTitleButton={false}
    />
  )

  useEffect(() => {
    updateTagList()
  }, [])

  useEffect(() => {
    tagList.forEach(([name, type, _]) => {
      let validTag = false
      if (name === props.searchParams.name && type === props.searchParams.type) {
        validTag = true
      }
      if (validTag) loadTag(props.searchParams.name, props.searchParams.type)
    })
  }, [tagList])

  useEffect(() => {
    loadTree(curTagData)
  }, [
    curTagData
  ])

  useEffect(() => {
    addSubTree(lastObject, metaData && lastObject && metaData[lastObject.id] && metaData[lastObject.id].parent)
  }, [
    lastObject
  ])

  // React components
  const title = (
    <PanelTitle
      title={'Tag Tree'}
      subtext={`Inspect the ${curTagType}`}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  const payloadView = (
    <PayloadView
      curPayloadData={curPayloadData}
      curPayloadTag={curPayload}
      curPayloadType={curPayloadType}
    />
  )

  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Flex>
          <Flex>
            <FlexItem>
              {treeComp}
            </FlexItem>
          </Flex>
          <Flex
            direction={{ default: 'column' }}
            grow={{ default: 'grow' }}
          >
            {payloadView}
          </Flex>
        </Flex>
      </PageSection>
    </Page>
  )
}

TreePanel.propTypes = {
  componentTypes: PropTypes.array.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  searchParams: PropTypes.object.isRequired
}

export default TreePanel
