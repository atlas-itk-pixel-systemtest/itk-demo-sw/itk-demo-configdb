import React from 'react'
import {
  Bullseye,
  Button,
  FileUpload,
  Flex,
  FlexItem,
  Page,
  PageSection,
  TextArea,
  ValidatedOptions
} from '@patternfly/react-core'
import {
  CheckedTextInput,
  TypeaheadSelectInput
} from '@itk-demo-sw/components'
// Hooks
import useAddPayload from '../hooks/useAddPayload'
import {
  useCheckedTextInput,
  useFileUpload,
  useTypeaheadSelectInput
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const AddPayload = (props) => {
  // States and custom Hooks
  const [
    type,
    isSelectOpen,
    handleSelect,
    handleToggleSelect,
    clearSelection
  ] = useTypeaheadSelectInput()
  const [name, handleSerialChange] = useCheckedTextInput()
  const [filename, data, handleFileChange] = useFileUpload()
  // eslint-disable-next-line no-unused-vars
  const [setPayload] = useAddPayload(
    props.dbApiUrl,
    props.onError,
    props.onSuccess
  )

  return (
    <Page>
      <PageSection>
        <Flex direction={{ default: 'column' }}>
          <Flex>
            {/* <FlexItem grow={{ default: 'grow' }}>
              <CheckedTextInput
                name={'Config Tag'}
                handleChange={handleNameChange}
                value={name}
                width={'100%'}
              />
            </FlexItem> */}
            <FlexItem grow={{ default: 'grow' }}>
              <TypeaheadSelectInput
                clearSelection={clearSelection}
                onSelect={handleSelect}
                onToggle={handleToggleSelect}
                isOpen={isSelectOpen}
                selected={type}
                selectOptions={props.componentTypes}
                placeholderText={'Config Type'}
              />
            </FlexItem>
            <FlexItem grow={{ default: 'grow' }}>
              <CheckedTextInput
                name={'File name'}
                handleChange={handleSerialChange}
                value={name}
                width={'100%'}
              />
            </FlexItem>
          </Flex>
          <FlexItem grow={{ default: 'grow' }}>
            <FileUpload
              id="simple-file"
              type="text"
              filenamePlaceholder={
                'Upload config file or type content manually'
              }
              value={data}
              filename={filename}
              onChange={handleFileChange}
              browseButtonText="Upload"
              hideDefaultPreview
            >
              <TextArea
                resizeOrientation='vertical'
                rows="15"
                aria-label="configContent"
                onChange={(value) => handleFileChange(value, filename)}
                validated={
                  data === ''
                    ? ValidatedOptions.error
                    : ValidatedOptions.success
                }
                value={data}
              />
            </FileUpload>
          </FlexItem>
          <FlexItem>
            <Bullseye>
              <Button
                variant="primary"
                onClick={() => setPayload(type, name, data)}
              >
                Submit
              </Button>
            </Bullseye>
          </FlexItem>
        </Flex>
      </PageSection>
    </Page>
  )
}

AddPayload.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  componentTypes: PropTypes.array.isRequired,
  onError: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default AddPayload
