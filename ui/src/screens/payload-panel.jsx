import React from 'react' //, { useEffect, useState } from 'react'
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Screens
import AddPayload from './add-payload'
// Components
import PayloadTable from '../components/payload-table'
import PayloadView from '../components/payload-view'
import PanelTitle from '../components/panel-title'
import {
  ButtonedModal
} from '@itk-demo-sw/components'
// Hooks
import usePayloadList from '../hooks/usePayloadList'
import usePayloadLoader from '../hooks/usePayloadLoader'
import {
  useButtonedModal
} from '@itk-demo-sw/hooks'
// Proptypes
import PropTypes from 'prop-types'

const ConfigPanel = (props) => {
  // States and custom Hooks
  const [payloadList, updateConfigList] = usePayloadList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState)
  const [
    curPayloadData,
    curPayloadTag,
    curPayloadType,
    loadPayload
  ] = usePayloadLoader(props.dbApiUrl, props.onError)
  const [isModalOpen, toggleModal, closeModal] = useButtonedModal()
  // const typedConfigLists = {}
  // const typedConfigUpdates = {}
  // for (const type of props.componentTypes) {
  //   const [tmpState, tmpSet] = usePayloadList(props.dbApiUrl, props.onError)
  //   typedConfigLists[type] = tmpState
  //   typedConfigUpdates[type] = tmpSet
  // }

  // // Helper Functions
  // const updateConfigList = () => {
  //   for (const updateFunc of Object.values(typedConfigUpdates)) {
  //     updateFunc()
  //   }
  // }

  // // Additional Hooks
  // useEffect(() => {
  //   setConfigList([])
  //   for (const [type, list] of Object.entries(typedConfigLists)) {
  //     setConfigList(prevConfigList => {
  //       const newItems = list.map(
  //         cfgName => [cfgName, type]
  //       )
  //       return prevConfigList.concat(newItems)
  //     })
  //   }
  // }, Object.values(typedConfigLists))

  // React components
  const title = (
    <PanelTitle
      title={'Payload Access'}
      subtext={'Check the status of payloads stored in the Database. ' +
        'You can view existing payloads and add new ones.'}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  const configTable = (
    <PayloadTable
      payloadList={payloadList}
      componentTypes={props.componentTypes}
      onRowClick={(row) => loadPayload(row[2])}
      updateConfigList={updateConfigList}
    />
  )

  const payloadView = (
    <PayloadView
      curPayloadData={curPayloadData}
      curPayloadTag={curPayloadTag}
      curPayloadType={curPayloadType}
    />
  )

  const addPayload = (
    <AddPayload
      dbApiUrl={props.dbApiUrl}
      componentTypes={props.componentTypes}
      onError={props.onError}
      onSuccess={(msg) => {
        props.onSuccess(msg, true)
        closeModal()
        updateConfigList()
      }}
    />
  )

  const addPayloadModal = (
    <ButtonedModal
      buttonText={'Add Payload'}
      content={addPayload}
      isOpen={isModalOpen}
      toggleModal={toggleModal}
    />
  )

  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        {addPayloadModal}
        <Flex>
          <Flex>
            <FlexItem>
              {configTable}
            </FlexItem>
          </Flex>
          <Flex
            direction={{ default: 'column' }}
            grow={{ default: 'grow' }}
          >
            {payloadView}
          </Flex>
        </Flex>
      </PageSection>
    </Page>
  )
}

ConfigPanel.propTypes = {
  componentTypes: PropTypes.array.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired
}

export default ConfigPanel
