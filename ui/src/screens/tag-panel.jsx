import React from 'react' //, { useEffect, useState } from 'react'able
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Components
import TagTable from '../components/tag-table'
import PanelTitle from '../components/panel-title'
// Hooks
import useTagList from '../hooks/useTagList'
// Proptypes
import PropTypes from 'prop-types'

const TagPanel = (props) => {
  // const navigate = useNavigate()
  // States and custom Hooks
  const [tagList, updateTagList] = useTagList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState)

  // React components
  const title = (
    <PanelTitle
      title={'Tag Access'}
      subtext={'Check the status of tags stored in the Database.'}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  const tagTable = (
    <TagTable
      tagList={tagList}
      componentTypes={props.componentTypes}
      onRowClick={(row) => {
        window.open(`/?path=tree&name=${row[0]}&type=${row[1]}`)
      }}
      updateTagList={updateTagList}
    />
  )

  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Flex>
          <Flex>
            <FlexItem>
              {tagTable}
            </FlexItem>
          </Flex>
        </Flex>
      </PageSection>
    </Page>
  )
}

TagPanel.propTypes = {
  componentTypes: PropTypes.array.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired
}

export default TagPanel
