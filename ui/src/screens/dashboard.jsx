import React, { useState, useEffect } from 'react'
import { useConfig } from '../admin-panel/config'
import {
  Alert,
  Page,
  Button,
  PageNavigation,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Screens
import ConfigPanel from './payload-panel'
import TagPanel from './tag-panel'
// Components
import {
  LoggingViewer,
  Navbar,
  Notifications
} from '@itk-demo-sw/components'
// Hooks
import {
  useConfigureValue,
  useLoggingViewer,
  useNavbar,
  useNotifications
} from '@itk-demo-sw/hooks'
// Proptypes
import PropTypes from 'prop-types'

const ConfigdbDashboard = (props) => {
  const [connectionEstablished, setConnectionEstablished] = useState(false)
  const [showLog, setShowLog] = useState(false)
  const [logContent, addLog] = useLoggingViewer()
  const [dbState, setState] = useState('')
  const [dbStateStatus, setStatus] = useState('')
  const [
    alerts,
    onError,
    onWarn,
    onInfo,
    onSuccess,
    deleteAlert,
    onGenericError
  ] = useNotifications(addLog)
  const { getConfig, setConfig } = useConfig()
  const healthFrequency = getConfig('healthFrequency')
  const callHealth = getConfig('callHealth')
  const [intervalId, setIntervalId] = useState(0)
  const dbApiUrl = useConfigureValue('/config', 'urlKey', getConfig('backend'), 'backendUrl')
  useEffect(() => {
    setConfig('backend', dbApiUrl)
  }, [dbApiUrl])

  const componentTypes = [
    'Frontend',
    'FELIX',
    'Optoboard',
    'Scan',
    'Connectivity'
  ]

  const handleLog = response => {
    if ((Object.keys(response).includes('status')) &&
      (Object.keys(response).includes('messages'))) {
      if (response.messages.length > 0) {
        for (const m of response.messages) {
          if (response.status === 200) onInfo(m)
          if (response.status === 299) onWarn(m)
        }
      }
    }
  }

  async function getHealth (apiUrl) {
    return generalRequest(`${apiUrl}/health`).then(
      data => {
        handleLog(data)
        setState(data.state)
        setStatus(data.state_status)
        setConnectionEstablished(true)
        return data.state
      }
    ).catch(
      () => {
        console.log('Failed flask backend connection. URL: ' + apiUrl)
        setConnectionEstablished(false)
        setState('')
        setStatus('')
        return ''
      }
    )
  }

  useEffect(() => {
    clearInterval(intervalId)
    getHealth(dbApiUrl)
    setIntervalId(setInterval(() => { if (callHealth) getHealth(dbApiUrl) }, healthFrequency))
  }, [callHealth, healthFrequency, dbApiUrl])

  const noConnectionAlert = (
    connectionEstablished
      ? null
      : (
        <PageSection
          key={'no connection alert sec'}
          variant={PageSectionVariants.light}
          style={{ padding: '0px' }}
        >
          <Alert
            key={'no connection alert alert'}
            variant='danger'
            isInline
            title='configDB API is not responding!'
          >
            {'Please check if the API backend is running. ' +
              `Currently trying to connect to '${dbApiUrl}'.`}
          </Alert>
        </PageSection>
        )
  )

  const panels = props.panels
    ? props.panels.map(({ title, Content, props }) => {
      return {
        title,
        content: <Content
          componentTypes={componentTypes}
          dbApiUrl={dbApiUrl}
          onError={onError}
          onGenericError={onGenericError}
          onWarn={onWarn}
          onInfo={onInfo}
          onSuccess={onSuccess}
          dbState={dbState}
          dbStateStatus={dbStateStatus}
          updateState={getHealth}
          {...props}
        />
      }
    })
    : [
        {
          title: 'Tags',
          content: <TagPanel
            componentTypes={componentTypes}
            dbApiUrl={dbApiUrl}
            onError={onError}
            onGenericError={onGenericError}
            onWarn={onWarn}
            onInfo={onInfo}
            onSuccess={onSuccess}
            dbState={dbState}
            dbStateStatus={dbStateStatus}
            updateState={getHealth}
          />
        },
        {
          title: 'Payloads',
          content: <ConfigPanel
            componentTypes={componentTypes}
            dbApiUrl={dbApiUrl}
            onError={onError}
            onGenericError={onGenericError}
            onWarn={onWarn}
            onInfo={onInfo}
            onSuccess={onSuccess}
            dbState={dbState}
            dbStateStatus={dbStateStatus}
            updateState={getHealth}
          />
        }
      ]

  const [activeItem, itemNames, changePanel] = useNavbar(
    panels.map((p) => p.title)
  )

  return (
    <Page>
      {noConnectionAlert}
      <PageSection padding={{ default: 'noPadding' }}>
        <PageNavigation key={'page nav'}>
          <Navbar
            key={'navbar'}
            activeItem={activeItem}
            changePanel={changePanel}
            itemNames={itemNames}
            onError={onError}
            onInfo={onInfo}
            onSuccess={onSuccess}
            onWarn={onWarn}
          />
        </PageNavigation>

      </PageSection>
      <Notifications alerts={alerts} deleteAlert={deleteAlert} />
      <PageSection
        isFilled
        hasOverflowScroll
        padding={{ default: 'noPadding' }}
      >
        {panels[activeItem].content}
      </PageSection>
      <PageSection
        key={'logging section'}
        sticky='bottom'
        padding={{ default: 'noPadding' }}
        variant={PageSectionVariants.darker}
        isFilled={false}
        style={{
          backgroundColor: 'RoyalBlue'
        }}
      >
        <Button
          key={'toggle logging button'}
          isBlock
          variant='plain'
          onClick={(event) => setShowLog((prevShowLog) => !prevShowLog)}
        >
          Toggle Logging
        </Button>
        {showLog
          ? (
            <LoggingViewer content={logContent} key={'logging view'} />
            )
          : null
        }
      </PageSection>
    </Page>
  )
}

ConfigdbDashboard.propTypes = {
  panels: PropTypes.array
}

export default ConfigdbDashboard
