import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  Divider,
  EmptyState,
  EmptyStateBody,
  EmptyStateIcon,
  EmptyStateSecondaryActions,
  Pagination,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarFilter,
  ToolbarItem
} from '@patternfly/react-core'
import {
  SearchIcon,
  SyncIcon
} from '@patternfly/react-icons'
// Components
import {
  CheckboxSelect,
  SearchField,
  StandardTable
} from '@itk-demo-sw/components'
// Hooks
import {
  useCheckboxSelect,
  usePagination,
  useSearchField,
  useStandardTable
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const TagTable = (props) => {
  // States and custom Hooks
  const [nameFilter, setNameFilter] = useState('')

  const [
    searchValue,
    handleSearchChange,
    handleSearch,
    clearSearch
  ] = useSearchField(() => setNameFilter(searchValue))

  const [
    isOpen,
    selections,
    onToggle,
    onSelect,
    clearSelections
  ] = useCheckboxSelect()

  const [
    page,
    perPage,
    handleSetPage,
    handlePerPageSelect,
    resetPage
  ] = usePagination(15)

  const [filteredRows, setFilteredRows] = useStandardTable(props.tagList)

  // Helper functions

  const applyFilters = () => {
    const newRows = (
      nameFilter !== '' || selections.length > 0
        ? props.tagList.filter(row => {
          return (
            (nameFilter === '' ||
              row[0].toLowerCase().includes(nameFilter.toLowerCase())
            ) &&
            (selections.length === 0 || selections.includes(row[1]))
          )
        })
        : props.tagList
    )
    setFilteredRows([...newRows])
    resetPage()
  }

  const clearAll = () => {
    clearSearch()
    setNameFilter('')
    clearSelections()
  }

  const handleDelete = (filterOption, id = '') => {
    if (filterOption === 'Name') {
      clearSearch()
      setNameFilter('')
    }
    if (filterOption === 'Type') {
      if (selections.includes(id)) {
        onSelect(null, id)
      }
    }
    resetPage()
  }

  // Additional Hooks
  useEffect(() => {
    applyFilters()
  }, [nameFilter, selections, props.tagList])

  // Constants
  const perPageOptions = [
    { title: '5', value: 5 },
    { title: '10', value: 10 },
    { title: '15', value: 15 },
    { title: '20', value: 20 }
  ]

  const columns = [
    { title: 'Name' },
    { title: 'Type' },
    { title: 'ID' }
  ]

  // React components
  const searchField = (
    <SearchField
      handleChange={handleSearchChange}
      handleSearch={handleSearch}
      value={searchValue}
      name={'Tag Name'}
    />
  )

  const checkboxSelect = (
    <CheckboxSelect
      isOpen={isOpen}
      onToggle={onToggle}
      onSelect={onSelect}
      options={props.componentTypes}
      selections={selections}
      name={'Tag Type'}
    />
  )

  const refreshButton = (
    <Button
      variant="plain"
      onClick={() => props.updateTagList()}
    >
      <SyncIcon />
    </Button>
  )

  const pagination = (
    <Pagination
      itemCount={filteredRows.length}
      perPage={perPage}
      page={page}
      perPageOptions={perPageOptions}
      onPerPageSelect={handlePerPageSelect}
      onSetPage={handleSetPage}
      isCompact
    />
  )

  const toolbar = (
    <Toolbar
      id="tag-list-toolbar"
      clearAllFilters={clearAll}
      collapseListedFiltersBreakpoint="xl"
    >
      <ToolbarContent>
        <ToolbarFilter
          variant="search-filter"
          categoryName="Name"
          chips={nameFilter === '' ? [] : [nameFilter]}
          deleteChip={handleDelete}
        >
          {searchField}
        </ToolbarFilter>
        <ToolbarFilter
          categoryName="Type"
          chips={selections}
          deleteChip={handleDelete}
        >
          {checkboxSelect}
        </ToolbarFilter>
        <ToolbarItem>
          {refreshButton}
        </ToolbarItem>
        <ToolbarItem>
          {pagination}
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )

  const emptyTable = (
    <Bullseye>
      <EmptyState>
        <EmptyStateIcon icon={SearchIcon} />
        <Title headingLevel="h5" size="lg">
          No results found
        </Title>
        <EmptyStateBody>
          {'No results match this filter criteria. ' +
            'Clear all filters to show results.'}
        </EmptyStateBody>
        <EmptyStateSecondaryActions>
          <Button variant="link" onClick={() => clearAll()}>
            Clear all filters
          </Button>
        </EmptyStateSecondaryActions>
      </EmptyState>
    </Bullseye>
  )

  const table = (
    <StandardTable
      rows={filteredRows.slice((page - 1) * perPage, page * perPage)}
      columns={columns}
      emptyTable={emptyTable}
      isHoverable={props.onRowClick !== null}
      onRowClick={props.onRowClick ? props.onRowClick : () => { }}
    />
  )

  return (
    <React.Fragment>
      {toolbar}
      <Divider />
      {table}
    </React.Fragment>
  )
}

TagTable.propTypes = {
  tagList: PropTypes.array.isRequired,
  componentTypes: PropTypes.array.isRequired,
  onRowClick: PropTypes.func.isRequired,
  updateTagList: PropTypes.func.isRequired
}

export default TagTable
