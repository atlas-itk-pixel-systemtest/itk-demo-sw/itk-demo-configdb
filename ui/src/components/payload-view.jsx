import React from 'react'
import {
  Divider,
  Text,
  TextArea,
  TextContent
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

const PayloadView = (props) => {
  return (
    <TextContent>
      <Text
        style={{
          color: 'rgb(100,149,237,1)',
          fontSize: '28px',
          whiteSpace: 'pre-wrap'
        }}
      >
        {'Payload View'}
      </Text>
      <Divider />
      <Text
        component="p"
        style={{
          color: props.curPayloadTag ? 'DarkBlue' : 'rgb(100,149,237,0.7)',
          fontSize: '22px',
          display: 'inline-block',
          whiteSpace: 'pre-wrap'
        }}
      >
        {props.curPayloadData ? props.curPayloadTag : 'No payload selected.'}
      </Text>
      <Text
        component="p"
        style={{
          color: 'rgb(100,149,237,1)',
          fontStyle: 'italic',
          fontSize: '20px',
          display: 'inline-block',
          whiteSpace: 'pre-wrap'
        }}
      >
        {'   ' + props.curPayloadType}
      </Text>
      {/* <Text
        style={{
          width: '100%',
          height: '500px',
          borderStyle: 'solid',
          borderWidth: '1px',
          backgroundColor: 'AliceBlue',
          whiteSpace: 'pre-wrap'
        }}
      >
        {props.curPayloadData}
      </Text> */}

      <TextArea
        aria-label="config-text-area"
        value={typeof props.curPayloadData === 'object' ? JSON.stringify(props.curPayloadData) : props.curPayloadData}
        resizeOrientation='vertical'
        isReadOnly
        rows={20}
        autoResize
        style={{
          width: '100%',
          height: '500px',
          borderStyle: 'solid',
          borderWidth: '1px',
          backgroundColor: 'rgb(240,248,255,0.7)',
          color: 'rgb(0,0,0,0.85)',
          whiteSpace: 'pre-wrap'
        }}
      />
    </TextContent>
  )
}

PayloadView.propTypes = {
  curPayloadData: PropTypes.string.isRequired,
  curPayloadTag: PropTypes.string.isRequired,
  curPayloadType: PropTypes.string.isRequired
}

export default PayloadView
