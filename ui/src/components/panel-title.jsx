import React from 'react'
import {
  Flex,
  FlexItem,
  PageSection,
  Text,
  Button,
  TextContent
} from '@patternfly/react-core'
import {
  DatabaseIcon
} from '@patternfly/react-icons'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// PropTypes
import PropTypes from 'prop-types'

const PanelTitle = (props) => {
  let statusMessage = 'unknow database state'
  let buttonUrl = `${props.dbApiUrl}/health`
  let buttonText = 'Check state'
  let successText = ''
  if (props.dbState === 'DatabaseNotConnected') {
    statusMessage = 'DB not connected'
    buttonUrl = `${props.dbApiUrl}/database/connect`
    buttonText = 'Connect'
    successText = 'Connected to database backend :)'
  } else if (props.dbState === 'DatabaseNotCreated') {
    statusMessage = 'DB not created'
    buttonUrl = `${props.dbApiUrl}/database/create`
    buttonText = 'Create'
    successText = 'Database successfully created'
  } else if (props.dbState === 'DatabaseOutdated') {
    statusMessage = 'DB not up to date'
    buttonUrl = `${props.dbApiUrl}/database/upgrade`
    buttonText = 'Upgrade'
    successText = 'Database tables successfully upgraded.'
  } else if (props.dbState === 'Busy') {
    statusMessage = `Busy - ${props.dbStateStatus}`
  }

  const actionButton = (
    <Button onClick={() => {
      generalRequest(buttonUrl).then(
        data => {
          props.onInfo(successText)
          props.updateState(props.dbApiUrl)
        }
      ).catch(
        err => props.onError(err)
      )
    }}>
      {buttonText}
    </Button>
  )

  return (
    <PageSection
      style={{
        background: 'linear-gradient(AliceBlue,white)',
        backgroundColor: 'White'
      }}
    >
      <Flex key={`${props.title} title flex`}>
        <FlexItem key={`${props.title} title text flex item`}>
          <TextContent key={`${props.title} textcon`}>
            <Text
              key={`${props.title} title text`}
              component="h1"
              style={{
                color: 'RoyalBlue',
                fontSize: '30px',
                display: 'inline-block',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.title}
            </Text>
            <Text
              key={`${props.title} subtitle text`}
              component="p"
              style={{
                color: 'RoyalBlue',
                fontSize: '20px',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.subtext}
            </Text>
          </TextContent>
        </FlexItem>
        <FlexItem
          key={`${props.title} title icon flex item`}
          align={{ default: 'alignRight' }}
        >
          <DatabaseIcon
            key={'scan panel dbavail icon'}
            style={{
              fontSize: '20px',
              color: props.dbState === 'Active' ? 'green' : 'grey'
            }}
          />
        </FlexItem>
        <FlexItem key={`${props.title} db avail text flex item`}>
          <TextContent key={'scan panel dbavail textcon'}>
            <Text
              key={'scan panel dbavail text'}
              component='p'
              style={{
                fontStyle: 'italic',
                fontSize: '20px',
                color: props.dbState === 'Active' ? 'green' : 'grey'
              }}
            >
              {props.dbState === 'Active'
                ? 'Backend DB connected'
                : statusMessage
              }
            </Text>
            {props.dbState === 'Active' || props.dbState === 'Busy' ? null : actionButton}
          </TextContent>
        </FlexItem>
      </Flex>
    </PageSection>
  )
}

PanelTitle.propTypes = {
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtext: PropTypes.string.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  updateState: PropTypes.func.isRequired
}

export default PanelTitle
