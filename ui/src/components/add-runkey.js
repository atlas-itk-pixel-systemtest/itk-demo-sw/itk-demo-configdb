import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  FileUpload,
  Flex,
  FlexItem,
  Grid,
  GridItem,
  Text,
  TextContent,
  TextInput,
  ValidatedOptions
} from '@patternfly/react-core';
import {
  MinusIcon,
  PlusIcon
} from '@patternfly/react-icons';
import TypeaheadSelectInput, {TypeaheadSelectInputNoValidate} from 'basic-gui-elements/typeahead-select-input';
import { getPostBody, checkResponse } from 'basic-gui-elements/utility-functions';


const getConfigList = (configOptions, setConfigLists, dbApiUrl, getStatustext, onError) => {
  const promises = [];
  for (const type of configOptions) {
    const requestData = {
      table: "configuration_tag",
      order_by: "name",
      type: type.toLowerCase()
    };
    promises.push(
      fetch(
        `${dbApiUrl}/get_all`, getPostBody(requestData)
      ).then(
        (response) => checkResponse(response)
      ).then(
        (data) => {
          if (data.status !== 1) throw new Error(getStatustext(data.status));
          let obj = {};
          obj[type] = data.list;
          return obj;
        }
      ).catch(
        err => {
          onError(err);
          let obj = {};
          obj[type] = [];
          return obj;
        }
      )
    );
  }

  Promise.all(promises).then((values) => {
    let newConfigList = {};
    for (let obj of values) {
      newConfigList = { ...newConfigList, ...obj };
    }
    setConfigLists(newConfigList);
  });
}


const submitRunkey = (name, connListUnfiltered, dbApiUrl, getStatustext, onError, onSuccess) => {
  const connList = connListUnfiltered.filter(conn => !conn.hidden);
  const types = ["felix", "optoboard", "frontend", "connectivity"];
  const connPromises = [];

  for (const conn of connList) {
    // Upload of new configs and obtaining of other configs IDs
    const configPromises = [];
    for (const type of types) {
      if (conn[type].upload) {
        const body = {
          type: type,
          data: conn[type].content
        }
        configPromises.push(
          fetch(
            `${dbApiUrl}/configuration/set`, getPostBody(body)
          ).then(
            response => checkResponse(response)
          ).then(
            (data) => {
              if (data.status !== 1) throw new Error(getStatustext(data.status));
              return data.id;
            }
          ).catch(
            err => { onError(err); return -1; }
          )
        )
      }
      else {
        const body = {
          name: conn[type].name,
          table: "configuration"
        }
        configPromises.push(
          fetch(
            `${dbApiUrl}/tag/get`, getPostBody(body)
          ).then(
            response => checkResponse(response)
          ).then(
            (data) => {
              if (data.status !== 1) throw new Error(getStatustext(data.status));
              return data.dataset;
            }
          ).catch(
            err => { onError(err); return -1; }
          )
        )
      }
    }

    //Upload of connectivities based on obtained config-ids
    connPromises.push(
      Promise.all(configPromises).then(
        (values) => {
          const body={}
          for (let i=0; i<types.length; i++){
            body[types[i]] = values[i]
          }
          return fetch(
            `${dbApiUrl}/connectivity/set`, getPostBody(body)
          ).then(
            response => checkResponse(response)
          ).then(
            (data) => {
              if (data.status !== 1) throw new Error(getStatustext(data.status));
              return data.id;
            }
          ).catch(
            err => { onError(err); return -1; }
          )
        }
      )
    );
  }
  
  Promise.all(connPromises).then((values) => {
    fetch(
      `${dbApiUrl}/runkey/set`, getPostBody({connects: values})
    ).then(
      response => checkResponse(response)
    ).then(
      data => {
        if (data.status !== 1) throw new Error(getStatustext(data.status))
        const body = {
          name: name,
          table: "runkey",
          dataset: data.id
        }
        fetch(
          `${dbApiUrl}/tag/set`, getPostBody(body)
        ).then(
          response => checkResponse(response)
        ).then(
          data2 => {
            if (data2.status !== 1) throw new Error(getStatustext(data2.status))
            onSuccess(`Successfully added new runkey with tag "${name}" to the database.`)
          } 
        ).catch(onError);
      }
    ).catch(onError);
  });
}


const ConfigSelection = (props) => {
  const select = (
    <TypeaheadSelectInputNoValidate
      selectOptions={props.payloadList ? props.payloadList : []}
      onSelect={props.handleSelect}
      placeholderText={`Select ${props.type} config`}
      initialSelect={props.name}
      maxheight="450%"
    />
  );

  const upload = (
    <FileUpload
      id="simple-file"
      type="text"
      filenamePlaceholder={`Upload ${props.type} config`}
      value={props.content}
      filename={props.name}
      onChange={props.handleFileChange}
      hideDefaultPreview
      browseButtonText="Upload"
    />
  );

  return (
    <Flex spaceItems={{ default: 'spaceItemsNone' }}>
      <Flex >
        <FlexItem style={{width: "400px"}}>
          {props.upload ? upload : select}
        </FlexItem>
      </Flex>
      <Button 
        variant="control"
        onClick={props.onUploadChange}
        style={{width: "100px"}}
      >
        {props.upload ? "From DB" : "Upload"}
      </Button>
    </Flex>
  );
}


const ConnectivitySlice = (props) => {
  const onUploadChange = (type) => {
    const newConn = props.curConn; 
    newConn[type] = {
      name: "",
      content: null,
      upload: !props.curConn[type].upload
    }
    props.updateConn(newConn);
  }

  const handleSelect = (type, value) => {
    const newConn = props.curConn; 
    newConn[type] = {
      ...props.curConn[type],
      name: value
    }
    props.updateConn(newConn);
  }

  const handleFileChange = (type, value, filename) => {
    const newConn = props.curConn; 
    newConn[type] = {
      ...props.curConn[type],
      name: filename,
      content: value
    }
    props.updateConn(newConn);
  }

  const felix = (
    <ConfigSelection
      payloadList={props.configLists.felix ? props.configLists.felix : []}
      handleSelect={(value) => handleSelect("felix", value)}
      handleFileChange={(value, filename, event) => handleFileChange("felix", value, filename)}
      onUploadChange={() =>onUploadChange("felix")}
      type={"FELIX"}
      name={props.curConn.felix.name}
      content={props.curConn.felix.content}
      upload={props.curConn.felix.upload}
    />
  );

  const optoboard = (
    <ConfigSelection
      payloadList={props.configLists.optoboard ? props.configLists.optoboard : []}
      handleSelect={(value) => handleSelect("optoboard", value)}
      handleFileChange={(value, filename, event) => handleFileChange("optoboard", value, filename)}
      onUploadChange={() =>onUploadChange("optoboard")}
      type={"Optoboard"}
      name={props.curConn.optoboard.name}
      content={props.curConn.optoboard.content}
      upload={props.curConn.optoboard.upload}
    />
  );

  const frontend = (
    <ConfigSelection
      payloadList={props.configLists.frontend ? props.configLists.frontend : []}
      handleSelect={(value) => handleSelect("frontend", value)}
      handleFileChange={(value, filename, event) => handleFileChange("frontend", value, filename)}
      onUploadChange={() =>onUploadChange("frontend")}
      type={"Frontend"}
      name={props.curConn.frontend.name}
      content={props.curConn.frontend.content}
      upload={props.curConn.frontend.upload}
    />
  );

  const connectivity = (
    <ConfigSelection
      payloadList={props.configLists.connectivity ? props.configLists.connectivity : []}
      handleSelect={(value) => handleSelect("connectivity", value)}
      handleFileChange={(value, filename, event) => handleFileChange("connectivity", value, filename)}
      onUploadChange={() =>onUploadChange("connectivity")}
      type={"Connectivity"}
      name={props.curConn.connectivity.name}
      content={props.curConn.connectivity.content}
      upload={props.curConn.connectivity.upload}
    />
  );


  const setHidden = () => {
    props.updateConn({
      ...props.curConn,
      hidden: true
    })
  }

  const removeButton = (
    <Button
      variant='plain'
      onClick={() => setHidden()}
    >
      <MinusIcon style={{ color: "#C9190B" }} />
    </Button>
  );

  return (
    <React.Fragment>
    <GridItem span={1}>
      <Bullseye>
      <TextContent>
        <Text component="h1">{`${props.num+1}.`}</Text>
      </TextContent>
      </Bullseye>
    </GridItem>
    <GridItem span={10}>
      <Flex grow={{ default: 'grow' }}>
        <Flex grow={{ default: 'grow' }}>
          <FlexItem>
            {felix}
          </FlexItem>
          <FlexItem >
            {optoboard}
          </FlexItem>
        </Flex>
        <Flex grow={{ default: 'grow' }}>
          <FlexItem >
            {frontend}
          </FlexItem>
          <FlexItem >
            {connectivity}
          </FlexItem>
        </Flex>
      </Flex>
    </GridItem>
    <GridItem span={1}>
      <Bullseye>
        {removeButton}
      </Bullseye>
    </GridItem>
    </React.Fragment>
  );
}


const AddRunkey = (props) => {
  const [configLists, setConfigLists] = useState({});
  const [runkeyName, setRunkeyName] = useState("");
  const [connList, setConnList] = useState([{
    id: 0,
    felix: {
      name: "",
      content: null,
      upload: true
    },
    optoboard: {
      name: "",
      content: null,
      upload: true
    },
    frontend: {
      name: "",
      content: null,
      upload: true
    },
    connectivity: {
      name: "",
      content: null,
      upload: true
    },
    hidden: false
  }]);


  const onError = err => props.onLog({variant: "danger", text: err.message});
  const onSuccess = (log) => props.onLog({variant: "success", text: log})

  const updateFeConn = (i, newConn) => {
    const curFeList = connList;
    curFeList[i] = newConn;
    setConnList([...curFeList]);
  }

  const addFeConn = () => {
    const curFeList = connList;
    const visibleFeList = curFeList.filter(conn => !conn.hidden);
    curFeList.push({
      ...visibleFeList[visibleFeList.length - 1],
      id: curFeList.length,
      hidden: false
    });
    setConnList([...curFeList]);
  }

  useEffect(() => {
    const configOptions = ["frontend", "optoboard", "felix", "connectivity"];
    getConfigList(configOptions, setConfigLists, props.dbApiUrl, props.getStatustext, onError);
  }, [props.dbApiUrl]);


  const runkeyNameInput = (
    <React.Fragment>
      <GridItem>
        <Flex>
          <FlexItem>
            <TextContent>
              <Text component="h3">{"Runkey Name"}</Text>
            </TextContent>
          </FlexItem>
          <FlexItem>
            <TextInput
              type="text"
              aria-label="rk name input"
              isRequired
              validated={runkeyName === "" ? ValidatedOptions.error : ValidatedOptions.success}
              onChange={(name) => setRunkeyName(name)}
            />
          </FlexItem>
        </Flex>
      </GridItem>
    </React.Fragment>
  );

  const connSlices = connList.filter(
    (conn) => (!conn.hidden)
  ).map((conn, i) => {
    return (
      <React.Fragment key={`conn_fragment_${conn.id}`}>
        <ConnectivitySlice
          key={`conn_${conn.id}`}
          curConn={conn}
          updateConn={(newConn) => updateFeConn(conn.id, newConn)}
          configLists={configLists}
          num={i}
        />
      </React.Fragment>
    );
  }
  );


  const addConnButton = (
    // <GridItem offset={1} span={10}>
    //   <Grid hasGutter>
        <GridItem offset={11} span={1}>
          <Bullseye>
          <Button
            variant="plain"
            onClick={() => addFeConn()}
          >
            <PlusIcon style={{ color: "#3E8635" }} />
          </Button>
          </Bullseye>
        </GridItem>
    //   </Grid>
    // </GridItem>
  );

  const submitButton = (
    <GridItem span={12}>
      <Bullseye>
        <Button
          variant="primary"
          onClick={() => {
            submitRunkey(runkeyName, connList, props.dbApiUrl, props.getStatustext, onError, onSuccess)
          }}
        >
          Submit
        </Button>
      </Bullseye>
    </GridItem>
  );

  const content = (
    <Grid hasGutter>
      {runkeyNameInput}
      <GridItem span={12}>
        <TextContent>
          <Text component="h3">{"Connectivity Slices"}</Text>
        </TextContent>
      </GridItem>
      {connSlices}
      {addConnButton}
      <GridItem span={12} />
      <GridItem span={12} />
      <GridItem span={12} />
      <GridItem span={12} />
      <GridItem span={12} />

      {submitButton}
    </Grid>
  );

  return (
    <React.Fragment>
      {content}
    </React.Fragment>
  );
}


export default AddRunkey;