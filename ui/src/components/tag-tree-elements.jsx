import React from 'react'
import {
  Text,
  DataListContent,
  DataList,
  DataListItem,
  DataListItemRow,
  DataListToggle,
  DataListItemCells,
  DataListCell,
  Button
} from '@patternfly/react-core'
import { StandardTable } from '@itk-demo-sw/components'

const TagTreeLayer = (id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton, hasComponentChildren, loadObject) => {
  const isExpanded = expandedMap[id]
  return (
    <DataListItem
      aria-label={`List ${id}`}
      isExpanded={isExpanded}
      key={`List ${id}`}
    >
      <DataListItemRow
        key={`Item Row ${id}`}
        onClick={() => {
          if (!hasComponentChildren(id)) {
            loadObject(id)
          }
          toggleComp(id)
        }}
      >
        <DataListToggle
          isExpanded={isExpanded}
          key={`List Toggle ${id}`}
        />
        <DataListItemCells
          style={{
            height: '40px'
          }}
          key={`Item Cell ${id}`}
          dataListCells={[
            <DataListCell key={`Name Cell ${id}`}>
              <Text
                key={`Row Title ${id}`}
                style={{
                  fontSize: '14px',
                  color: 'black'
                }}
              >
                <span style={{ color: 'navy', fontWeight: 'bold' }}>{metaData[id].type}:</span>  {id}
              </Text>
            </DataListCell>
          ]}
        />
      </DataListItemRow>
      <DataListContent
        key={`List Content ${id}`}
        isHidden={!isExpanded}
      >
        <DataList
          isCompact
          key={`${id}`}
        >
          {content}
        </DataList>
      </DataListContent>
    </DataListItem>)
}

const TagTreePayload = (id, metaData, onCompSelect) => {
  return (
    <StandardTable
      columns={[]}
      rows={[[
        <Button
          variant='plain'
          key={`TitleButton ${id}`}
          onClick={() => {
            onCompSelect(id)
          }}
        >
          <Text
            key={`type ${id}`}
            style={{
              fontSize: '14px',
              display: 'inline-block'
            }}
          >
            {`${metaData[id].type}: `}<span style={{ userSelect: 'all' }}>{id}</span>
          </Text>
        </Button>
      ]]}
    />
  )
}

const tagTreeElements = () => {
  return [TagTreeLayer, TagTreePayload]
}

export default tagTreeElements
