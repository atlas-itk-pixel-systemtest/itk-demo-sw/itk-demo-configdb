import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function useTagList (
  dbApiUrl,
  // type,
  onError = (data) => { },
  onGenericError = (data) => { },
  updateState = (url) => { }
) {
  const [tagList, setTagList] = useState([])

  const reqBody = {
    // type: type.toLowerCase(),
    table: 'tag',
    stage: false,
    verbosity: 1
  }

  // const niceTypes = {
  //   frontend: 'Frontend',
  //   felix: 'FELIX',
  //   optoboard: 'Optoboard',
  //   scan: 'Scan',
  //   connectivity: 'Connectivity'
  // }

  const updateTag = () => {
    updateState(dbApiUrl).then(
      (state) => {
        if (state === 'Active') {
          generalRequest(
            `${dbApiUrl}/read_all`, reqBody
          ).then(
            data => {
              const newTagList = []
              for (const [, dataset] of data.list.entries()) {
                // console.log(dataset)
                newTagList.push([
                  dataset.name,
                  dataset.type,
                  dataset.id
                ])
              }
              setTagList(newTagList)
            }
          ).catch(
            err => {
              if (err.detail === undefined) {
                onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
              } else {
                onError(err)
              }
            }
          )
        }
      }
    )
  }

  useEffect(() => {
    updateTag()
  }, [dbApiUrl])

  return [tagList, updateTag]
}
