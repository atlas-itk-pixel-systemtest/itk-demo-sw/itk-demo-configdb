import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function useAddPayload (
  dbApiUrl,
  onError = (data) => { },
  onSuccess = (data) => { }
) {
  const setPayload = async (type, name, data) => {
    const reqBody = {
      stage: false,
      type: type,
      name: name,
      data: data
    }
    return generalRequest(
      `${dbApiUrl}/payload/create`, reqBody
    ).then(
      (response) => {
        onSuccess(
          `Saved new payload with id ${response.id}.`
        )
      }
    ).catch(
      (err) => {
        onError(err)
        return -1
      }
    )
  }

  // const setTaggedConfig = async (name, serial, type, data) => {
  //   setPayload(type, serial, data).then(
  //     (id) => {
  //       if (id === -1) return
  //       const reqBody = {
  //         name: name,
  //         table: 'payload',
  //         dataset: id
  //       }
  //       generalRequest(
  //         `${dbApiUrl}/tag/set`, reqBody
  //       ).then(
  //         (data) => {
  //           onSuccess(
  //             `Saved new config with id ${id} and tag '${name}.'`
  //           )
  //         }
  //       ).catch(
  //         (err) => onError(err)
  //       )
  //     }
  //   ).catch(
  //     (err) => onError(err)
  //   )
  // }

  return [setPayload]
}
