import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function usePayloadList (
  dbApiUrl,
  // type,
  onError = (data) => { },
  onGenericError = (data) => { },
  updateState = (url) => { }
) {
  const [payloadList, setConfigList] = useState([])

  const reqBody = {
    // type: type.toLowerCase(),
    table: 'payload',
    stage: false,
    verbosity: 1
  }

  // const niceTypes = {
  //   frontend: 'Frontend',
  //   felix: 'FELIX',
  //   optoboard: 'Optoboard',
  //   scan: 'Scan',
  //   connectivity: 'Connectivity'
  // }

  const updateConfigs = () => {
    updateState(dbApiUrl).then(
      (state) => {
        if (state === 'Active') {
          generalRequest(
            `${dbApiUrl}/read_all`, reqBody
          ).then(
            data => {
              const newConfigList = []
              for (const [, dataset] of data.list.entries()) {
                // console.log(dataset)
                newConfigList.push([
                  dataset.name,
                  dataset.type,
                  dataset.id
                ])
              }
              setConfigList(newConfigList)
            }
          ).catch(
            err => {
              if (err.detail === undefined) {
                onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
              } else {
                onError(err)
              }
            }
          )
        }
      }
    )
  }

  useEffect(() => {
    updateConfigs()
  }, [dbApiUrl])

  return [payloadList, updateConfigs]
}
