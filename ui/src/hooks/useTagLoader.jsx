import { useState } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// import { callbackify } from 'util'

const useTagLoader = (
  dbApiUrl,
  onError = (data) => { }
) => {
  const [tagData, setTagData] = useState({})
  const [tagTag] = useState('')
  const [tagType, setTagType] = useState('')
  const [lastObject, setLastObjet] = useState({})

  const loadObject = (id) => {
    const reqBody = {
      stage: false,
      id,
      depth: 1,
      payload_data: false
    }
    generalRequest(
      `${dbApiUrl}/object/tree`, reqBody
    ).then(
      data => {
        console.log(data)
        setLastObjet({ id: data.id, children: data.children, payloads: data.payloads, type: data.type })
      }
    ).catch(
      err => {
        if (err.status === 403 && err.title === 'Dataset not found') {
          err.detail = 'Dataset not yet added to the database. Wait a moment or contact a developer.'
        }
        onError(err)
      }
    )
  }

  const loadTag = (name, type, killError = false) => {
    const reqBody = {
      stage: false,
      depth: 1,
      payload_data: false,
      name
    }
    generalRequest(
      `${dbApiUrl}/tag/tree`, reqBody
    ).then(
      data => {
        console.log(data)
        setTagData({ id: data.id, children: data.objects, payloads: data.payloads, type: data.type })
        setTagType(type + ': ' + name)
      }
    ).catch(
      err => {
        setTagData('Error loading tag data!')
        if (killError) {
          console.log(err)
        } else {
          onError(err)
        }
      }
    )
  }

  return [tagData, tagTag, tagType, loadTag, loadObject, lastObject]
}

export default useTagLoader
