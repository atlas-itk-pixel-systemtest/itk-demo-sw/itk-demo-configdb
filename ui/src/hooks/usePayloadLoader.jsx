import { useState } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

export default function usePayloadLoader (
  dbApiUrl,
  onError = (data) => {}
) {
  const [configData, setPayloadData] = useState('')
  const [configTag] = useState('')
  const [configType, setPayloadType] = useState('')

  // const _getConfigId = async (tagOrId) => {
  //   if (!isNaN(parseInt(tagOrId))) {
  //     setConfigTag('')
  //     return parseInt(tagOrId)
  //   }
  //   const reqBody = {
  //     name: tagOrId,
  //     table: 'configuration'
  //   }
  //   return generalRequest(
  //     `${dbApiUrl}/tag/get`, reqBody
  //   ).then(
  //     data => {
  //       setConfigTag(tagOrId)
  //       return data.dataset
  //     }
  //   ).catch(
  //     err => {
  //       setPayloadData('Error loading config tag!')
  //       onError(err)
  //     }
  //   )
  // }

  const loadPayload = (id) => {
    const reqBody = {
      id: id,
      stage: false,
      verbosity: 3
    }
    generalRequest(
      `${dbApiUrl}/payload/read`, reqBody
    ).then(
      data => {
        // console.log(data)
        setPayloadData(data.data)
        setPayloadType(data.type)
      }
    ).catch(
      err => {
        setPayloadData('Error loading config data!')
        onError(err)
      }
    )
  }

  return [configData, configTag, configType, loadPayload]
}
