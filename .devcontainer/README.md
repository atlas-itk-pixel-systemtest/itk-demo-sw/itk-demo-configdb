# ConfigDB devcontainer 

## Prerequisites
Please execute ./config before starting the devcontainer for the first time. This assures that the HOST variable is set correctly and allows you to access containers started on the host machine.

After starting the container you will have to reinstall the python environment. This is done by executing the following command in the terminal:

```bash
poetry install
```
NOTE: You will have to do this again if you want to use the python environment from outside the container.

## Docker containers
You are able to start the configdb stack either from within the devcontainer or from the host machine. If you want to start it from within the container make sure that the HOST variable in the .env file is set to the hostname of the machine and not the hostname of the devcontainer. Otherwise they wont be accessible from the host machine / will throw cors errors.
You can manually edit the .env file before starting the containers.
