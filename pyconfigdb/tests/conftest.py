import pytest
from pyconfigdb.configdb import ConfigDB
from pyconfigdb.configdb_pydantic import ConfigDB as ConfigDBPydantic


@pytest.fixture
def db() -> ConfigDB:
    key = "demi/api_test/pyconfigdb/test_api/url"
    configdb = ConfigDB(key)
    assert configdb.reset(False) == 200
    assert configdb.reset(True) == 200    
    return configdb

@pytest.fixture
def db_pydantic() -> ConfigDBPydantic:
    key = "demi/api_test/pyconfigdb/test_api/url"
    configdb = ConfigDBPydantic(key)
    assert configdb.reset(False).status == 200
    assert configdb.reset(True).status == 200
    return configdb


# @pytest.fixture
# def db() -> ConfigDB:
#     key = "demi/api_test/pyconfigdb/test_api/url"
#     configdb = ConfigDB(key)
#     return configdb