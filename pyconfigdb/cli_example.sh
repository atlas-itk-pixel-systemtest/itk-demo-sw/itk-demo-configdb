#!/bin/bash

mkdir ./dummy_rk/
mkdir ./dummy_rk/felixCard~0/
mkdir ./dummy_rk/felixCard~0/felixDevice~00
mkdir ./dummy_rk/felixCard~0/felixDevice~01
mkdir ./dummy_rk/felixCard~1
mkdir ./dummy_rk/felixCard~1/felixDevice~10
mkdir ./dummy_rk/felixCard~1/felixDevice~11

echo this is a payload >> ./dummy_rk/felixCard~0/payload~00
echo this is a payload >> ./dummy_rk/felixCard~0/payload~01
echo this is a payload >> ./dummy_rk/felixCard~0/felixDevice~00/payload~000
echo this is a payload >> ./dummy_rk/felixCard~0/felixDevice~00/payload~001
echo this is a payload >> ./dummy_rk/felixCard~0/felixDevice~01/payload~010
echo this is a payload >> ./dummy_rk/felixCard~0/felixDevice~01/payload~011

echo this is a payload >> ./dummy_rk/felixCard~1/payload~10
echo this is a payload >> ./dummy_rk/felixCard~1/payload~11
echo this is a payload >> ./dummy_rk/felixCard~1/felixDevice~10/payload~100
echo this is a payload >> ./dummy_rk/felixCard~1/felixDevice~10/payload~101
echo this is a payload >> ./dummy_rk/felixCard~1/felixDevice~11/payload~110
echo this is a payload >> ./dummy_rk/felixCard~1/felixDevice~11/payload~111

configdb import-rk ./dummy_rk dummy_rk
configdb commit dummy_rk committed_dummy -t "runkey" -a "Mary Jane" -c "This is a dummy"
configdb clone committed_dummy cloned_dummy -t "runkey" -a "Mary Jane" -c "This is a dummy"
#configdb import-rk ./dummy_rk dummy_rk -a "Mary Jane" -c "This is a dummy" --commit
#configdb clone dummy_rk cloned_dummy -t "runkey" -a "Mary Jane" -c "This is a dummy"
configdb export-rk ./exported_dummy cloned_dummy
configdb runkeys -t "runkey"
configdb runkeys -t "runkey" -b
configdb delete cloned_dummy