from pyconfigdb import ConfigDB
import json


def write(filename):
    db = ConfigDB("demi/jonas-laptop/itk-demo-configdb/api/url")
    with open(f"crud/pyconfigdb/scripts/strips/{filename}", "r") as f:
        data = json.load(f)

    db.stage_create(name="runkey2", data=data)


def read(filename):
    db = ConfigDB("demi/jonas-laptop/itk-demo-configdb/api/url")
    tree = db.tag_tree("runkey")

    test = json.dumps(tree["objects"][0])
    with open(f"crud/pyconfigdb/scripts/strips/{filename}", "w") as f:
        f.write(test)


if __name__ == "__main__":
    write("runkey_dump_to_file.json")
    read("output.json")

    write("output.json")
    read("output2.json")
