# pyconfigdb

**pyconfigdb** is a wrapper for the ConfigDB endpoints. It contains several functions to access the HTTP endpoints in python code. For more information on the functionalities, please refer to the ConfigDB API.

## Installation
To install this package and use it simply install it via pip (preferably from inside a venv). This will also enable you to run the CLI by running pyconfigdb from you terminal.
```bash
pip install pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```
To upgrade run:
```bash
pip install --upgrade pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```

The cli is also available as a container. For an example please refer to the [extended tutorial](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial/-/tree/extended_tutorial?ref_type=heads).

## CLI
*cli.py* implements a command line interface. For an exemplary bash script using the CLI, please see *cli_example.sh*.

The example script sets up a dummy runkey *(dummy_rk)* and writes it to the disk. This dummy_rk consists of two felixCards with two felixDevices each, as well as two payloads attached to each felixCard and felixDevice.

The script then uses cli.py to import the dummy_rk from the disk to the staging area. It then commits it to the database, clones it back into the staging area and exports it back to the disk with the name *exported_dummy*. \
It is also possible to directly commit the runkey to the database on import. For an example on how to do this, please refer to the commented lines in the script.

Then, all runkeys are listed with name, author and comment. Without the flag -b, *runkeys -t "runkey"* lists all runkeys from the staging area of the configdb. With -b, all the runkeys from the backend database are listed.

Lastly for cleanup, the script deletes the cloned dummy runkey from the staging area.

## Common Issues
If you are using a different configdb-stack than the example or you modified the STACK variable in the example config script you will need to specify the configdb-key under which it can be found in the service-registry. To do so simply append the --key parameter to you CLI commands, e.g.:

```bash
configdb runkeys -b --key demi/default/itk-demo-configdb/api/url
```

or export it once to the environment:

```bash
export KEY=demi/default/itk-demo-configdb/api/url
```
If you are unsure what value to set it to, take a look at your service-registry dump (by default at http://localhost:5111/) and search for the itk-demo-configdb/api/url key.

The same prinicple applies to the URL of the service-registry. If it is not http://localhost:5111/api/ set it to the correct value in each CLI command or in your environment:

```bash
export SR_URL=http://localhost:5111/api/
```