import urllib
from requests import get
from datetime import datetime

def default_serializer(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError(f"Type {type(obj)} not serializable")

def read_scan_configs(module_type: str, filter: str = ""):
    repo_url = "https://gitlab.cern.ch/api/v4/projects/9166/repository"
    params = {
        "path": f"configs/scans/{module_type}",
        "ref": "master",
        "per_page": 100,
    }

    url = f"{repo_url}/tree"
    response = get(url, params=params)
    if response.status_code == 200:
        items = response.json()
        configs = []
        for item in items:
            if filter:
                if filter not in item["name"]:
                    continue
            data = fetch_file_content(
                f"{repo_url}/files/{urllib.parse.quote(item['path'], safe='')}/raw?ref=master"
            )
            configs.append({"data": data, "type": item["name"].replace(".json", "")})
        return configs
    else:
        print(f"Failed to list directories: {response.status_code}")
        return None


def fetch_file_content(file_url):
    response = get(file_url)
    if response.status_code == 200:
        return response.text
    else:
        print(f"Failed to fetch file: {response.status_code}")
        return None


if __name__ == "__main__":
    items = read_scan_configs("rd53a", "std_")
    print("Done")
