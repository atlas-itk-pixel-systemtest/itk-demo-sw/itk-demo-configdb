import json
import os
import socket
import re

from pyregistry import ServiceRegistry
from requests import delete, get, post
from requests.exceptions import ConnectionError

from pyconfigdb.exceptions import ConfigDBConnectionError, ConfigDBResponseError
from pyconfigdb.tools import read_scan_configs, default_serializer
from configdb_server.dataclasses import (
    StandardResponseModel,
    ObjectFullModel,
    StandardMultiResponseModel,
    InsertObjectsModel,
    ObjectResponseModel,
    PayloadResponseModel,
    PayloadFullModel,
    AddToNodeModel,
    SearchSubtreeResponseModel,
    SearchResponse,
    StatusResponseModel,
    FullTreeModel,
    InsertModel,
    ObjectTreeResponse,
    FormatingResponseModel,
    TagResponseModel,
    UpdateConnectionsModel,
    TagFullModel,
    PayloadUpdateModel,
    TagTreeResponseModel,
    ObjectRecursiveModel,
    SearchTagResponse,
)

hostname = socket.gethostname()


class ConfigDB:
    def __init__(self, dbKey, srUrl=f"http://{hostname}:5111/api", headers={"content-type": "application/json", "accept": "application/json"}):
        self.dbKey = dbKey
        self.headers = headers

        sr = ServiceRegistry(srUrl)

        self.dbUrl = sr.get(self.dbKey)

        if not self.health():
            raise ConfigDBConnectionError(self.dbUrl)

    def _standard_post(self, endpoint, body, parameter, default_msg):
        try:
            response = post(f"{self.dbUrl}{endpoint}", params=parameter, data=json.dumps(body, default=default_serializer), headers=self.headers)
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    def _standard_get(self, endpoint, parameter, default_msg):
        try:
            response = get(f"{self.dbUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    def _standard_delete(self, endpoint, parameter, default_msg):
        try:
            response = delete(f"{self.dbUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    # High-level endpoints

    def health(self):
        """
        Check health of configDB

        Returns
        -------
        True if successfull
        """
        try:
            self._standard_get("/health", {}, "")
        except (ConfigDBConnectionError, ConfigDBResponseError):
            return False

        return True

    def add_node(self, object: ObjectFullModel, stage: bool = True) -> StandardResponseModel:
        """
        Adds an object with payloads.

        Parameters
        ----------
        object: ObjectFullModel
            type : str
                Type of the created object.
            id : str
                uuid of the object to create.
            payloads : list
                List of payload dicts or IDs.
            parents : list
                List of connections (dict of "id" and "view" option) for parents.
            children : list
                List of connections (dict of "id" and "view" option) for children.
            tags : list
                List of tag names that this object should be included in.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : ID of the created object.
        """
        res = self._standard_post("/node", object.model_dump(), {"stage": stage}, "Could not add node.")

        return StandardResponseModel(**res)

    def add_nodes(self, mylist: InsertObjectsModel, stage: bool = True) -> StandardMultiResponseModel:
        """
        Adds multiple objects, optionally including new payloads.

        Parameters
        ----------
        mylist : list
            List of objects to be inserted. See add_node for the structure of the objects.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["ids"] : List of IDs of the created objects.
        """
        res = self._standard_post("/nodes", mylist.model_dump(), {"stage": stage}, "Could not add nodes.")

        return StandardMultiResponseModel(**res)

    def add_to_node(self, model: AddToNodeModel, id: str = None, stage: bool = True) -> StandardMultiResponseModel:
        """
        Adds payloads or children to an existing node.

        Parameters
        ----------
        id : str
            UUID of the node.
        parents : list
            List of connections (dict of "id" and "view" option) for parents.
        children : list
            List of connections (dict of "id" and "view" option) for children.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["ids"] : IDs of the newly added payloads.
        """
        res = self._standard_post("/on_node", model.model_dump(), {"id": id, "stage": stage}, "Could not add payloads.")

        return StandardMultiResponseModel(**res)

    def payloads_bundle(self, ids: list[str]):
        """
        Bundles content of multiple json payloads into one.

        Parameters
        ----------
        ids : list[str]
            list of uuids to bundle

        Returns
        -------
        bundled payloads dict
        """
        res = self._standard_get("/payloads", {"ids": ids}, "Could not bundle payloads.")

        return res["bundle"]

    def payloads_change(self, changes: dict = {}, associations: list = [dict]):
        """
        Changes values of mulitple payloads at once

        Parameters
        ----------
        changes : dict
            dict representing the changes to be made to all payloads, in order to change list elements, simply use the index as key and the new value as the value
        associations : list
            list of associations (dict with payload and object ids)

        Returns
        -------
        list of newly created payloads
        """
        res = self._standard_post("/payloads", {"changes": changes, "associations": associations}, {}, "Could not change payloads.")

        return res["ids"]

    def remove_from_node(self, id: str = None, children: list = [], payloads: list = []):
        """
        Removes payloads and/or children from an existing node.

        Parameters
        ----------
        id : str
            uuid of the node.
        children : list
            List of children to be deleted.
        payloads : list
            List of payloads to be deleted.
        """
        res = self._standard_delete(  # noqa: F841
            "/on_node", {"id": id, "children": children, "payloads": payloads}, "Could not remove from node."
        )

        return None

    def pdb_import(
        self,
        itkdb_access_code1: str,
        itkdb_access_code2: str,
        name: str,
        serials: list,
        author: str = "",
        comment: str = "",
        type: str = "",
        count: bool = False,
    ):
        """
        Imports module configurations from the production database.

        Parameters
        ----------
        itkdb_access_code1/_code2 : str
            Access codes 1 and 2 for the itkdb, respectively.
        name : str
            Name of the tag with which the modules are saved.
        serials : list
            List of module serial numbers.
        author, comment, type : str
            Author, comment and type of the tag, respectively.
        count: if True, modules serials are extended with a counter

        Returns
        -------
        None.
        """
        res = self._standard_post(  # noqa: F841
            "/pdb_import",
            {
                "author": author,
                "comment": comment,
                "itkdb_access_code1": itkdb_access_code1,
                "itkdb_access_code2": itkdb_access_code2,
                "name": name,
                "serials": serials,
                "type": type,
            },
            {"count": count},
            "Could not import module configurations.",
        )

        return None

    def search(self, identifier: str, search_dict: dict, object_type: str = "", config_type: str = "") -> SearchResponse:
        """
        Gets the payloads of an object of a specific type with specific metadata.

        Parameters
        ----------
        identifier : str
            Name of the root tag or UUID of the root node.
        object_type : str
            Type of the searched object.
        config_type : str
            Type of the searched configuration.
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str

        Returns
        -------
        res["data"] : list of objects and their payloads
        """
        res = self._standard_post(
            "/search",
            {"search_dict": search_dict},
            {"identifier": identifier, "object_type": object_type, "config_type": config_type},
            "Could not get payloads.",
        )

        return SearchResponse(**res)

    def search_in_tag(
        self,
        name: str,
        payload_types: list[str] = [],
        object_ids: list[str] = [],
        search_dict: dict = {},
        payload_data: bool = False,
        order_by_object: bool = False,
    ) -> SearchTagResponse:
        """
        Gets the payloads of an object of a specific type with specific metadata.

        Parameters
        ----------
        name : str
            Name of the tag
        payload_types : list[str]
            List of types to filter by
        object_ids : list[str]
            List of object ids to filter by
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str
        payload_data: bool
            adds data of payload
        order_by_object: bool
            order payload types by object id

        Returns
        -------
        res["data"] : list of payloads
        """
        res = self._standard_post(
            "/search/tag",
            {"payload_types": payload_types, "search_dict": search_dict, "object_ids": object_ids},
            {"name": name, "payload_data": payload_data, "order_by_object": order_by_object},
            "Could not search tag.",
        )

        return SearchTagResponse(**res)

    def search_subtree(
        self, identifier: str, search_dict: dict, object_type: str = "", payload_data: bool = False, depth: int = -1, view: int = 1
    ) -> SearchSubtreeResponseModel:
        """
        Gets the subtrees with a root node of a specific type with specific metadata.

        Parameters
        ----------
        identifier : str
            Name of the root tag or UUID of the root node.
        object_type : str
            Type of the searched object.
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str
        payload_data: bool
            adds data of payload to each dataset
        depth: int
            defines depth level to which the tree should be read (-1 for full tree)
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.

        Returns
        -------
        res : list of subtrees
        """
        res = self._standard_post(
            "/search/subtree",
            {"search_dict": search_dict},
            {"identifier": identifier, "object_type": object_type, "payload_data": payload_data, "depth": depth, "view": view},
            "Could not get subtree.",
        )

        return SearchSubtreeResponseModel(**res)

    # Stage endpoints

    def stage_delete(self, identifier: str) -> StatusResponseModel:
        """
        Deletes a runkey from the staging area.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node.

        Returns
        -------
        res["id"] : id of the deleted runkey.
        """
        res = self._standard_delete("/stage", {"identifier": identifier}, "Could not delete runkey.")

        return StatusResponseModel(**res)

    def stage_create(self, tree: FullTreeModel) -> StandardResponseModel:
        """
        Creates an new tree or subtree.

        Parameters
        ----------
        data : dict
            (Sub-)tree to be added.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        name : str
            If given, a tag with this name is created for the root node.
        payloads : list
            List of payloads of the newly created node.

        Returns
        -------
        res["id"] : id of the root node of the newly created tree.
        """
        res = self._standard_post("/stage", tree.model_dump(), {}, "Could not create tree.")

        return StandardResponseModel(**res)

    def stage_clone(
        self, identifier: str, new_name: str = "", type: str = "", author: str = "", comment: str = "", view: int = 1, keep_ids: bool = False
    ) -> StandardMultiResponseModel:
        """
        Clones a tagged tree from the database to the staging area.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node in the database.
        new_name : str
            Name to be set for the cloned tree in the staging area.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        keep_ids : bool
            If True, the ids of the objects are kept. Default is False.

        Returns
        -------
        res["ids"] : list id of object uuids of the newly cloned tree.
        """
        res = self._standard_post(
            "/stage/clone",
            {"identifier": identifier, "name": new_name, "type": type, "author": author, "comment": comment, "view": view},
            {"keep_ids": keep_ids},
            "Could not clone tree.",
        )

        return StandardMultiResponseModel(**res)

    def stage_commit(
        self, identifier: str, new_name: str = "", type: str = "", author: str = "", comment: str = "", view: int = 1, keep_ids: bool = False
    ) -> StandardMultiResponseModel:
        """
        Commits a tagged tree from the staging area to the database.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node in the staging area.
        new_name : str
            Name to be set for the committed tree in the database.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        keep_ids : bool
            If True, the ids of the objects are kept. Default is False.


        Returns
        -------
        res["ids"] : list id of object uuids of the newly committed tree.
        """
        res = self._standard_post(
            "/stage/commit",
            {
                "identifier": identifier,
                "name": new_name,
                "type": type,
                "author": author,
                "comment": comment,
                "view": view,
            },
            {"keep_ids": keep_ids},
            "Could not commit tree.",
        )

        return StandardMultiResponseModel(**res)

    # Low-level endpoints

    def insert(self, model: InsertModel, table: str, stage: bool = True) -> StatusResponseModel:
        """
        Inserts multiple datasets into specified table.

        Parameters
        ----------
        insert_list : list
            List of objects to insert.
        table : str
            Table to insert the list into.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        None.
        """
        res = self._standard_post("/insert", model.model_dump(), {"stage": stage, "table": table}, "Could not insert objects.")

        return StatusResponseModel(**res)

    def object_create(
        self,
        object: ObjectFullModel,
        stage: bool = True,
    ) -> StandardResponseModel:
        """
        Sets an object dataset.

        Parameters
        ----------
        type : str
            Type of the object.
        payloads : list
            List of uuids of connected payloads.
        parents : list
            List of connections (dict of "id" and "view" option) for parents.
        children : list
            List of connections (dict of "id" and "view" option) for children.
        tags : list
            List of tag names that this object should be included in.
        id : str
            uuid of the object to create.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : id of created object
        """
        res = self._standard_post("/object", object.model_dump(), {"stage": stage}, "Could not create object.")

        return StandardResponseModel(**res)

    def object_read(self, id: str, stage: bool = True, view: int = 1) -> ObjectResponseModel:
        """
        Reads an object dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.


        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get("/object", {"id": id, "view": view, "stage": stage}, "Could not read object.")

        return ObjectResponseModel(**res)

    def object_tree(
        self,
        id: str,
        stage: bool = True,
        depth: int = -1,
        payload_data: bool = False,
        payload_filter: str = "",
        view: int = 1,
        format: bool = False,
    ) -> ObjectTreeResponse:
        """
        Gets a tree from an object dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        depth : int
            Depth up to which the tree will be traversed. Default is -1 (for full tree).
        payload_data : bool
            Defines whether or not to add the payload data. Default is False.
        payload_filter : str
            only include payloads which type contains this string
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/object/tree",
            {
                "stage": stage,
                "id": id,
                "depth": depth,
                "payload_data": payload_data,
                "payload_filter": payload_filter,
                "view": view,
                "format": format,
            },
            "Could not get object tree.",
        )

        return ObjectTreeResponse(**res)

    def payload_create(self, payload: PayloadFullModel, stage: bool = True) -> StandardResponseModel:
        """
        Creates a payload dataset.

        Parameters
        ----------
        data : str
            Data of the payload.
        name : str
            Name of the payload file.
        type : str
            Type of the payload.
        id : str
            uuid of the payload in hex form.
        tags : list
            List of tag names that this payload should be included in.
        objects : list
            List of uuids of objects that include this payload.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.

        Returns
        -------
        res["id"] : uuid of the created payload dataset.
        """
        res = self._standard_post("/payload", payload.model_dump(), {"stage": stage}, "Could not create payload.")

        return StandardResponseModel(**res)

    def payload_update(
        self,
        payload: PayloadUpdateModel,
        stage: bool = True,
    ):
        """
        Update a payload dataset.

        Parameters
        ----------
        id : str
            uuid of the payload in hex form.
        type : str
            Type of the payload.
        data : str
            Data of the payload.
        name : str
            Name of the payload file.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.


        Returns
        -------
        True
        """
        res = self._standard_post(  # noqa: F841
            "/payload/update", payload.model_dump(), {"stage": stage}, "Could not update payload."
        )

        return True

    def payload_read(self, id: str, stage: bool = True, meta: bool = False, format: bool = False) -> PayloadResponseModel:
        """
        Reads a payload dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get("/payload", {"id": id, "meta": meta, "format": format, "stage": stage}, "Could not get payload.")

        return PayloadResponseModel(**res)

    def tag_create(self, tag: TagFullModel, stage: bool = True) -> StandardResponseModel:
        """
        Creates a tag dataset.

        Parameters
        ----------
        objects : list
            List of uuids of tagged objects.
        payloads : list
            List of uuids of connected payloads.
        groups : list
            List of tag names that this tag should be included in.
        members : list
            List of tag names this tag should include.
        name, author, comment, type : str
            Name, author, comment and type of the tag, respectively.
        id : str
            uuid of the tag to be created.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : uuid of the created tag dataset.
        """
        res = self._standard_post("/tag", tag.model_dump(), {"stage": stage}, "Could not create tag.")

        return StandardResponseModel(**res)

    def tag_format(self, name: str, stage: bool = True, include_id: bool = False, shorten_data: int = 10) -> FormatingResponseModel:
        """
        Returns a tag dataset formatted as a string.

        Parameters
        ----------
        name : str
            Name of the tag dataset.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        include_id : bool
            Defines whether to include the id of the datasets. Default is False.
        shorten_data : int
            Amount of characters to shorten the data to. Default is 10. -1 returns the full data.

        Returns
        -------
        res["string"] : Requested string.
        """
        res = self._standard_get(
            "/tag/format",
            {
                "stage": stage,
                "include_id": include_id,
                "name": name,
                "shorten_data": shorten_data,
            },
            "Could not format tag.",
        )

        return FormatingResponseModel(**res)

    def tag_read(self, name: str, stage: bool = True) -> TagResponseModel:
        """
        Reads a tag dataset.

        Parameters
        ----------
        name : str
            Name of the tag dataset.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res : Dataset of the requested tag (without the HTML status code).
        """
        res = self._standard_get("/tag", {"stage": stage, "name": name}, "Could not read tag.")

        return TagResponseModel(**res)

    def tag_tree(
        self,
        name: str,
        stage: bool = True,
        depth: int = -1,
        payload_data: bool = False,
        payload_filter: str = "",
        view: int = 1,
        format: bool = False,
    ) -> TagTreeResponseModel:
        """
        Gets a tree from an object dataset.

        Parameters
        ----------
        name : str
            Name of the tag to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        depth : int
            Depth up to which the tree will be traversed. Default is -1 (for full tree).
        payload_data : bool
            Defines whether or not to add the payload data. Default is False.
        payload_filter : str
            only include payloads which type contains this string
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/tag/tree",
            {
                "stage": stage,
                "depth": depth,
                "payload_data": payload_data,
                "payload_filter": payload_filter,
                "name": name,
                "view": view,
                "format": format,
            },
            "Could not get tag tree.",
        )
        return TagTreeResponseModel(**res)

    def update_connections(self, connection: UpdateConnectionsModel) -> StatusResponseModel:
        """
        Updates the view of parent-child-connections.

        Parameters
        ----------
        list : list
            List of dictionaries containing:
                "child": str
                    uuid of child
                "parent": str
                    uuid of parent
                "view": int
                    view of the connection to be set. 1 = default, 2 = stale, 3 = both

        Returns
        -------
        StatusResponseModel
        """
        res = self._standard_post("/connections/update", connection.model_dump(), {}, "Could not update connection.")

        return StatusResponseModel(**res)

    # Database endpoints

    def read_all(
        self,
        table: str,
        filter: str = "",
        name_filter: str = "",
        payload_filter: str = "",
        child_filter: str = "",
        offset: int = 0,
        limit: int = 0,
        order_by: str = "",
        asc: bool = True,
        runkey_info: bool = False,
        payload_data: bool = False,
        depth: int = 0,
        connections: bool = False,
        stage: bool = True,
    ):
        """
        Gets a list of datasets from one table.

        Parameters
        ----------
        table : str
            Table to be listed.
        filter : str
            Type to filter by.
        name_filter: str
            Name to filter by.
        payload_filter : str
            Type of payload to filter by (only has an effect when reading the object or tag table). Datasets without a suitable payload will not be listed (only works for direct associations).
        child_filter : str
            Type of child to filter by, datasets without a suitable child will not be listed (only works for direct associations).
        offset : int
            Offset by which to start the lsit. Default is 0.
        limit : int
            Limit after which to end the list. Default is 0.
        order_by : str
            Attribute by which to order the list
        asc : bool
            Defines whether to order the list ascending or descending.
        runkey_info : bool
            Whether or not to add the runkey data to each dataset. Default is False.
        payload_data : bool
            Whether or not to add the payload data. Default is False.
        depth : int
            Depth up to which the tree will be traversed (-1 for full tree). Default is 0.
        connections : bool
            Whether to include the connections of the datasets (payloads and children)
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["list"] : List of requested datasets.
        """
        res = self._standard_get(
            "/read_all",
            {
                "stage": stage,
                "asc": asc,
                "child_filter": child_filter,
                "depth": depth,
                "filter": filter,
                "name_filter": name_filter,
                "limit": limit,
                "offset": offset,
                "order_by": order_by,
                "payload_data": payload_data,
                "payload_filter": payload_filter,
                "runkey_info": runkey_info,
                "table": table,
                "connections": connections,
            },
            "Could not read dataset.",
        )
        return res["list"]

    def reset(self, stage: bool = True) -> StatusResponseModel:
        """
        Deletes database tables and creates them again.
        ONLY AVAILABLE WHEN RUNNING IN DEVELOPER MODE

        Parameters
        ----------
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        StatusResponseModel
        """

        res = self._standard_get("/database/reset", {"stage": stage}, "Could not reset database.")
        return StatusResponseModel(**res)

    # Utilities

    def export_runkey(self, export_path: str, runkey_name: str, stage: bool = True):
        """
        Exports a runkey to the disk.

        Parameters
        ----------
        export_path : str
            Save path of the runkey.
        runkey_name : str
            Name of the runkey to be exported.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        None.
        """
        counter = 0
        new_export_path = os.path.join(export_path, runkey_name, str(counter))
        while os.path.isdir(new_export_path):
            new_export_path = os.path.join(
                export_path,
                runkey_name,
                str(counter),
            )
            counter = counter + 1

        tag = self.tag_tree(runkey_name, payload_data=True, stage=stage)

        export_from_dict(new_export_path, tag)
        print(f"Exported runkey to: {new_export_path}")

        return new_export_path

    def import_runkey(self, import_path: str, runkey_name: str, author: str, type: str, comment: str = "", commit: bool = False):
        """
        Exports a runkey from the disk.

        Parameters
        ----------
        commit : bool
            Whether or not to commit the runkey to the database.
        import_path : str
            File path of the runkey.
        runkey_name : str
            Name of the runkey to be imported.
        type, author, comment: str
            type, author, comment of the tag, respectively.

        Returns
        -------
        None.
        """
        runkey = create_import_object(import_path, "root")
        # Send this runkey to the staging area
        tag = FullTreeModel(name=runkey_name, data=runkey, author=author, comment=comment, type=type)
        root_id = self.stage_create(tag)
        # Get id of root node from response
        print(f"Imported runkey with root_id: {root_id}")

        if commit:
            self.stage_commit(root_id, runkey_name, type=type, author=author, comment=comment)
            print(f"Committed runkey with tag: {runkey_name}")

    def import_scan_configs(self, module_type: str = "rd53a", runkey_name: str = "scan_configs", filter: str = ""):
        """
        Creates (and commits) a runkey with scan configs from yarr repo

        Parameters
        ----------
        module_type : str
            Type of the module for which the scan configs are imported (see https://gitlab.cern.ch/YARR/YARR/-/tree/master/configs/scans?ref_type=heads for available types).
            Default is "rd53a".
        runkey_name : str
            Name of the tag for the runkey. Default is "scan_configs".
        filter : str
            Filter for the scan_config type. Default is empty string.

        Returns
        -------
        None.
        """

        configs = read_scan_configs(module_type, filter=filter)

        scan_configs = []
        for config in configs:
            scan_configs.append(
                {
                    "type": config["type"],
                    "payloads": [{"data": config["data"], "type": "config"}],
                }
            )

        tree = {"type": "root", "children": scan_configs}

        id = self.stage_create(FullTreeModel(data=tree, name="scan_configs", author="pyconfigdb")).id  # ????
        self.stage_commit(identifier=id, new_name=runkey_name, type="scan_configs", author="pyconfigdb")


def export_from_dict(dir, tag: TagTreeResponseModel):
    if not os.path.exists(dir):
        os.makedirs(dir, exist_ok=True)

    tag = tag.objects[0]

    __export_payloads(dir, tag.payloads)
    for object in tag.children:
        __loop_export_tree(dir, object)

    return dir


def __loop_export_tree(dir, data):
    dir = os.path.join(dir, f"{data.type}~{data.id}")
    os.makedirs(dir, exist_ok=True)
    __export_payloads(dir, data.payloads)
    for child in data.children:
        __loop_export_tree(dir, child)


def __export_payloads(dir, payloads):
    for payload in payloads:
        # if "name" in payload and payload["name"] is not None:
        #     name = payload["name"]
        # else:

        name = f"{payload.type}~{payload.id}"
        if "meta" in payload and payload.meta:
            name = f"meta_{name}"
        with open(os.path.join(dir, name), "w") as file:
            file.write(payload.data)


def create_import_object(dir, type):
    object = ObjectRecursiveModel(type=type)

    for obj in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, obj)):
            child_type = re.split(r"(~?\d+)|(~\w+)", obj)[0]
            child = create_import_object(os.path.join(dir, obj), child_type)
            object.children.append(child)
        else:
            object.payloads.append(__import_object_payload(dir, obj))
    return object


def __import_object_payload(dir, obj):
    with open(os.path.join(dir, obj), "r") as file:
        data = file.read()
    payl = PayloadFullModel(type=re.split(r"(~?\d+)|(~\w+)", obj)[0], data=data, name=obj)
    if "meta" in obj:
        payl.meta = True
    return payl
