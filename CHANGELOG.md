# Changelog

All notable changes[^1] to the `itk-demo-configdb`

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Staging area now only references payloads on clone instead of copying
- Added options to resolve these references to various endpoints
- Updated openapi
- Refactored and added pytests
- Author and type now mandatory for many endpoints
- Bugfixes

## [2.5.0] - 2025-02-21
- Removed redundant /stage/root endpoint
- Added /lists post and get endpoint to read/write from /to lists
- Refactoring and clean up of pydantic models
- Implemented use of pydantic models (instead of dicts) in server code and pyconfigdb
- Added reset database endpoint, if running database in developer mode

## [2.4.2] - 2025-02-05
- Added option to return histogramms as jsroot compatible files on get /payload

## [2.4.1] - 2025-01-17
- Fully switched to uv

## [2.4.0] - 2025-01-15
- Added possibility to delete runkeys from backend database, when running the db in dev mode
- Added response models
- Moved pdb import functionality to its own microservice
- Moved server code from its own repo
- Moved installation of server code to baseimage in /server

## [2.3.4] - 2024-11-21
- Fixed ambiguity in CLI commands when install pyconfigdb and itk-demo-configdb packages 

## [2.3.3] - 2024-09-24
- Bumped configdb-server version
  - Added option to add tags and objects when creating objects or payloads

## [2.3.2] - 2024-09-18
- Bumped configdb-server version
  - Bugfixes

## [2.3.1] - 2024-09-13
- Bumped configdb-server version
  - Enabled search_in_tag method to sent payload_data when order_by_dict

## [2.3.0] - 2024-09-09
- Moved pyconfigdb into itk-demo-configdb repo
- Added connections param to read_all

## [2.2.3] - 2024-09-09
- Added id param to add_node

## [2.2.2] - 2024-09-05
- Added additional options to search_in_tag method

## [2.2.1] - 2024-09-04
- Added keep_ids option to commit and clone

## [2.2.0] - 2024-09-03
- Added search_in_tag endpoint to search for payload from specific object inside a tag

## [2.1.1] - 2024-08-29
 - Improved migration process

## [2.1.0] - 2024-08-28
 - Bumped configdb-server version
  - Added tag-tag table and member and group parameters to read and create tag endpoints
  - Fixed bug in runkey creation when reusing ids
  - Improved deletion methods
 - Requires deletion of staging area database (docker volume), because sqlite does not support necessary migration
  
## [2.0.6] - 2024-08-27
 - Fixed bug that sometimes prevented the database from changing its state after a busy

## [2.0.5] - 2024-08-23
 - Bugfix in payload update

## [2.0.4] - 2024-08-22
 - Bumped configdb-server version
  - Added payload_filter to read_tree methods
  - Reenabled committing of tree identified by their root-node

## [2.0.3] - 2024-08-21
 - Bumped configdb-server version
  - Bugfixes
  
## [2.0.2] - 2024-08-21
 - Fixed db startup when using mariadb_key and service registry

## [2.0.1] - 2024-08-14
 - Bumped configdb-server version with bugfix for tree creation

## [2.0.0] - 2024-08-14
 - Changed from flask to fastapi framework
 - Changed docker compose file and api, see api/BREAKING.md for detailed changes

## [1.11.8] - 2024-08-09
- Fixed bug that prevented using a given id as the uuid in add_nodes

## [1.11.7] - 2024-08-08
 - Made metadata payloads editable in backend as well

## [1.11.6] - 2024-08-06
 - Bump to newest server code version
  - Includes fixes for tags with multiple objects/payloads

## [1.11.5] - 2024-07-12
 - Added option to give parents/children as uuids instead of connection dicts
 
## [1.11.4] - 2024-07-12
 - Bump to newest configdb-server version
  - Improved NameInUseError

## [1.11.3] - 2024-07-04
- Added format option to payload read functions, to enable formatting of json payloads

## [1.11.2] - 2024-07-01
 - Added meta option to read and update payloads

## [1.11.1] - 2024-06-28
 - Added endpoint to update a payload in the staging area

## [1.11.0] - 2024-06-27
 - Bump to newest configdb-server version
  - Rework of clone and commit functions, payload/object connections now persist
  - Bugfixes and Improvements to bulk payload methods

## [1.10.2] - 2024-06-26
 - Bump to newest configdb-server version
  - Bugfixes and Improvements to bulk payload methods

## [1.10.1] - 2024-06-23
 - Bugfix in API definition for object/create

## [1.10.0] - 2024-06-20
 - Updated to newest configdb-server tag 1.5.1
    - Includes new endpoints to bundle payloads and change multiple payloads at once

## [1.9.3] - 2024-06-10
 - Made new tag optional when committing a runkey

## [1.9.2] - 2024-06-05
 - Added errors on unsuccessfull startup

## [1.9.1] - 2024-05-27
 - Updated to newest configdb-server tag
 - Includes bugfixes to add_to_node and tree read endpoints

## [1.9.0] - 2024-05-16
 - Updated to newest configdb-server tag (includes breaking changes)
 - Added update connections endpoint
 - Bugfix for graph and delete operations
 - Subtree reusage support for /stage/new
  - Changed key which is used to detect whether a payload/object should be reused to "reuse_id" (was "id")

## [1.8.0] - 2024-05-14
 - Updated to newest configdb-server tag
 - Includes view parameter for many get & set endpoints
 - Slight change in add_node and add_nodes endpoints
  - Connections to objects are changed from lists of uuids to lists of dicts containing uuids and views

## [1.7.2] - 2024-04-25
 - Additional options for pdb import

## [1.7.1] - 2024-04-25
 - Updated to latest configdb-server version

## [1.7.0] - 2024-04-23
 - Removed import and export scripts
  - Now part of pyconfigdb package
 - Bumped configdb-server version
  - Changed output of search functions

## [1.6.0] - 2024-04-18
 - Added pdb-import endpoint

## [1.5.1] - 2024-04-17
 - Fixes to OpenAPI spec

## [1.5.0] - 2024-04-11
 - Improved application startup when going through database states
 - Improved gunicorn application startup
 - Bumped configdb-server version to fix alembic overiding logger
 - Removed unused migration files (migration is done via configdb-server package)
 - Removed default config.json file in favor of default values in pydantic config model
 - Fix to openapi spec
 - Changed default value of stage to True
    - Certain functions can be executed in staging area or backend database, if stage is not set, it will now default to the staging area database

## [1.4.2] - 2024-04-10
 - Bugfix to OpenAPI: Allow ids to be null

## [1.4.1] - 2024-04-09
 - Updated configdb-server package
 - Fixes to openapi definitions

## [1.4.0] - 2024-03-13
 - Updated openapi
 - Added ability to reuse payloads when creating whole tree from scratch

## [1.3.1] - 2024-02-21
 - Bump to newest database api version
 - Bugfixes
 - Option to set comment and author on cloning

## [1.3.0] - 2024-02-21
 - Bump to newest database api version
  - Includes alembic database migration features
  - Comment attribute for tag table
  - latest tag, that always points to the newest commit

## [1.2.1] - 2024-01-18
 - Bump to newest database api version

## [1.2.0] - 2024-01-09
- Reworked custom error ids  

## [1.1.2] - 2024-01-08
 - Bump to newest database api version

## [1.1.1] - 2024-01-04
 - Changed configdb containers to use pip instead of poetry

## [1.1.0] - 2023-12-19
 - Updated search and runkey example
 - Improved runkey container, allows you to create runkey from local dir structure
 - Changed tag format
    - From: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/itk-demo-configdb-api:1.1.0
    - To:   gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/api:1.1.0

## [1.0.4] - 2023-12-15
 - Updated example with fixed versions of microservices
 - Bumped configdb-server version to 0.36.0 and adapted for changes
 - Added new search_for_subtree endpoint

## [1.0.3] - 2023-12-13
  - Updated CI to tag api and ui

## [1.0.0] - 2023-12-13
  - Updated dependencies for config-checker (service-registry) and configdb-server
  - Updated example
    - Split into configdb compose file and services compose file
    - Service compose files includes new itk-demo-registry microservice

## [0.1.4] - 2023-11-29

### Changed
- Distinction between metadata and payload is now done via boolean parameter
    not implicitely via the type of the dataset

## [0.1.3] - 2023-11-27

### Changed
- Added default value for stage attribute (false) to all endpoints
  - Stage is not required anymore, endpoints will default to access the storage database
- Changed default value for depth attribute to zero for read_all endpoint

## [0.1.2] - 2023-11-24

### Added
- Started changelog

### Removed
- Removed old files in attic
- Empty database directory (will be created at runtime)

### Changed
- Major restructuring of the repository
  - Added api directory and moved all api related files there
- Updated example scripts and moved them to the /scripts directory



