# pyconfigdb
is a wrapper for the ConfigDB endpoints. It contains several functions to access the HTTP endpoints in python code. For more information on the functionalities, please refer to the ConfigDB API.

## Installation
To install this package and use it simply install it via pip (preferably from inside a venv). This will also enable you to run the CLI by running pyconfigdb from you terminal.
```bash
pip install pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```
To upgrade run:
```bash
pip install --upgrade pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```

For more info on that package and the CLI please refer to the [README of the pyconfigdb](https://demi.docs.cern.ch/pyconfigdb/).

The cli is also available as a container. For an example please refer to the [extended tutorial](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial/-/tree/extended_tutorial?ref_type=heads).

# itk-demo-configdb
The itk-demo-configdb microservice is part of the [readout-stack](https://demi.docs.cern.ch/deployments/readout-stack). It provides an openAPI interface to the functionality provided by the [configdb-server](https://demi.docs.cern.ch/pypi-packages/configdb-server) python package. It allows you to access the configuration database via HTTP requests. For access from python one can also use the configdb-server package directly.

Slides giving an overview of the itk-demo-configdb microservice can be found [here](https://indico.cern.ch/event/1373836/contributions/5774492/attachments/2803264/4891080/configdb.pdf).

## Related repositories
There are several repositories related to the configdb:
- [itk-demo-configdb](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb), general configdb repo, serves as a webserver for the configdb-server code
- [configdb-server](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/configdb-server), backend code of the configdb
- [configdb benchmarking](https://gitlab.cern.ch/jschmein/configdb_benchmarks), repository containing configdb-benchmarking scripts
- [runkey-ui](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-runkey), UI to create runkeys in the configdb

# Setup
Instructions on the setup of the configdb containers can be found under quick setup. 
A more generell microservice setup guide can be found [here](https://demi.docs.cern.ch/tutorials/microservice_setup).

## Quick setup
The itk-demo-configdb repository also includes some preconfigured docker-compose files in the example directory. These can be used to quickly set up a configdb instance. You only need to make sure that the following values in the .env file are set according to your local setup. 

- hostname of your system
- uid of your user
- gid of your user
- timezone of your system
- the docker directory, a directory where docker can store persistent data (like the database files), the docker user needs to have read and write access to this directory

For this you can use the following script.
```bash
./config
```

The docker compose file includes the following services:

- configdb-api
- mariadb server
- phpmyadmin
- runkey-ui

The services docker compose file includes the following services:

- service-registry
- etcd
- dashboard-ui
- restish

If you don´t need all of these services you can remove or comment them out from the docker-compose file.

You can start the services using the following command:
```bash
docker compose up -d
docker compose -f services_compose.yaml up -d
```

# How to use
The configuration database system consists of two databases. The staging area and the backend database. The staging area is intended to be used while constructing a runkey (or other kinds of trees/graphs). After the runkey is constructed has to be committed. Its structure is checked and the runkey is then copied to the backend database. Once committed a runkey can not be modified. It is only possible to add additional notes (e.g. scans and results).
If you want to edit an existing runkey, you can clone it into the staging area and make changes there. Once finished you can commit it as a new runkey. The old runkey will still exist in the backend database.

## Browser UI
The runkey-UI container is part of the configdb-stack. It allows you to manage your runkeys via a GUI.
When starting via the example docker compose files (explained [here](https://demi.docs.cern.ch/itk-demo-configdb/#quick-installation)) it is running on port 5111. It can also be found on the dashboard running on port 80.

## Runkeys
When starting the configdb-stack the runkey-import container will create an example runkey from the configs found in /example/configs/tree.
More detailed information on how to add custom runkeys using the runkey-import container, the GUI or the API can be found [here](https://demi.docs.cern.ch/tutorials/runkey/).

## Accessing the configdb interface
The main way of accessing the configdb microservice is directly through the HTTP API. The API is documented [here](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/-/blob/master/api/openapi/openapi.yml). The API is also available as a Swagger UI served from the microservice itself. For example, if the microservice is running on localhost:5000, the swagger UI can be accessed at localhost:5000/api/ui. The swagger UI provides documentation and a way to test the API endpoints.

## CLI
To access the configdb api via the command line you can use the restish container found [here](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/compose-collection/-/tree/master/restish?ref_type=heads) and in the example stack. It gives you access to the API of all compatible microservices. The restish container is also included in the docker-compose files in the example directory.

# Links
- [itk-demo-configdb slides](https://indico.cern.ch/event/1373836/contributions/5774492/attachments/2803264/4891080/configdb.pdf)
- [Git repo](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb)
- [Microservice-Setup](https://demi.docs.cern.ch/tutorials/microservice_setup)
- [Readout-Stack](https://demi.docs.cern.ch/deployments/readout-stack)
- [OpenAPI documentation](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/-/blob/master/api/openapi/openapi.yml)
- [configdb-server](https://demi.docs.cern.ch/pypi-packages/configdb-server)