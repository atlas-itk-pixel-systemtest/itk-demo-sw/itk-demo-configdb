# ConfigDB example

## Setup

The itk-demo-configdb repository also includes some preconfigured docker-compose files in the example directory. These can be used to quickly set up a configdb instance. You only need to make sure that the following values in the .env file are set according to your local setup.

- hostname of your system
- uid of your user
- gid of your user
- timezone of your system
- the docker directory, a directory where docker can store persistent data (like the database files), the docker user needs to have read and write access to this directory

For this you can use the following script.

```bash
./config
```

The docker compose stacks includes the following services:

- configdb-api
- postgres server
- adminer
- runkey-ui
- pyconfigdb
- service-registry
- etcd
- dashboard-ui
- dozzle

If you don´t need all of these services you can remove or comment them out from the docker-compose file or start only one of these stacks.

## Starting the containers

You can start the containers using the following command:

```bash
docker compose up -d
```

## Getting started

To import an example runkey from the file system execute:

```bash
./import_runkey
```

## Importing/Exporting runkeys from the file system

To import or export runkey use the pyconfigdb package. It can be accessed via the pyconfigdb container and the cli script.

```bash
./configdb --help
```

E.g. to import the example runkey from disk use:

```bash
./configdb import-rk /config/workspace/runkey example_rk --commit
```

to get a list of all available runkeys and than export one of them to your filesystem use:

```bash
./configdb runkeys --backend
./configdb export-rk /config/workspace/runkey latest --backend
```

Additional documentation on runkey I/O usage can be found [here](https://demi.docs.cern.ch/tutorials/runkey/#runkey-import-from-your-file-system), documentation for pyconfigdb is available [here](https://demi.docs.cern.ch/pyconfigdb/).

## Additional documentation

Additional documentation about using the configDB to store and search runkeys can be found [here](https://demi.docs.cern.ch/tutorials/runkey/).
