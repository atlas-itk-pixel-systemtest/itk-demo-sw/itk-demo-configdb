import requests
import json
from rich import print

# Python script to demonstrate the use of the CRUD operations
configdb_url = "http://localhost:5999/api"
headers = {"content-type": "application/json", "accept": "application/json"}

# Example runkey dict, runkey can be imported from file system as well
# Runkey consists of one felix connected to two optoboards, each having two frontends
example_runkey = {
    "type": "felix",
    "payloads": [
        {"type": "config", "data": "felix_config_data_placeholder"},
        {"type": "metadata", "data": {"serial": 1234}, "meta": True},
    ],
    "children": [
        {
            "type": "optoboard",
            "payloads": [
                {"type": "config", "data": "opto_config_data_placeholder"},
                {"type": "metadata", "data": {"serial": "opto1"}, "meta": True},
            ],
            "children": [
                {
                    "type": "frontend",
                    "payloads": [
                        {"type": "config", "data": "frontend_config_data_placeholder"},
                        {"type": "connectivity", "data": "frontend_connect_data_placeholder"},
                        {"type": "metadata", "data": {"serial": "fe1"}, "meta": True},
                    ],
                },
                {
                    "type": "frontend",
                    "payloads": [
                        {"type": "config", "data": "frontend_config_data_placeholder"},
                        {"type": "connectivity", "data": "frontend_connect_data_placeholder"},
                        {"type": "metadata", "data": {"serial": "fe2"}, "meta": True},
                    ],
                },
            ],
        },
        {
            "type": "optoboard",
            "payloads": [
                {"type": "config", "data": "opto_config_data_placeholder"},
                {"type": "metadata", "data": {"serial": "opto2"}, "meta": True},
            ],
            "children": [
                {
                    "type": "frontend",
                    "payloads": [
                        {"type": "config", "data": "frontend_config_data_placeholder"},
                        {"type": "connectivity", "data": "frontend_connect_data_placeholder"},
                        {"type": "metadata", "data": {"serial": "fe3", "rx": 2, "tx": 2}, "meta": True},
                    ],
                },
                {
                    "type": "frontend",
                    "payloads": [
                        {"type": "config", "data": "frontend_config_data_placeholder"},
                        {"type": "connectivity", "data": "frontend_connect_data_placeholder"},
                        {"type": "metadata", "data": {"serial": "fe4", "rx": 3, "tx": 3}, "meta": True},
                    ],
                },
            ],
        },
    ],
}


# create runkey in staging area
def create_runkey(runkey):
    body = {"name": runkey, "data": example_runkey}
    response = requests.post(f"{configdb_url}/stage", data=json.dumps(body), headers=headers)
    print("Create runkey response:")
    print(response.json())


# commit runkey to production area
def commit_runkey(runkey):
    body = {
        "name": runkey,  # new name
        "identifier": runkey,  # old name before commit
        "author": "example_script",
        "comment": "example runkey",
    }
    response = requests.post(f"{configdb_url}/stage/commit", data=json.dumps(body), headers=headers)
    print("Commit runkey response:")
    print(response.json())


# read an entire runkey (without payload data)
# contains only the runkey structure
def read_runkey(runkey, stage=False):
    params = {"name": runkey, "payload_data": False, "stage": stage}
    response = requests.get(f"{configdb_url}/tag/tree", params=params, headers=headers)
    print("Read runkey response:")
    print(response.json())

    # get uuid of frontend 1 to retrieve its config file later
    tree = response.json()["objects"]
    felix = tree[0]
    for opto in felix["children"]:
        if opto["metadata"]["serial"] == "opto1":
            opto1 = opto
            break
    for fe in opto1["children"]:
        if fe["metadata"]["serial"] == "fe1":
            fe1 = fe
            break
    return fe1


# clone an existing runkey
def clone_runkey(old_runkey, new_runkey):
    body = {
        "name": new_runkey,  # new name
        "identifier": old_runkey,  # old name before commit
        "author": "example_script",
        "comment": "example runkey",
    }
    response = requests.post(f"{configdb_url}/stage/clone", data=json.dumps(body), headers=headers)
    print("Clone runkey response:")
    print(response.json())


# search for a config payload conencted to a specific frontend in a runkey
def get_frontend_config(runkey, frontend):
    params = {"identifier": runkey, "object_type": "frontend", "config_type": "config"}
    body = {"search_dict": {"serial": frontend}}
    # search dict can contain any information from the metadata payload (here the serial is used)
    response = requests.post(f"{configdb_url}/search", params=params, data=json.dumps(body), headers=headers)
    print("Get frontend config response:")
    print(response.json())


def get_frontend_config_by_id(fe_config_id):
    params = {"id": fe_config_id, "stage": False}  # stage = False because runkey is already in production
    response = requests.get(f"{configdb_url}/payload", params=params, headers=headers)
    print("Get frontend config by id response:")
    print(response.json())


# search for a connectivity payload conencted to a specific frontend in a runkey
# only difference to get_frontend_config is the config_type parameter
def get_frontend_connectivity(runkey, frontend):
    params = {"identifier": runkey, "object_type": "frontend", "config_type": "connectivity"}
    body = {"search_dict": {"serial": frontend}}
    response = requests.post(f"{configdb_url}/search", params=params, data=json.dumps(body), headers=headers)
    print("Get frontend connectivity response:")
    print(response.json())


# update a config payload of a frontend
def update_fe_payload(fe, new_config):
    # get id of config payload of fe
    fe_payloads = fe["payloads"]
    for payload in fe_payloads:
        if payload["type"] == "config":
            fe_config_id = payload["id"]

    # delete old config payload
    params = {"id": fe["id"], "payloads": [fe_config_id]}
    response = requests.delete(f"{configdb_url}/on_node", params=params, headers=headers)
    print("Delete old config payload response:")
    print(response.json())

    # add new config payload
    params = {"id": fe["id"], "stage": True}
    body = {"payloads": [{"type": "config", "data": new_config}]}
    response = requests.post(f"{configdb_url}/on_node", params=params, data=json.dumps(body), headers=headers)
    print("Add new config payload response:")
    print(response.json())


if __name__ == "__main__":
    rk_name = "example_rk"
    rk2_name = "example_rk_edited"

    # create a runkey, commit it and read it
    create_runkey(runkey=rk_name)
    commit_runkey(runkey=rk_name)
    fe1 = read_runkey(runkey=rk_name, stage=False)  # stage = False because runkey is already in production

    # get id of config payload of fe 1
    fe1_payloads = fe1["payloads"]
    for payload in fe1_payloads:
        if payload["type"] == "config":
            fe_config_id = payload["id"]

    # frontend payloads (config or connecectivity) can be retrieved by their uuid or searched for
    get_frontend_config(runkey=rk_name, frontend="fe1")
    get_frontend_config_by_id(fe_config_id=fe_config_id)
    get_frontend_connectivity(runkey=rk_name, frontend="fe1")

    # clone existing runkey, change one payload config and commit it
    clone_runkey(old_runkey=rk_name, new_runkey=rk2_name)
    fe1 = read_runkey(runkey=rk2_name, stage=True)  # stage=True to read from staging area
    update_fe_payload(fe=fe1, new_config="new_config_data")
    commit_runkey(runkey=rk2_name)
    get_frontend_config(runkey=rk2_name, frontend="fe1")
